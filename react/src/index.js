/**
 *      1、{props.children}子组件，类似插槽作用
 *      2、props同样可传递完整组件
 *      3、本质上来讲，JSX 只是为 React.createElement(component, props, ...children) 方法提供的语法糖
 *      4、Component.defaultProps指定组件默认属性
 *      5、ref处理场景：处理焦点、文本选择或媒体控制。 触发强制动画。 集成第三方 DOM 库；
 *      6、react优化：1、线上使用压缩过的生产版本，可以看devtool颜色判断
 *                    2、shouldComponentUpdate做浅比较，也可以继承PureComponent实现；PureComponent只做数据浅比较，故而深层数据变更无法发现，应注意使用场景
 *                    3、React.memo定义函数式组件的数据浅比较，也可以自定义比较规则；
 */
