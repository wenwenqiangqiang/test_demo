### LwjUpload 
  
1. 实现快速生成下载链接
2. 使用方式：
   1. 引包
   2. 调用：
   ```

    <LwjUpload {...uploadProps}/>

   ```
   3. 传入参数：
   ```

    uploadTitle: 上传按钮名称，string类型，必填；
    uploadIcon: 按钮前icon，string类型，请从antd字体图标库选取，非必填；
    btnType: 按钮类型，string类型，可选参数“primary” “ghost”，默认“ghost”，非必填；
    target: 上传文件路径，string类型，必填；
    name: 上传文件名称，string类型，非必填；
    fileType: 上传文件类型限制，string类型，非必填,不填默认不限制文件类型；
    onChange: 上传文件回调，func类型，非必填，返回包含==>{
                                                        file: { ... },
                                                        fileList: [ ... ],
                                                        event: { ... }
                                                      }
    showUploadList: 是否显示已上传列表，boolean类型，非必填，默认true；

   ```

