import { DatePicker } from 'antd';
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import moment from 'moment';


export default class LwjDatePicker extends Component {

  state = {
  };

  componentDidMount() { }
  render() {
    const {
      onChange,
      checkData = null,
      ...fromProps
    } = this.props;

    const preChange = (dates, dateStrings) => {
      onChange(dates ? moment(dateStrings, 'YYYY-MM-DD') : null, dateStrings);
      checkData && checkData(dates ? moment(dateStrings, 'YYYY-MM-DD') : null, dateStrings);
    }



    return (
      <DatePicker onChange={(dates, dateStrings) => preChange(dates, dateStrings)} style={{
        width: '100%',
      }} {...fromProps} />
    );
  }
}

LwjDatePicker.props = {
  onChange: PropTypes.func
}
