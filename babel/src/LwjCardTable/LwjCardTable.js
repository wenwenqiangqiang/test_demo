import React from 'react'
import { Tooltip, Pagination, Card, Popover, Row } from 'antd'
import {
  ChartCard,
  MiniProgress,
} from 'components/Charts';
import PropTypes from 'prop-types';
import { couponStatus } from 'utils/enumConfig'
import moment from 'moment';
import OrderFilterUnExpand from 'components/Filter/NormalFilter';
import DescriptionList from 'components/DescriptionList';

const LwjCardTable = ({
  dataSource,
  total,
  pagination,
  loading,
  dispatch,
  pageChangeAction,
  currentItem,
  ...extConfigs
}) => {
  const { Description } = DescriptionList;
  const { Grid } = Card;
  const onShowSizeChange = (current, pageSize) => {
    pagination.current = current;
    pagination.defaultPageSize = pageSize;
    dispatch({
      type: pageChangeAction,
      payload: {
        limit: pageSize,
        start: current - 1,
        isTablePagination: true
      }
    })
  }
  const onChange = (page, pageSize) => {
    pagination.current = page;
    pagination.defaultPageSize = pageSize;
    dispatch({
      type: pageChangeAction,
      payload: {
        limit: pageSize,
        start: page - 1,
        isTablePagination: true
      }
    })
  }
  const filterProps = {
    filterItems: [
      {
        type: 'select',
        id: 'couponsStatus',
        title: '优惠券状态',
        options: [
          {
            value: "",
            name: '全部'
          }, {
            value: 'CAN_USE',
            name: '可用'
          }, {
            value: 'USED',
            name: '已使用'
          }, {
            value: 'ABANDONED',
            name: '已作废'
          }
        ]
      }, {
        type: 'dateRule',
        id: 'createTime',
        dateFormat: 'YYYY/MM/DD HH:mm:ss',
        title: '添加时间'
      }
    ],
    onFilterChange(value) {
      dispatch({
        type: 'coupon/queryByDistributor',
        payload: {
          ...value,
          limit: pagination.defaultPageSize,
          start: 0
        }
      })
    }
  }

  const renderCardList = (source) => {
    if (!_.isArray(source)) {
      return
    }
    return source.map((value) => {
      return (
        <Grid key={value.createTime} >
          <ChartCard
            key={value.createTime}
            loading={loading}
            bordered={false}
            title={value.couponsNum}
            total={<span style={{ color: value.status === 'CAN_USE' ? "black" : "gainsboro" }}>{couponStatus[value.status]}</span>}
            footer={
              <div style={{ whiteSpace: 'nowrap', overflow: 'hidden' }}>
                <span style={{ color: value.status === 'CAN_USE' ? "black" : "gainsboro" }}>创建日期：{moment(value.createTime).format("YYYY-MM-DD")}
                  <Popover placement="right" title="优惠券详情" content={
                    <div>
                      <div>优惠券ID:{value.couponsNum}</div>
                      <div>状态:{couponStatus[value.status]}</div>
                      <div>业务类型:下单</div>
                      <div>总金额:{value.amount}</div>
                      <div>已用金额:{value.usedAmount}</div>
                      <div>使用合同号:{value.contractNum}</div>
                      <div>创建时间:{moment(value.createTime).format("YYYY-MM-DD HH:mm:ss")}</div>
                      {value.useTime && <div>使用时间:{moment(value.useTime).format("YYYY-MM-DD HH:mm:ss")}</div>}
                    </div>
                  } trigger="click">
                    <a style={{ marginLeft: 15 }}>使用详情</a>
                  </Popover>
                </span>
              </div>
            }
            contentHeight={46}
          >
            <MiniProgress  percent={((value.amount - value.usedAmount) / value.amount) * 100} strokeWidth={7} target={100} color={value.status === 'CAN_USE' ? "#13C2C2" : "gainsboro"} />
            <span style={{ color: value.status === 'CAN_USE' ? "black" : "gainsboro" }}>总金额：¥ {value.amount} </span>
          </ChartCard>
        </Grid>
      )
    })
  }

  return (
    <div>
      <Card >
        <OrderFilterUnExpand {...filterProps} />
        {renderCardList(dataSource)}
      </Card>
      <Pagination showSizeChanger onChange={onChange} onShowSizeChange={onShowSizeChange} defaultCurrent={1} total={total} /></div>
  )
};

LwjCardTable.propTypes = {
  total: PropTypes.any,
  dataSource: PropTypes.array,
  pagination: PropTypes.any,
  loading: PropTypes.bool,
  dispatch: PropTypes.func,
  pageChangeAction: PropTypes.string,
};

export default LwjCardTable
