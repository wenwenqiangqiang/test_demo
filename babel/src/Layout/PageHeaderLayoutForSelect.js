import React from 'react';
import {Link} from 'dva/router';
import PageHeader from '../PageHeader';
import styles from './PageHeaderLayoutForSelect.less';
import { Menu, Dropdown, Button, Icon, message,Select } from 'antd';
const { Option } = Select;

export default({
  children,
  wrapperClassName,
  top,
  selecData,
  selectedMendian,
  selectedMendianData,
  handleMenuClick,
  ...restProps
}) => {



  // 处理下拉选项
  const getOptions = (value) => value.map((item,index) => {
    return (
      <Menu.Item key={index}>
         {item.name}
      </Menu.Item>
    )
  })


  const menu = (
    <Menu onClick={handleMenuClick}>
      {getOptions(selecData)}
    </Menu>
  );
  

  const getSelectOptions =value=>value.map((item,index)=>{
    return (
      <Option value={index} key={index}>{item.name}</Option>
    )
  })


  const getdefaultValue=()=>{
    let indexdata=0;
    selecData.forEach((element,index) => {
      if(element.id==selectedMendianData.id){
        indexdata=index
      }
    });
    return indexdata;
  }
  return (
    <div style={{
      margin: '-24px -24px 0'
    }} className={wrapperClassName}>
      {top}
      <PageHeader key="pageheader" {...restProps} linkElement={Link}/>
      <div  className={ styles.Dropdown }>

        <Select onChange={handleMenuClick} style={{minWidth:'180px'}} value={selectedMendian}>
          {getSelectOptions(selecData)}
        </Select>
        {/* <Dropdown overlay={menu}>
            <Button>
              {selectedMendian} <Icon type="down" />
            </Button>
        </Dropdown> */}
      </div>
      {children
        ? <div className={styles.content}>{children}</div>
        : null}
    </div>
  )
};
