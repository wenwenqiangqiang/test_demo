import React from 'react'
import { config } from 'utils'
import styles from './Footer.less'

const Footer = () => (<div className={styles.footer}>
  由丽维家产业互联网委员会提供技术支持
</div>)

export default Footer
