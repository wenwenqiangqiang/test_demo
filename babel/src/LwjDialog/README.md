### LwjDialog 
  
1. 实现快速弹窗功能
2. 使用方式：
   1. 引包
   2. 调用：
   ```

    LwjDialog .success();
    LwjDialog .info();
    LwjDialog .erro();
    LwjDialog .confirm();

   ```
   3. 传入参数：
   ```

   传入参数为一个对象，可包含以下参数：
   title：弹窗标题，建议必填，如无标题，可传入'';
   content:弹窗内容，建议必填；
   okText：'确认按钮显示文本'，可选；
   cancelText：'取消按钮文本'，可选，仅适用于confirm,
   onOk：点击确认回调，可选；
   onCancel：仅用于confirm取消回调，可选；

   ```

