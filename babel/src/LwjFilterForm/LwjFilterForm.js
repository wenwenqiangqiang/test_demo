import {
    Form,
    Upload,
    Button,
    Icon,
    message,
    Col,
    Input,
    Row,
    Select,
    DatePicker
} from 'antd';
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import {get} from 'http';
import style from './LwjFilterForm.less';
const FormItem = Form.Item;
const Option = Select.Option;

let FilterForm = ({
    filterItems,
    getBackData,
    form: {
        getFieldDecorator,
        getFieldsValue,
        setFieldsValue
    }
}) => {
    // 表单项布局
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 16
            }
        }
    };

    const getItems = filterItems => filterItems.map((value) => {
        return <Col span={8} key={value.id}>{getItem(value)}</Col>
    })

    const getItem = (value => {
        switch (value.type) {
            case 'input':
                return (
                    <FormItem label={value.title} {...formItemLayout}>
                        {getFieldDecorator(value.id, {})(<Input placeholder={value.placeholder}/>)}
                    </FormItem>
                )
            case 'select':
                return (
                    <FormItem label={value.title} {...formItemLayout}>
                        {getFieldDecorator(value.id, {initialValue: value.defaultValue})(
                            <Select initialValue={value.defaultValue}>
                                {getOptions(value.options)}
                            </Select>
                        )}
                    </FormItem>
                )
            case 'date':
                return (
                    <FormItem label={value.title} {...formItemLayout}>
                        {getFieldDecorator(value.bid, {initialValue: value.defaultValue})(<DatePicker
                            onChange={dateChange}
                            showTime
                            format={value.format || "YYYY/MM/DD"}
                            style={{
                            width: 160
                        }}/>)}
                        至{getFieldDecorator(value.eid, {initialValue: value.defaultValue})(<DatePicker
                            showTime
                            format={value.format || "YYYY/MM/DD"}
                            style={{
                            width: 160
                        }}/>)}
                    </FormItem>
                )
        }
    })

    const dateChange = (value) => {
        console.log(value);
    }
    // 处理下拉选项
    const getOptions = (value) => value.map(option => {
        return <Option value={option.value} key={option.value}>{option.name}</Option>
    })

    const pullOut = () => {
        let fields = getFieldsValue();
        getBackData(fields);
    }
    return (
        <div>
            <Form inline={'true'}>
                {getItems(filterItems)}
                <Col span={8} className={style.center}>
                    <Button type={'primary'} onClick={pullOut}>搜索</Button>
                </Col>
            </Form>
        </div>
    );
};

export default Form.create()(FilterForm);