### LwjImageView 
  
1. 实现图片裁剪
2. 使用方式：
   1. 引包
   2. 调用：
   ```

  <LwjImageView {...imgProps}/>

   ```
   3. 传入参数：
   ```

       dataUrl:图片链接地址，必填；
       getCropData:框选完成后回传裁剪参数，可选；
       getFile：框选完成后回传生成二进制文件，可选；

   ```

