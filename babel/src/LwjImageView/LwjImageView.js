import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import ReactCrop, {makeAspectCrop} from './ImageCrop/ReactCrop'
import './LwjImageView.less'

export default class LwjImageView extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      crop: {
        x: 0,
        y: 0,
        // aspect: 16 / 9,
      },
      // 最大高度 maxHeight: 80
      pixelCrop: {},
      isOK: false
    };
  }

  onCropChange = (crop, pixelCrop) => {
    this.setState({crop, pixelCrop});
  }
  onImageLoaded = (image) => {
    this.setState({
      // crop: makeAspectCrop({     x: 0,     y: 0,     aspect: 10 / 4,     width: 25
      // }, image.naturalWidth / image.naturalHeight),
      image
    });
  }

  onCropComplete = (crop, pixelCrop) => {
    const img = new Image();
    img.src = this.props.dataUrl;
    this.test(img, pixelCrop, 'LwjNewPic');
    if (this.props.getCropData) {
      this
        .props
        .getCropData(pixelCrop);
    }
  }

  /**
    * 绘制剪切图片
    * @param {File} image - Image File Object
    * @param {Object} pixelCrop - pixelCrop Object provided by react-image-crop
    * @param {String} fileName - Name of the returned file in Promise
    */
  getCroppedImg = (image, pixelCrop, fileName) => {
    const canvas = document.createElement('canvas');
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);

    /**
         * As Base64 string const base64Image = canvas.toDataURL('image/jpeg');
         *    As ablob
         */

    return new Promise((resolve, reject) => {
      canvas.toBlob(file => {
        file.name = fileName;
        resolve(file);
      }, 'image/jpeg');
    });
  }

  async test(image, pixelCrop, returnedFileName) {
    const croppedImg = await this.getCroppedImg(image, pixelCrop, returnedFileName);
    if (this.props.getFile) {
      this
        .props
        .getFile(croppedImg);
    }
  }

  render() {
    const {dataUrl} = this.props;

    // const dataUrlCheck = (value) => {     let image = new Image(); image.onload =
    // ()=> {         let width = this.width;         let height = this.height;
    // if (width > 1920 || height > 1080) { this.props.isOK=true; console.log(999);
    //            return
    // 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAEUlEQV
    // Q ImWP8DwQMQMDEAAUAPfgEAJtlyeIAAAAASUVORK5CYII=';         } else {
    // this.state.isOK=false;             return value;         }     } image.src =
    // value; }
    return (
      <div>
        <ReactCrop
          {...this.state}
          src={dataUrl}
          onImageLoaded={this.onImageLoaded}
          onComplete={this.onCropComplete}
          onChange={this.onCropChange}/> {(this.state.pixelCrop.width || this.state.pixelCrop.height) && <div>{this.state.pixelCrop.width}X{this.state.pixelCrop.height}</div>}
      </div>
    )
  }
}

LwjImageView.propTypes = {
  getCropData: PropTypes.func,
  getFile: PropTypes.func
}