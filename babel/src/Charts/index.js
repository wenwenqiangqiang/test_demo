import ChartCard from './ChartCard';
import MiniProgress from './MiniProgress';



const Charts = {
  MiniProgress,
  ChartCard,

};

export {
  Charts as default,
  MiniProgress,
  ChartCard,
};
