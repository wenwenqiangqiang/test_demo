import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Col,Tooltip } from 'antd';
import styles from './index.less';
import responsive from './responsive';

const Description = ({ term, column, tooltip, className, children, ...restProps }) => {
  const clsString = classNames(styles.description, className);
  return (
    <Col className={clsString} {...responsive[column]} {...restProps}>
      {term && <div className={styles.term}>{term}</div>}
      {<div className={styles.detail}><Tooltip placement="topLeft" title={tooltip}>{children ? children : "无"}</Tooltip></div>}
    </Col>
  );
};

Description.defaultProps = {
  term: '',
};

Description.propTypes = {
  term: PropTypes.node,
};

export default Description;
