let city = {
    "110000": [
        {
            "province": "北京市", 
            "name": "市辖区", 
            "code": "110100"
        }
    ], 
    "120000": [
        {
            "province": "天津市", 
            "name": "市辖区", 
            "code": "120100"
        }
    ], 
    "130000": [
        {
            "province": "河北省", 
            "name": "石家庄市", 
            "code": "130100"
        }, 
        {
            "province": "河北省", 
            "name": "唐山市", 
            "code": "130200"
        }, 
        {
            "province": "河北省", 
            "name": "秦皇岛市", 
            "code": "130300"
        }, 
        {
            "province": "河北省", 
            "name": "邯郸市", 
            "code": "130400"
        }, 
        {
            "province": "河北省", 
            "name": "邢台市", 
            "code": "130500"
        }, 
        {
            "province": "河北省", 
            "name": "保定市", 
            "code": "130600"
        }, 
        {
            "province": "河北省", 
            "name": "张家口市", 
            "code": "130700"
        }, 
        {
            "province": "河北省", 
            "name": "承德市", 
            "code": "130800"
        }, 
        {
            "province": "河北省", 
            "name": "沧州市", 
            "code": "130900"
        }, 
        {
            "province": "河北省", 
            "name": "廊坊市", 
            "code": "131000"
        }, 
        {
            "province": "河北省", 
            "name": "衡水市", 
            "code": "131100"
        }, 
        {
            "province": "河北省", 
            "name": "省直辖县级行政区划", 
            "code": "139000"
        }
    ], 
    "140000": [
        {
            "province": "山西省", 
            "name": "太原市", 
            "code": "140100"
        }, 
        {
            "province": "山西省", 
            "name": "大同市", 
            "code": "140200"
        }, 
        {
            "province": "山西省", 
            "name": "阳泉市", 
            "code": "140300"
        }, 
        {
            "province": "山西省", 
            "name": "长治市", 
            "code": "140400"
        }, 
        {
            "province": "山西省", 
            "name": "晋城市", 
            "code": "140500"
        }, 
        {
            "province": "山西省", 
            "name": "朔州市", 
            "code": "140600"
        }, 
        {
            "province": "山西省", 
            "name": "晋中市", 
            "code": "140700"
        }, 
        {
            "province": "山西省", 
            "name": "运城市", 
            "code": "140800"
        }, 
        {
            "province": "山西省", 
            "name": "忻州市", 
            "code": "140900"
        }, 
        {
            "province": "山西省", 
            "name": "临汾市", 
            "code": "141000"
        }, 
        {
            "province": "山西省", 
            "name": "吕梁市", 
            "code": "141100"
        }
    ], 
    "150000": [
        {
            "province": "内蒙古自治区", 
            "name": "呼和浩特市", 
            "code": "150100"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "包头市", 
            "code": "150200"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "乌海市", 
            "code": "150300"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "赤峰市", 
            "code": "150400"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "通辽市", 
            "code": "150500"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "鄂尔多斯市", 
            "code": "150600"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "呼伦贝尔市", 
            "code": "150700"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "巴彦淖尔市", 
            "code": "150800"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "乌兰察布市", 
            "code": "150900"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "兴安盟", 
            "code": "152200"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "锡林郭勒盟", 
            "code": "152500"
        }, 
        {
            "province": "内蒙古自治区", 
            "name": "阿拉善盟", 
            "code": "152900"
        }
    ], 
    "210000": [
        {
            "province": "辽宁省", 
            "name": "沈阳市", 
            "code": "210100"
        }, 
        {
            "province": "辽宁省", 
            "name": "大连市", 
            "code": "210200"
        }, 
        {
            "province": "辽宁省", 
            "name": "鞍山市", 
            "code": "210300"
        }, 
        {
            "province": "辽宁省", 
            "name": "抚顺市", 
            "code": "210400"
        }, 
        {
            "province": "辽宁省", 
            "name": "本溪市", 
            "code": "210500"
        }, 
        {
            "province": "辽宁省", 
            "name": "丹东市", 
            "code": "210600"
        }, 
        {
            "province": "辽宁省", 
            "name": "锦州市", 
            "code": "210700"
        }, 
        {
            "province": "辽宁省", 
            "name": "营口市", 
            "code": "210800"
        }, 
        {
            "province": "辽宁省", 
            "name": "阜新市", 
            "code": "210900"
        }, 
        {
            "province": "辽宁省", 
            "name": "辽阳市", 
            "code": "211000"
        }, 
        {
            "province": "辽宁省", 
            "name": "盘锦市", 
            "code": "211100"
        }, 
        {
            "province": "辽宁省", 
            "name": "铁岭市", 
            "code": "211200"
        }, 
        {
            "province": "辽宁省", 
            "name": "朝阳市", 
            "code": "211300"
        }, 
        {
            "province": "辽宁省", 
            "name": "葫芦岛市", 
            "code": "211400"
        }
    ], 
    "220000": [
        {
            "province": "吉林省", 
            "name": "长春市", 
            "code": "220100"
        }, 
        {
            "province": "吉林省", 
            "name": "吉林市", 
            "code": "220200"
        }, 
        {
            "province": "吉林省", 
            "name": "四平市", 
            "code": "220300"
        }, 
        {
            "province": "吉林省", 
            "name": "辽源市", 
            "code": "220400"
        }, 
        {
            "province": "吉林省", 
            "name": "通化市", 
            "code": "220500"
        }, 
        {
            "province": "吉林省", 
            "name": "白山市", 
            "code": "220600"
        }, 
        {
            "province": "吉林省", 
            "name": "松原市", 
            "code": "220700"
        }, 
        {
            "province": "吉林省", 
            "name": "白城市", 
            "code": "220800"
        }, 
        {
            "province": "吉林省", 
            "name": "延边朝鲜族自治州", 
            "code": "222400"
        }
    ], 
    "230000": [
        {
            "province": "黑龙江省", 
            "name": "哈尔滨市", 
            "code": "230100"
        }, 
        {
            "province": "黑龙江省", 
            "name": "齐齐哈尔市", 
            "code": "230200"
        }, 
        {
            "province": "黑龙江省", 
            "name": "鸡西市", 
            "code": "230300"
        }, 
        {
            "province": "黑龙江省", 
            "name": "鹤岗市", 
            "code": "230400"
        }, 
        {
            "province": "黑龙江省", 
            "name": "双鸭山市", 
            "code": "230500"
        }, 
        {
            "province": "黑龙江省", 
            "name": "大庆市", 
            "code": "230600"
        }, 
        {
            "province": "黑龙江省", 
            "name": "伊春市", 
            "code": "230700"
        }, 
        {
            "province": "黑龙江省", 
            "name": "佳木斯市", 
            "code": "230800"
        }, 
        {
            "province": "黑龙江省", 
            "name": "七台河市", 
            "code": "230900"
        }, 
        {
            "province": "黑龙江省", 
            "name": "牡丹江市", 
            "code": "231000"
        }, 
        {
            "province": "黑龙江省", 
            "name": "黑河市", 
            "code": "231100"
        }, 
        {
            "province": "黑龙江省", 
            "name": "绥化市", 
            "code": "231200"
        }, 
        {
            "province": "黑龙江省", 
            "name": "大兴安岭地区", 
            "code": "232700"
        }
    ], 
    "310000": [
        {
            "province": "上海市", 
            "name": "市辖区", 
            "code": "310100"
        }
    ], 
    "320000": [
        {
            "province": "江苏省", 
            "name": "南京市", 
            "code": "320100"
        }, 
        {
            "province": "江苏省", 
            "name": "无锡市", 
            "code": "320200"
        }, 
        {
            "province": "江苏省", 
            "name": "徐州市", 
            "code": "320300"
        }, 
        {
            "province": "江苏省", 
            "name": "常州市", 
            "code": "320400"
        }, 
        {
            "province": "江苏省", 
            "name": "苏州市", 
            "code": "320500"
        }, 
        {
            "province": "江苏省", 
            "name": "南通市", 
            "code": "320600"
        }, 
        {
            "province": "江苏省", 
            "name": "连云港市", 
            "code": "320700"
        }, 
        {
            "province": "江苏省", 
            "name": "淮安市", 
            "code": "320800"
        }, 
        {
            "province": "江苏省", 
            "name": "盐城市", 
            "code": "320900"
        }, 
        {
            "province": "江苏省", 
            "name": "扬州市", 
            "code": "321000"
        }, 
        {
            "province": "江苏省", 
            "name": "镇江市", 
            "code": "321100"
        }, 
        {
            "province": "江苏省", 
            "name": "泰州市", 
            "code": "321200"
        }, 
        {
            "province": "江苏省", 
            "name": "宿迁市", 
            "code": "321300"
        }
    ], 
    "330000": [
        {
            "province": "浙江省", 
            "name": "杭州市", 
            "code": "330100"
        }, 
        {
            "province": "浙江省", 
            "name": "宁波市", 
            "code": "330200"
        }, 
        {
            "province": "浙江省", 
            "name": "温州市", 
            "code": "330300"
        }, 
        {
            "province": "浙江省", 
            "name": "嘉兴市", 
            "code": "330400"
        }, 
        {
            "province": "浙江省", 
            "name": "湖州市", 
            "code": "330500"
        }, 
        {
            "province": "浙江省", 
            "name": "绍兴市", 
            "code": "330600"
        }, 
        {
            "province": "浙江省", 
            "name": "金华市", 
            "code": "330700"
        }, 
        {
            "province": "浙江省", 
            "name": "衢州市", 
            "code": "330800"
        }, 
        {
            "province": "浙江省", 
            "name": "舟山市", 
            "code": "330900"
        }, 
        {
            "province": "浙江省", 
            "name": "台州市", 
            "code": "331000"
        }, 
        {
            "province": "浙江省", 
            "name": "丽水市", 
            "code": "331100"
        }
    ], 
    "340000": [
        {
            "province": "安徽省", 
            "name": "合肥市", 
            "code": "340100"
        }, 
        {
            "province": "安徽省", 
            "name": "芜湖市", 
            "code": "340200"
        }, 
        {
            "province": "安徽省", 
            "name": "蚌埠市", 
            "code": "340300"
        }, 
        {
            "province": "安徽省", 
            "name": "淮南市", 
            "code": "340400"
        }, 
        {
            "province": "安徽省", 
            "name": "马鞍山市", 
            "code": "340500"
        }, 
        {
            "province": "安徽省", 
            "name": "淮北市", 
            "code": "340600"
        }, 
        {
            "province": "安徽省", 
            "name": "铜陵市", 
            "code": "340700"
        }, 
        {
            "province": "安徽省", 
            "name": "安庆市", 
            "code": "340800"
        }, 
        {
            "province": "安徽省", 
            "name": "黄山市", 
            "code": "341000"
        }, 
        {
            "province": "安徽省", 
            "name": "滁州市", 
            "code": "341100"
        }, 
        {
            "province": "安徽省", 
            "name": "阜阳市", 
            "code": "341200"
        }, 
        {
            "province": "安徽省", 
            "name": "宿州市", 
            "code": "341300"
        }, 
        {
            "province": "安徽省", 
            "name": "六安市", 
            "code": "341500"
        }, 
        {
            "province": "安徽省", 
            "name": "亳州市", 
            "code": "341600"
        }, 
        {
            "province": "安徽省", 
            "name": "池州市", 
            "code": "341700"
        }, 
        {
            "province": "安徽省", 
            "name": "宣城市", 
            "code": "341800"
        }
    ], 
    "350000": [
        {
            "province": "福建省", 
            "name": "福州市", 
            "code": "350100"
        }, 
        {
            "province": "福建省", 
            "name": "厦门市", 
            "code": "350200"
        }, 
        {
            "province": "福建省", 
            "name": "莆田市", 
            "code": "350300"
        }, 
        {
            "province": "福建省", 
            "name": "三明市", 
            "code": "350400"
        }, 
        {
            "province": "福建省", 
            "name": "泉州市", 
            "code": "350500"
        }, 
        {
            "province": "福建省", 
            "name": "漳州市", 
            "code": "350600"
        }, 
        {
            "province": "福建省", 
            "name": "南平市", 
            "code": "350700"
        }, 
        {
            "province": "福建省", 
            "name": "龙岩市", 
            "code": "350800"
        }, 
        {
            "province": "福建省", 
            "name": "宁德市", 
            "code": "350900"
        }
    ], 
    "360000": [
        {
            "province": "江西省", 
            "name": "南昌市", 
            "code": "360100"
        }, 
        {
            "province": "江西省", 
            "name": "景德镇市", 
            "code": "360200"
        }, 
        {
            "province": "江西省", 
            "name": "萍乡市", 
            "code": "360300"
        }, 
        {
            "province": "江西省", 
            "name": "九江市", 
            "code": "360400"
        }, 
        {
            "province": "江西省", 
            "name": "新余市", 
            "code": "360500"
        }, 
        {
            "province": "江西省", 
            "name": "鹰潭市", 
            "code": "360600"
        }, 
        {
            "province": "江西省", 
            "name": "赣州市", 
            "code": "360700"
        }, 
        {
            "province": "江西省", 
            "name": "吉安市", 
            "code": "360800"
        }, 
        {
            "province": "江西省", 
            "name": "宜春市", 
            "code": "360900"
        }, 
        {
            "province": "江西省", 
            "name": "抚州市", 
            "code": "361000"
        }, 
        {
            "province": "江西省", 
            "name": "上饶市", 
            "code": "361100"
        }
    ], 
    "370000": [
        {
            "province": "山东省", 
            "name": "济南市", 
            "code": "370100"
        }, 
        {
            "province": "山东省", 
            "name": "青岛市", 
            "code": "370200"
        }, 
        {
            "province": "山东省", 
            "name": "淄博市", 
            "code": "370300"
        }, 
        {
            "province": "山东省", 
            "name": "枣庄市", 
            "code": "370400"
        }, 
        {
            "province": "山东省", 
            "name": "东营市", 
            "code": "370500"
        }, 
        {
            "province": "山东省", 
            "name": "烟台市", 
            "code": "370600"
        }, 
        {
            "province": "山东省", 
            "name": "潍坊市", 
            "code": "370700"
        }, 
        {
            "province": "山东省", 
            "name": "济宁市", 
            "code": "370800"
        }, 
        {
            "province": "山东省", 
            "name": "泰安市", 
            "code": "370900"
        }, 
        {
            "province": "山东省", 
            "name": "威海市", 
            "code": "371000"
        }, 
        {
            "province": "山东省", 
            "name": "日照市", 
            "code": "371100"
        }, 
        {
            "province": "山东省", 
            "name": "莱芜市", 
            "code": "371200"
        }, 
        {
            "province": "山东省", 
            "name": "临沂市", 
            "code": "371300"
        }, 
        {
            "province": "山东省", 
            "name": "德州市", 
            "code": "371400"
        }, 
        {
            "province": "山东省", 
            "name": "聊城市", 
            "code": "371500"
        }, 
        {
            "province": "山东省", 
            "name": "滨州市", 
            "code": "371600"
        }, 
        {
            "province": "山东省", 
            "name": "菏泽市", 
            "code": "371700"
        }
    ], 
    "410000": [
        {
            "province": "河南省", 
            "name": "郑州市", 
            "code": "410100"
        }, 
        {
            "province": "河南省", 
            "name": "开封市", 
            "code": "410200"
        }, 
        {
            "province": "河南省", 
            "name": "洛阳市", 
            "code": "410300"
        }, 
        {
            "province": "河南省", 
            "name": "平顶山市", 
            "code": "410400"
        }, 
        {
            "province": "河南省", 
            "name": "安阳市", 
            "code": "410500"
        }, 
        {
            "province": "河南省", 
            "name": "鹤壁市", 
            "code": "410600"
        }, 
        {
            "province": "河南省", 
            "name": "新乡市", 
            "code": "410700"
        }, 
        {
            "province": "河南省", 
            "name": "焦作市", 
            "code": "410800"
        }, 
        {
            "province": "河南省", 
            "name": "濮阳市", 
            "code": "410900"
        }, 
        {
            "province": "河南省", 
            "name": "许昌市", 
            "code": "411000"
        }, 
        {
            "province": "河南省", 
            "name": "漯河市", 
            "code": "411100"
        }, 
        {
            "province": "河南省", 
            "name": "三门峡市", 
            "code": "411200"
        }, 
        {
            "province": "河南省", 
            "name": "南阳市", 
            "code": "411300"
        }, 
        {
            "province": "河南省", 
            "name": "商丘市", 
            "code": "411400"
        }, 
        {
            "province": "河南省", 
            "name": "信阳市", 
            "code": "411500"
        }, 
        {
            "province": "河南省", 
            "name": "周口市", 
            "code": "411600"
        }, 
        {
            "province": "河南省", 
            "name": "驻马店市", 
            "code": "411700"
        }, 
        {
            "province": "河南省", 
            "name": "省直辖县级行政区划", 
            "code": "419000"
        }
    ], 
    "420000": [
        {
            "province": "湖北省", 
            "name": "武汉市", 
            "code": "420100"
        }, 
        {
            "province": "湖北省", 
            "name": "黄石市", 
            "code": "420200"
        }, 
        {
            "province": "湖北省", 
            "name": "十堰市", 
            "code": "420300"
        }, 
        {
            "province": "湖北省", 
            "name": "宜昌市", 
            "code": "420500"
        }, 
        {
            "province": "湖北省", 
            "name": "襄阳市", 
            "code": "420600"
        }, 
        {
            "province": "湖北省", 
            "name": "鄂州市", 
            "code": "420700"
        }, 
        {
            "province": "湖北省", 
            "name": "荆门市", 
            "code": "420800"
        }, 
        {
            "province": "湖北省", 
            "name": "孝感市", 
            "code": "420900"
        }, 
        {
            "province": "湖北省", 
            "name": "荆州市", 
            "code": "421000"
        }, 
        {
            "province": "湖北省", 
            "name": "黄冈市", 
            "code": "421100"
        }, 
        {
            "province": "湖北省", 
            "name": "咸宁市", 
            "code": "421200"
        }, 
        {
            "province": "湖北省", 
            "name": "随州市", 
            "code": "421300"
        }, 
        {
            "province": "湖北省", 
            "name": "恩施土家族苗族自治州", 
            "code": "422800"
        }, 
        {
            "province": "湖北省", 
            "name": "省直辖县级行政区划", 
            "code": "429000"
        }
    ], 
    "430000": [
        {
            "province": "湖南省", 
            "name": "长沙市", 
            "code": "430100"
        }, 
        {
            "province": "湖南省", 
            "name": "株洲市", 
            "code": "430200"
        }, 
        {
            "province": "湖南省", 
            "name": "湘潭市", 
            "code": "430300"
        }, 
        {
            "province": "湖南省", 
            "name": "衡阳市", 
            "code": "430400"
        }, 
        {
            "province": "湖南省", 
            "name": "邵阳市", 
            "code": "430500"
        }, 
        {
            "province": "湖南省", 
            "name": "岳阳市", 
            "code": "430600"
        }, 
        {
            "province": "湖南省", 
            "name": "常德市", 
            "code": "430700"
        }, 
        {
            "province": "湖南省", 
            "name": "张家界市", 
            "code": "430800"
        }, 
        {
            "province": "湖南省", 
            "name": "益阳市", 
            "code": "430900"
        }, 
        {
            "province": "湖南省", 
            "name": "郴州市", 
            "code": "431000"
        }, 
        {
            "province": "湖南省", 
            "name": "永州市", 
            "code": "431100"
        }, 
        {
            "province": "湖南省", 
            "name": "怀化市", 
            "code": "431200"
        }, 
        {
            "province": "湖南省", 
            "name": "娄底市", 
            "code": "431300"
        }, 
        {
            "province": "湖南省", 
            "name": "湘西土家族苗族自治州", 
            "code": "433100"
        }
    ], 
    "440000": [
        {
            "province": "广东省", 
            "name": "广州市", 
            "code": "440100"
        }, 
        {
            "province": "广东省", 
            "name": "韶关市", 
            "code": "440200"
        }, 
        {
            "province": "广东省", 
            "name": "深圳市", 
            "code": "440300"
        }, 
        {
            "province": "广东省", 
            "name": "珠海市", 
            "code": "440400"
        }, 
        {
            "province": "广东省", 
            "name": "汕头市", 
            "code": "440500"
        }, 
        {
            "province": "广东省", 
            "name": "佛山市", 
            "code": "440600"
        }, 
        {
            "province": "广东省", 
            "name": "江门市", 
            "code": "440700"
        }, 
        {
            "province": "广东省", 
            "name": "湛江市", 
            "code": "440800"
        }, 
        {
            "province": "广东省", 
            "name": "茂名市", 
            "code": "440900"
        }, 
        {
            "province": "广东省", 
            "name": "肇庆市", 
            "code": "441200"
        }, 
        {
            "province": "广东省", 
            "name": "惠州市", 
            "code": "441300"
        }, 
        {
            "province": "广东省", 
            "name": "梅州市", 
            "code": "441400"
        }, 
        {
            "province": "广东省", 
            "name": "汕尾市", 
            "code": "441500"
        }, 
        {
            "province": "广东省", 
            "name": "河源市", 
            "code": "441600"
        }, 
        {
            "province": "广东省", 
            "name": "阳江市", 
            "code": "441700"
        }, 
        {
            "province": "广东省", 
            "name": "清远市", 
            "code": "441800"
        }, 
        {
            "province": "广东省", 
            "name": "东莞市", 
            "code": "441900"
        }, 
        {
            "province": "广东省", 
            "name": "中山市", 
            "code": "442000"
        }, 
        {
            "province": "广东省", 
            "name": "潮州市", 
            "code": "445100"
        }, 
        {
            "province": "广东省", 
            "name": "揭阳市", 
            "code": "445200"
        }, 
        {
            "province": "广东省", 
            "name": "云浮市", 
            "code": "445300"
        }
    ], 
    "450000": [
        {
            "province": "广西壮族自治区", 
            "name": "南宁市", 
            "code": "450100"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "柳州市", 
            "code": "450200"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "桂林市", 
            "code": "450300"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "梧州市", 
            "code": "450400"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "北海市", 
            "code": "450500"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "防城港市", 
            "code": "450600"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "钦州市", 
            "code": "450700"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "贵港市", 
            "code": "450800"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "玉林市", 
            "code": "450900"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "百色市", 
            "code": "451000"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "贺州市", 
            "code": "451100"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "河池市", 
            "code": "451200"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "来宾市", 
            "code": "451300"
        }, 
        {
            "province": "广西壮族自治区", 
            "name": "崇左市", 
            "code": "451400"
        }
    ], 
    "460000": [
        {
            "province": "海南省", 
            "name": "海口市", 
            "code": "460100"
        }, 
        {
            "province": "海南省", 
            "name": "三亚市", 
            "code": "460200"
        }, 
        {
            "province": "海南省", 
            "name": "三沙市", 
            "code": "460300"
        }, 
        {
            "province": "海南省", 
            "name": "儋州市", 
            "code": "460400"
        }, 
        {
            "province": "海南省", 
            "name": "省直辖县级行政区划", 
            "code": "469000"
        }
    ], 
    "500000": [
        {
            "province": "重庆市", 
            "name": "市辖区", 
            "code": "500100"
        }, 
        {
            "province": "重庆市", 
            "name": "县", 
            "code": "500200"
        }
    ], 
    "510000": [
        {
            "province": "四川省", 
            "name": "成都市", 
            "code": "510100"
        }, 
        {
            "province": "四川省", 
            "name": "自贡市", 
            "code": "510300"
        }, 
        {
            "province": "四川省", 
            "name": "攀枝花市", 
            "code": "510400"
        }, 
        {
            "province": "四川省", 
            "name": "泸州市", 
            "code": "510500"
        }, 
        {
            "province": "四川省", 
            "name": "德阳市", 
            "code": "510600"
        }, 
        {
            "province": "四川省", 
            "name": "绵阳市", 
            "code": "510700"
        }, 
        {
            "province": "四川省", 
            "name": "广元市", 
            "code": "510800"
        }, 
        {
            "province": "四川省", 
            "name": "遂宁市", 
            "code": "510900"
        }, 
        {
            "province": "四川省", 
            "name": "内江市", 
            "code": "511000"
        }, 
        {
            "province": "四川省", 
            "name": "乐山市", 
            "code": "511100"
        }, 
        {
            "province": "四川省", 
            "name": "南充市", 
            "code": "511300"
        }, 
        {
            "province": "四川省", 
            "name": "眉山市", 
            "code": "511400"
        }, 
        {
            "province": "四川省", 
            "name": "宜宾市", 
            "code": "511500"
        }, 
        {
            "province": "四川省", 
            "name": "广安市", 
            "code": "511600"
        }, 
        {
            "province": "四川省", 
            "name": "达州市", 
            "code": "511700"
        }, 
        {
            "province": "四川省", 
            "name": "雅安市", 
            "code": "511800"
        }, 
        {
            "province": "四川省", 
            "name": "巴中市", 
            "code": "511900"
        }, 
        {
            "province": "四川省", 
            "name": "资阳市", 
            "code": "512000"
        }, 
        {
            "province": "四川省", 
            "name": "阿坝藏族羌族自治州", 
            "code": "513200"
        }, 
        {
            "province": "四川省", 
            "name": "甘孜藏族自治州", 
            "code": "513300"
        }, 
        {
            "province": "四川省", 
            "name": "凉山彝族自治州", 
            "code": "513400"
        }
    ], 
    "520000": [
        {
            "province": "贵州省", 
            "name": "贵阳市", 
            "code": "520100"
        }, 
        {
            "province": "贵州省", 
            "name": "六盘水市", 
            "code": "520200"
        }, 
        {
            "province": "贵州省", 
            "name": "遵义市", 
            "code": "520300"
        }, 
        {
            "province": "贵州省", 
            "name": "安顺市", 
            "code": "520400"
        }, 
        {
            "province": "贵州省", 
            "name": "毕节市", 
            "code": "520500"
        }, 
        {
            "province": "贵州省", 
            "name": "铜仁市", 
            "code": "520600"
        }, 
        {
            "province": "贵州省", 
            "name": "黔西南布依族苗族自治州", 
            "code": "522300"
        }, 
        {
            "province": "贵州省", 
            "name": "黔东南苗族侗族自治州", 
            "code": "522600"
        }, 
        {
            "province": "贵州省", 
            "name": "黔南布依族苗族自治州", 
            "code": "522700"
        }
    ], 
    "530000": [
        {
            "province": "云南省", 
            "name": "昆明市", 
            "code": "530100"
        }, 
        {
            "province": "云南省", 
            "name": "曲靖市", 
            "code": "530300"
        }, 
        {
            "province": "云南省", 
            "name": "玉溪市", 
            "code": "530400"
        }, 
        {
            "province": "云南省", 
            "name": "保山市", 
            "code": "530500"
        }, 
        {
            "province": "云南省", 
            "name": "昭通市", 
            "code": "530600"
        }, 
        {
            "province": "云南省", 
            "name": "丽江市", 
            "code": "530700"
        }, 
        {
            "province": "云南省", 
            "name": "普洱市", 
            "code": "530800"
        }, 
        {
            "province": "云南省", 
            "name": "临沧市", 
            "code": "530900"
        }, 
        {
            "province": "云南省", 
            "name": "楚雄彝族自治州", 
            "code": "532300"
        }, 
        {
            "province": "云南省", 
            "name": "红河哈尼族彝族自治州", 
            "code": "532500"
        }, 
        {
            "province": "云南省", 
            "name": "文山壮族苗族自治州", 
            "code": "532600"
        }, 
        {
            "province": "云南省", 
            "name": "西双版纳傣族自治州", 
            "code": "532800"
        }, 
        {
            "province": "云南省", 
            "name": "大理白族自治州", 
            "code": "532900"
        }, 
        {
            "province": "云南省", 
            "name": "德宏傣族景颇族自治州", 
            "code": "533100"
        }, 
        {
            "province": "云南省", 
            "name": "怒江傈僳族自治州", 
            "code": "533300"
        }, 
        {
            "province": "云南省", 
            "name": "迪庆藏族自治州", 
            "code": "533400"
        }
    ], 
    "540000": [
        {
            "province": "西藏自治区", 
            "name": "拉萨市", 
            "code": "540100"
        }, 
        {
            "province": "西藏自治区", 
            "name": "日喀则市", 
            "code": "540200"
        }, 
        {
            "province": "西藏自治区", 
            "name": "昌都市", 
            "code": "540300"
        }, 
        {
            "province": "西藏自治区", 
            "name": "林芝市", 
            "code": "540400"
        }, 
        {
            "province": "西藏自治区", 
            "name": "山南市", 
            "code": "540500"
        }, 
        {
            "province": "西藏自治区", 
            "name": "那曲地区", 
            "code": "542400"
        }, 
        {
            "province": "西藏自治区", 
            "name": "阿里地区", 
            "code": "542500"
        }
    ], 
    "610000": [
        {
            "province": "陕西省", 
            "name": "西安市", 
            "code": "610100"
        }, 
        {
            "province": "陕西省", 
            "name": "铜川市", 
            "code": "610200"
        }, 
        {
            "province": "陕西省", 
            "name": "宝鸡市", 
            "code": "610300"
        }, 
        {
            "province": "陕西省", 
            "name": "咸阳市", 
            "code": "610400"
        }, 
        {
            "province": "陕西省", 
            "name": "渭南市", 
            "code": "610500"
        }, 
        {
            "province": "陕西省", 
            "name": "延安市", 
            "code": "610600"
        }, 
        {
            "province": "陕西省", 
            "name": "汉中市", 
            "code": "610700"
        }, 
        {
            "province": "陕西省", 
            "name": "榆林市", 
            "code": "610800"
        }, 
        {
            "province": "陕西省", 
            "name": "安康市", 
            "code": "610900"
        }, 
        {
            "province": "陕西省", 
            "name": "商洛市", 
            "code": "611000"
        }
    ], 
    "620000": [
        {
            "province": "甘肃省", 
            "name": "兰州市", 
            "code": "620100"
        }, 
        {
            "province": "甘肃省", 
            "name": "嘉峪关市", 
            "code": "620200"
        }, 
        {
            "province": "甘肃省", 
            "name": "金昌市", 
            "code": "620300"
        }, 
        {
            "province": "甘肃省", 
            "name": "白银市", 
            "code": "620400"
        }, 
        {
            "province": "甘肃省", 
            "name": "天水市", 
            "code": "620500"
        }, 
        {
            "province": "甘肃省", 
            "name": "武威市", 
            "code": "620600"
        }, 
        {
            "province": "甘肃省", 
            "name": "张掖市", 
            "code": "620700"
        }, 
        {
            "province": "甘肃省", 
            "name": "平凉市", 
            "code": "620800"
        }, 
        {
            "province": "甘肃省", 
            "name": "酒泉市", 
            "code": "620900"
        }, 
        {
            "province": "甘肃省", 
            "name": "庆阳市", 
            "code": "621000"
        }, 
        {
            "province": "甘肃省", 
            "name": "定西市", 
            "code": "621100"
        }, 
        {
            "province": "甘肃省", 
            "name": "陇南市", 
            "code": "621200"
        }, 
        {
            "province": "甘肃省", 
            "name": "临夏回族自治州", 
            "code": "622900"
        }, 
        {
            "province": "甘肃省", 
            "name": "甘南藏族自治州", 
            "code": "623000"
        }
    ], 
    "630000": [
        {
            "province": "青海省", 
            "name": "西宁市", 
            "code": "630100"
        }, 
        {
            "province": "青海省", 
            "name": "海东市", 
            "code": "630200"
        }, 
        {
            "province": "青海省", 
            "name": "海北藏族自治州", 
            "code": "632200"
        }, 
        {
            "province": "青海省", 
            "name": "黄南藏族自治州", 
            "code": "632300"
        }, 
        {
            "province": "青海省", 
            "name": "海南藏族自治州", 
            "code": "632500"
        }, 
        {
            "province": "青海省", 
            "name": "果洛藏族自治州", 
            "code": "632600"
        }, 
        {
            "province": "青海省", 
            "name": "玉树藏族自治州", 
            "code": "632700"
        }, 
        {
            "province": "青海省", 
            "name": "海西蒙古族藏族自治州", 
            "code": "632800"
        }
    ], 
    "640000": [
        {
            "province": "宁夏回族自治区", 
            "name": "银川市", 
            "code": "640100"
        }, 
        {
            "province": "宁夏回族自治区", 
            "name": "石嘴山市", 
            "code": "640200"
        }, 
        {
            "province": "宁夏回族自治区", 
            "name": "吴忠市", 
            "code": "640300"
        }, 
        {
            "province": "宁夏回族自治区", 
            "name": "固原市", 
            "code": "640400"
        }, 
        {
            "province": "宁夏回族自治区", 
            "name": "中卫市", 
            "code": "640500"
        }
    ], 
    "650000": [
        {
            "province": "新疆维吾尔自治区", 
            "name": "乌鲁木齐市", 
            "code": "650100"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "克拉玛依市", 
            "code": "650200"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "吐鲁番市", 
            "code": "650400"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "哈密市", 
            "code": "650500"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "昌吉回族自治州", 
            "code": "652300"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "博尔塔拉蒙古自治州", 
            "code": "652700"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "巴音郭楞蒙古自治州", 
            "code": "652800"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "阿克苏地区", 
            "code": "652900"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "克孜勒苏柯尔克孜自治州", 
            "code": "653000"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "喀什地区", 
            "code": "653100"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "和田地区", 
            "code": "653200"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "伊犁哈萨克自治州", 
            "code": "654000"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "塔城地区", 
            "code": "654200"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "阿勒泰地区", 
            "code": "654300"
        }, 
        {
            "province": "新疆维吾尔自治区", 
            "name": "自治区直辖县级行政区划", 
            "code": "659000"
        }
    ]
}
export {city} 