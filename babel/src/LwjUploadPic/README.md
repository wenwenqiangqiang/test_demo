### LwjUpload 
  
1. 实现快速生成下载链接
2. 使用方式：
   1. 引包
   2. 调用：
   ```

    <LwjUpload {...uploadProps}/>

   ```
   3. 传入参数：
   ```

             maxfile:最大上传数,
             targetUrl：目标地址，必传,
             change：回调

   ```

