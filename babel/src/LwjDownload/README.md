### LwjDownload 
  
1. 实现快速生成下载链接
2. 使用方式：
   1. 引包
   2. 调用：
   ```

    <LwjDownload {...dowloadProps}/>

   ```
   3. 传入参数：
   ```

    title:链接名称，字符串类型，必填；
    targetUrl:下载路径，字符串如："./file/test.txt"，
              或者数组如：["./file/test.txt","./file/test.txt"]，
              或者对象如：{
	           "1.txt":"./file/test.txt",
	           "2.jpg":"./file/test.jpg"
              }，可用于重命名文件
              参数必填,单文件下载传入单条,多文件传入多条

   ```

