# LwjTable

## Demo

```
    <LwjTable
            dataSource={dataSource}
            pagination={pagination}
            loading={loading}            
            columns={columns}
            selectType={selectType}
            dispatch={dispatch}
            pageChangeAction={pageChangeAction}
            onSelectRows={onSelectRows}
            selectedRowKeys={selectedRowKeys}
            onRowClick={onRowClick}
            onRowMouseEnter={onRowMouseEnter}
            
            />  
```

参数解释:
1. 必填项，dataSource,pagination,onChange,loading,columns 请参考官方文档，columns 上可以增加 align(left|right|center) 属性，用于控制该列（居左|居中|居右）
2. 必填项，dispatch，为当前store的dispatch
3. 必填项，pageChangeAction，该模块query的action，如：report/query
4. 非必填,如果列表需要选择,使用selectType属性.(multiple|single)
5. 非必填,align,指定列的对齐格式.(left|right|center)
6. 非必填,onSelectRows,选中列的回调函数。**如果table拥有选择框，则selectedRowKeys为必填。保证state中的selectedRowKeys与LwjTable中的selectedRowKeys同步**
7. 非必填,onRowClick,单击列的回调函数。
8. 非必填,onRowMouseEnter, 当鼠标移上的回调函数







