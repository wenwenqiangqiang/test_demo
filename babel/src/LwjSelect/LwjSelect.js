import {Row, Col,Icon } from 'antd';
import React, {Component} from 'react'
import styles from './LwjSelect.less'
import { relative } from 'path';

export default class LwjSelect extends Component {

  state = {
    selectedItem:[]
  };

  componentDidMount(){

  }

  render() {
    const {
      items,
      itemInch,
      onSelect,
      selected
    } = this.props;

    return (
     <div className={styles.main}>
         {items.map((val,index)=><div onClick={()=>onSelect(val)} key={val.id} style={{lineHeight:itemInch.lineHeight,position: 'relative',textAlign:'center',margin:10,display:'inline-block',width:itemInch.width,height:itemInch.height,border:val.selected?'1px solid #33bdb8':'1px solid #ccc'}}>
             {val.value}
             {selected == val.id? <Icon className={styles.check} style={{fontSize:itemInch.fontSize ? itemInch.fontSize : '14px'}} type="check-square" theme="filled" /> :''}
         </div>)}
     </div>
    );
  }
}
  
LwjSelect.props = {
  
}
  
