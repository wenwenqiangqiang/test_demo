# LwjButton

相比原生ant design的Button,增加了Tooltip的使用

## Demo

```
<LwjButton tooltip="查看"
        type="primary" 
        shape="circle"
        size="small" icon="eye"
        onClick={() => onShowDetail(record)} />
```

参数解释
1. tooltip为文字提示
2. 其他的参数和ant design的button组件一样,请直接看官方文档






