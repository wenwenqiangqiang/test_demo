import React, {Component} from 'react';
import {Breadcrumb,Icon} from 'antd';
import NavLink from './NavLink';
import {breadcrumb} from './BreadcrumbList.less';
import PropTypes from 'prop-types';

const BreadcrumbItem = Breadcrumb.Item;

export default class BreadcrumbList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {breadcrumbItems} = this.props;
        return (
            <div className={breadcrumb}>
                <Breadcrumb separator="===>">
                    {breadcrumbItems.map(([
                        target, linkText, preIcon
                    ], index) => {
                        if (target) {
                            return (
                                <BreadcrumbItem key={index}>
                                <Icon type={preIcon} />
                                    <NavLink target={target} linkText={linkText}/>
                                </BreadcrumbItem>
                            );
                        } else {
                            return (
                                <Breadcrumb.Item key={linkText}> <Icon type={preIcon} />{linkText}</Breadcrumb.Item>
                            )
                        }
                    })}
                </Breadcrumb>
            </div>
        );
    }

}

BreadcrumbList.propTypes={
    breadcrumbItems : PropTypes.array
}