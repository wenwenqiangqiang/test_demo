# LwjTree

## Demo

```
<LwjTree dataSource={dataSource}/>
```

dataSource为数据来源,
数据格式为:

```
[{
		"id": "0",
		"updateTime": 1499225724466,
		"createTime": 1499225724466,
		"version": 0,
		"enabled": false,
		"name": "资源组",
		"priority": -1,
		"description": "资源组根节点"
	}, {
		"id": "8e0ee2d8b84e45459f837d77a873144b",
		"updateTime": 1510884680135,
		"createTime": 1510884680135,
		"version": 0,
		"enabled": false,
		"name": "财务管理",
		"code": "000001",
		"parentId": "0",
		"priority": 0,
		"description": "财务管理"
	}]
```

可选参数:

```
multiple : 是否多选,
checkable : 是否可以选择,
defaultExpandAll : 是否默认展开所有,
onCheck : 选中方法,返回选中的节点数据,
onSelect : 点击方法,返回点击的节点数据,
canEdit : 是否可编辑,
onAdd : 添加节点的方法, 
onDelete : 删除节点的方法
```







