import React, { useEffect, useState } from 'react';
import { Input, Select } from 'antd';
import PropTypes from 'prop-types';
const { Option } = Select;

const LwjAreaName = ({ value, onChange }) => {
  const selectAfterPrvince = (
    <Select defaultValue="Prvince" style={{ width: 50 }}>
      <Option value="Prvince">省</Option>
    </Select>
  );
  const selectAfterCity = (
    <Select defaultValue="City" style={{ width: 50 }}>
      <Option value="City">市</Option>
    </Select>
  );
  const selectAfterArea = (
    <Select defaultValue="Area" style={{ width: 50 }}>
      <Option value="Area">区</Option>
    </Select>
  );

  const [defaultValue, CHANGDEFAULTVALUE] = useState({
    provinceName: '',
    cityName: '',
    districtName: '',
  });

  useEffect(() => {
    if (value) {
      CHANGDEFAULTVALUE({
        ...value,
      });
    }
  }, value);

  const preChange = (e, name) => {
    defaultValue[name] = e.target.value;
    onChange({
      ...defaultValue,
    });
  };

  return (
    <div style={{ display: 'flex' }}>
      <Input
        onChange={e => preChange(e, 'provinceName')}
        addonAfter={selectAfterPrvince}
        defaultValue={defaultValue.provinceName}
      />
      <Input
        onChange={e => preChange(e, 'cityName')}
        addonAfter={selectAfterCity}
        defaultValue={defaultValue.cityName}
      />
      <Input
        onChange={e => preChange(e, 'districtName')}
        addonAfter={selectAfterArea}
        defaultValue={defaultValue.districtName}
      />
    </div>
  );
};

LwjAreaName.propTypes = {
  onChange: PropTypes.func,
};
export default LwjAreaName;
