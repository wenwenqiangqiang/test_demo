import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { FilterItem } from 'components'
import {
  Form,
  Button,
  Row,
  Col,
  Divider,
  Input,
  Select,
  DatePicker,
  Icon,
  TreeSelect,
  InputNumber
} from 'antd'

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

const filterStyle = {
  style: {
    marginBottom: 16
  }
};

const formItemLayout = {
  labelCol: {
    span: 6
  },
  wrapperCol: {
    span: 18
  }
};
const Option = Select.Option;

const OrderFilterExpand = ({
  filterItems,
  onFilterChange,
  form: {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue
  },
  noExpand
}) => {
  const handleFields = (fields) => {
    return fields
  }

  const handleSubmit = () => {
    let fields = getFieldsValue()
    fields = handleFields(fields)

    onFilterChange(fields)
  }

  const handleReset = () => {
    const fields = getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    setFieldsValue(fields)
    handleSubmit()
  }

  const handleChange = (key, values) => {
    let fields = getFieldsValue()
    fields[key] = values
    fields = handleFields(fields)
    onFilterChange(fields)
  }

  const dateChange = (date, dateString) => {
    // console.log(date, dateString);
  }
  const getFilterItemCol = (filterItems) => {
    const result = filterItems.map((filterItem, index) => <Col key={index} span={8}>
      <div id="parent">
        <FormItem label={filterItem.title} {...formItemLayout}>
          {filterItem.type == 'input' && getFieldDecorator(`${filterItem.id}`, {})(<Input placeholder={filterItem.placeholder} onPressEnter={handleSubmit} />)}
          {filterItem.type === 'inputNumber' && getFieldDecorator(`${filterItem.id}`, {})(<InputNumber
            placeholder={filterItem.placeholder} onPressEnter={handleSubmit} step={0.01} precision={2} style={{width: '100%'}}/>)}
          {filterItem.type == 'select' && getFieldDecorator(`${filterItem.id}`, { initialValue: filterItem.defaultValue })(
            <Select
              initialValue={filterItem.defaultValue}
              style={{
                width: '100%'
              }}
              showSearch={filterItem.showSearch}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              getPopupContainer={() => document.getElementById("parent")}
              >
              {getOptions(filterItem.options)}
            </Select>
          )}
          {filterItem.type === 'treeSelect' && getFieldDecorator(`${filterItem.id}`, { initialValue: filterItem.defaultValue })(
            <TreeSelect
              initialValue={filterItem.defaultValue}
              treeData={filterItem.options}
              treeCheckable={filterItem.checkable ? filterItem.checkable : false}
              maxTagCount={filterItem.maxTagCount ? filterItem.maxTagCount : 1}
              showCheckedStrategy={SHOW_PARENT}
              searchPlaceholder={filterItem.searchPlaceholder}
              placeholder={filterItem.placeholder}
              style={{
                width: '100%'
              }}
              getPopupContainer={() => document.getElementById("parent")}
            />
          )}
          {filterItem.type == 'dateRule' && getFieldDecorator(`${filterItem.id}`, {})(<RangePicker
            style={{
              width: '100%'
            }}
            showTime
            initialValue={[
              moment(filterItem.bdTime || '', filterItem.dateFormat || "YYYY/MM/DD"),
              moment(filterItem.edTime || '', filterItem.dateFormat || "YYYY/MM/DD")
            ]}
            format={filterItem.dateFormat || "YYYY/MM/DD"}
            onChange={dateChange} />)}
          {filterItem.type == 'date' && getFieldDecorator(`${filterItem.id}`, {})(<DatePicker
            style={{
              width: '100%'
            }}
            onChange={dateChange} />)}
        </FormItem>
      </div>
    </Col>);
    return ((result));
  }

  // 处理下拉选项
  const getOptions = (value) => value.map(option => {
    return <Option value={option.value} key={option.value}>{option.name}</Option>
  })

  const filterItemCols = getFilterItemCol(filterItems);

  const showState = true;

  return (
    <Row gutter={24} {...filterStyle}>
      {filterItemCols}
      <Col span={8} style={{ textAlign: 'left', marginTop: 2 }}>
        <Button type="primary" onClick={handleSubmit}>查询</Button>
      </Col>
    </Row>
  )
}

OrderFilterExpand.propTypes = {
  form: PropTypes.object,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func
}

export default Form.create({
    onFieldsChange(props, changedFields){
        if(props.onChange){
            props.onChange(changedFields);
        }
    }
})(OrderFilterExpand)
