import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { FilterItem } from 'components'
import {
  Form,
  Button,
  Row,
  Col,
  Divider,
  Input,
  Select,
  DatePicker,
  Icon,
  TreeSelect,
  InputNumber
} from 'antd'

const FormItem = Form.Item
const { RangePicker } = DatePicker
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const InputGroup = Input.Group

const filterStyle = {
  style: {
    marginBottom: 16
  }
}

const formItemLayout = {
  labelCol: {
    span: 6
  },
  wrapperCol: {
    span: 18
  }
}
const Option = Select.Option

const GeneralFilter = ({
  filterItems,
  onFilterChange,
  isExpand,
  form: {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue
  },
  expand,
  noExpand,
  col = 6
}) => {
  let filterItemsDealed
  if (isExpand) {
    filterItemsDealed = filterItems
  } else {
    if (filterItems.length > 3) {
      filterItemsDealed = filterItems.slice(0, 2)
    } else {
      filterItemsDealed = filterItems
    }
  }

  const handleFields = (fields) => {
    for (let key in fields) {
      let field = fields[key]
      // 删除请求中为空的对象
      if (!field || (_.isString(field) && field.replace(/(^\s*)|(\s*$)/g, '').length == 0)
        || _.isUndefined(field)
        || _.isNull(field)
        || (_.isArray(field) && field.length === 0)) {
        delete fields[key]
        continue
      }
      // 处理日期类型
      if (_.endsWith(key, 'Time') && _.isArray(fields[key]) && fields[key].length > 0) {
        let timeStartValue = fields[key][0].valueOf()
        let timeEndValue = fields[key][1].valueOf()
        let timeStartKey = key + 'Start'
        let timeEndKey = key + 'End'
        fields[timeStartKey] = timeStartValue
        fields[timeEndKey] = timeEndValue
        delete fields[key]
      }
    }
    return fields
  }

  const handleSubmit = () => {
    let fields = getFieldsValue()
    fields = handleFields(fields)
    onFilterChange(fields)
  }

  const handleReset = () => {
    const fields = getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    setFieldsValue(fields)
    handleSubmit()
  }

  const handleChange = (key, values) => {
    let fields = getFieldsValue()
    fields[key] = values
    fields = handleFields(fields)
    onFilterChange(fields)
  }

  const dateChange = (date, dateString) => {
    console.log(date, dateString)
  }
  const getFilterItemCol = (filterItems) => {
    const result = filterItems.map((filterItem, index) => <Col key={index} span={col}>
      <div id="parent">
        <FormItem label={filterItem.title} {...formItemLayout}>
          {filterItem.type === 'input' && getFieldDecorator(`${filterItem.id}`, {})(<Input
            placeholder={filterItem.placeholder} onPressEnter={handleSubmit} />)}
          {filterItem.type === 'inputNumber' && getFieldDecorator(`${filterItem.id}`, {})(<InputNumber
            placeholder={filterItem.placeholder} onPressEnter={handleSubmit} step={0.01} precision={2} style={{width: '100%'}}/>)}
          {filterItem.type === 'select' && getFieldDecorator(`${filterItem.id}`, { initialValue: filterItem.defaultValue })(
            <Select
              initialValue={filterItem.defaultValue}
              style={{
                width: '100%'
              }}
              showSearch={filterItem.showSearch}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              getPopupContainer={() => document.getElementById("parent")}
            >
              {getOptions(filterItem.options)}
            </Select>
          )}
          {filterItem.type === 'treeSelect' && getFieldDecorator(`${filterItem.id}`, { initialValue: filterItem.defaultValue })(
            <TreeSelect
              initialValue={filterItem.defaultValue}
              treeData={filterItem.options}
              treeCheckable={filterItem.checkable ? filterItem.checkable : false}
              maxTagCount={filterItem.maxTagCount ? filterItem.maxTagCount : 1}
              showCheckedStrategy={SHOW_PARENT}
              searchPlaceholder={filterItem.searchPlaceholder}
              placeholder={filterItem.placeholder}
              style={{
                width: '100%'
              }}
              getPopupContainer={() => document.getElementById("parent")}
            />
          )}
          {filterItem.type === 'dateRule' && getFieldDecorator(`${filterItem.id}`, {})(<RangePicker
            style={{
              width: '100%'
            }}
            showTime
            initialValue={[
              moment(filterItem.bdTime || '', filterItem.dateFormat || 'YYYY/MM/DD'),
              moment(filterItem.edTime || '', filterItem.dateFormat || 'YYYY/MM/DD')
            ]}
            format={filterItem.dateFormat || 'YYYY/MM/DD'}
            onChange={dateChange} />)}
          {filterItem.type === 'date' && getFieldDecorator(`${filterItem.id}`, {})(<DatePicker
            style={{
              width: '100%'
            }}
            onChange={dateChange} />)}
             {filterItem.type === 'numberBetween' && (
              <InputGroup compact    style={{
                width: '100%',
                display:'flex'
              }}>
                {getFieldDecorator(`${filterItem.id1}`, {})(
                  <Input style={{ textAlign: 'center' }} placeholder={filterItem.placeholder1 ? filterItem.placeholder1 : ''} />
                )}
                <Input
                  style={{
                    width: 30,
                    borderLeft: 0,
                    pointerEvents: 'none',
                    backgroundColor: '#fff',
                  }}
                  placeholder="~"
                  disabled
                />
                {getFieldDecorator(`${filterItem.id2}`, {})(
                  <Input
                    style={{ textAlign: 'center', borderLeft: 0 }}
                    placeholder={filterItem.placeholder2 ? filterItem.placeholder2 : ''}
                  />
                )}
              </InputGroup>
            )}
        </FormItem>
      </div>
    </Col>)
    return ((result))
  }

  // 处理下拉选项
  const getOptions = (value) => value.map(option => {
    return <Option value={option.value} key={option.value}>{option.name}</Option>
  })

  const filterItemCols = getFilterItemCol(filterItemsDealed)

  return (
    <Row gutter={24} {...filterStyle}>
      {filterItemCols}
      <Col span={6} style={{
        textAlign: 'left'
      }}>
        <Button type="primary" onClick={handleSubmit}>查询</Button>
        <Divider type="vertical" />
        <Button type="ghost" onClick={handleReset}>重置</Button>
      </Col>
    </Row>
  )
}

GeneralFilter.propTypes = {
  form: PropTypes.object,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func
}

export default Form.create({
    onFieldsChange(props, changedFields){
        if(props.onChange){
            props.onChange(changedFields);
        }
    }
})(GeneralFilter)
