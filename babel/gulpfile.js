var gulp = require('gulp');
var del = require('del');
var path = require('path');

gulp.task('clean:lwj', async function (cb) {
    await del([
        path.join(__dirname, './lib') + '/**/*'
    ], cb);
});

gulp.task('copy-lwj', async () =>
    await gulp.src([path.join(__dirname, 'src') + '/**/*'])
        .pipe(gulp.dest(path.join(__dirname, './lib')))
);

gulp.task('default', gulp.series('clean:lwj', 'copy-lwj'));
