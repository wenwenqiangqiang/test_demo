"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ChartCard", {
  enumerable: true,
  get: function get() {
    return _ChartCard["default"];
  }
});
Object.defineProperty(exports, "MiniProgress", {
  enumerable: true,
  get: function get() {
    return _MiniProgress["default"];
  }
});
exports["default"] = void 0;

var _ChartCard = _interopRequireDefault(require("./ChartCard"));

var _MiniProgress = _interopRequireDefault(require("./MiniProgress"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Charts = {
  MiniProgress: _MiniProgress["default"],
  ChartCard: _ChartCard["default"]
};
exports["default"] = Charts;