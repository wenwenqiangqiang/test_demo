"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _antd = require("antd");

var _index = _interopRequireDefault(require("./index.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var MiniProgress = function MiniProgress(_ref) {
  var target = _ref.target,
      _ref$color = _ref.color,
      color = _ref$color === void 0 ? 'rgb(19, 194, 194)' : _ref$color,
      strokeWidth = _ref.strokeWidth,
      percent = _ref.percent;
  return _react["default"].createElement("div", {
    className: _index["default"].miniProgress
  }, _react["default"].createElement(_antd.Tooltip, {
    title: "\u76EE\u6807\u503C: ".concat(target, "%")
  }, _react["default"].createElement("div", {
    className: _index["default"].target,
    style: {
      left: target ? "".concat(target, "%") : null
    }
  })), _react["default"].createElement("div", {
    className: _index["default"].progressWrap
  }, _react["default"].createElement("div", {
    className: _index["default"].progress,
    style: {
      backgroundColor: color || null,
      width: percent ? "".concat(percent, "%") : null,
      height: strokeWidth || null
    }
  })));
};

var _default = MiniProgress;
exports["default"] = _default;