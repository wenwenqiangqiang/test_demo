"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAddressData = void 0;

var _antd = require("antd");

var Option = _antd.Select.Option;
var FormItem = _antd.Form.Item;
var selectAfterPrvince = React.createElement(_antd.Select, {
  defaultValue: "Prvince",
  style: {
    width: 30
  },
  size: "small",
  disabled: true,
  showArrow: false
}, React.createElement(Option, {
  value: "Prvince",
  style: {
    margin: 0
  }
}, "\u7701"));
var selectAfterCity = React.createElement(_antd.Select, {
  defaultValue: "City",
  style: {
    width: 30
  },
  size: "small",
  disabled: true,
  showArrow: false
}, React.createElement(Option, {
  value: "City",
  style: {
    margin: 0
  }
}, "\u5E02"));
var selectAfterArea = React.createElement(_antd.Select, {
  defaultValue: "Area",
  style: {
    width: 30
  },
  size: "small",
  disabled: true,
  showArrow: false
}, React.createElement(Option, {
  value: "Area",
  style: {
    margin: 0
  }
}, "\u533A"));

var getAddressData = function getAddressData(getFieldDecorator, provinceName, cityName, districtName, showprovinceName, showcityName, showdistrictName, handleChange) {
  var isrequired = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : true;
  return React.createElement("div", {
    style: {
      display: 'flex'
    }
  }, React.createElement(FormItem, {
    style: {
      marginBottom: 0
    }
  }, getFieldDecorator(showprovinceName, {
    rules: [{
      required: isrequired,
      message: '请输入省',
      whitespace: true
    }],
    initialValue: provinceName
  })(React.createElement(_antd.Input, {
    onChange: handleChange,
    placeholder: "\u8BF7\u8F93\u5165\u7701"
  }))), React.createElement(FormItem, {
    style: {
      marginBottom: 0
    }
  }, getFieldDecorator(showcityName, {
    rules: [{
      required: isrequired,
      message: '请输入市',
      whitespace: true
    }],
    initialValue: cityName
  })(React.createElement(_antd.Input, {
    onChange: handleChange,
    placeholder: "\u8BF7\u8F93\u5165\u5E02"
  }))), React.createElement(FormItem, {
    style: {
      marginBottom: 0
    }
  }, getFieldDecorator(showdistrictName, {
    rules: [{
      required: isrequired,
      message: '请输入区',
      whitespace: true
    }],
    initialValue: districtName
  })(React.createElement(_antd.Input, {
    onChange: handleChange,
    placeholder: "\u8BF7\u8F93\u5165\u533A"
  }))));
};

exports.getAddressData = getAddressData;