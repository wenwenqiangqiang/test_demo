"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _antd = require("antd");

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Option = _antd.Select.Option;
var FormItem = _antd.Form.Item;

var LwjAreaName = function LwjAreaName(_ref) {
  var value = _ref.value,
      _ref$form = _ref.form,
      getFieldDecorator = _ref$form.getFieldDecorator,
      validateFieldsAndScroll = _ref$form.validateFieldsAndScroll,
      setFieldsValue = _ref$form.setFieldsValue,
      getFieldsValue = _ref$form.getFieldsValue;

  var selectAfterPrvince = _react["default"].createElement(_antd.Select, {
    defaultValue: "Prvince",
    style: {
      width: 50
    }
  }, _react["default"].createElement(Option, {
    value: "Prvince"
  }, "\u7701"));

  var selectAfterCity = _react["default"].createElement(_antd.Select, {
    defaultValue: "City",
    style: {
      width: 50
    }
  }, _react["default"].createElement(Option, {
    value: "City"
  }, "\u5E02"));

  var selectAfterArea = _react["default"].createElement(_antd.Select, {
    defaultValue: "Area",
    style: {
      width: 50
    }
  }, _react["default"].createElement(Option, {
    value: "Area"
  }, "\u533A"));

  var preChange = function preChange() {
    validateFieldsAndScroll(function (err, values) {
      if (!err) {
        onChange(values);
      }
    });
  };

  var _useState = (0, _react.useState)({
    provinceName: '',
    cityName: '',
    districtName: ''
  }),
      _useState2 = _slicedToArray(_useState, 2),
      defaultValue = _useState2[0],
      CHANGDEFAULTVALUE = _useState2[1]; // useEffect(() => {
  //   CHANGDEFAULTVALUE(value);
  // }, value);
  // console.log(value, '%%%%%%%%%%%%%%%%%%%%');


  return _react["default"].createElement(_antd.Form, null, _react["default"].createElement("div", {
    style: {
      display: 'flex'
    }
  }, _react["default"].createElement(FormItem, {
    style: {
      marginBottom: 0
    }
  }, getFieldDecorator('provinceName', {
    rules: [{
      required: true,
      message: '请输入省',
      whitespace: true
    }],
    initialValue: defaultValue && defaultValue.provinceName ? defaultValue.provinceName : ''
  })(_react["default"].createElement(_antd.Input, {
    onChange: preChange,
    addonAfter: selectAfterPrvince
  }))), _react["default"].createElement(FormItem, {
    style: {
      marginBottom: 0
    }
  }, getFieldDecorator('cityName', {
    rules: [{
      required: true,
      message: '请输入市',
      whitespace: true
    }],
    initialValue: defaultValue && defaultValue.cityName ? defaultValue.cityName : ''
  })(_react["default"].createElement(_antd.Input, {
    onChange: preChange,
    addonAfter: selectAfterCity
  }))), _react["default"].createElement(FormItem, {
    style: {
      marginBottom: 0
    }
  }, getFieldDecorator('districtName', {
    rules: [{
      required: true,
      message: '请输入区',
      whitespace: true
    }],
    initialValue: defaultValue && defaultValue.districtName ? defaultValue.districtName : ''
  })(_react["default"].createElement(_antd.Input, {
    onChange: preChange,
    addonAfter: selectAfterArea
  })))));
};

LwjAreaName.propTypes = {
  onChange: _propTypes["default"].func
};

var _default = _antd.Form.create()(LwjAreaName);

exports["default"] = _default;