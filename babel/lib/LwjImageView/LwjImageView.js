"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _ReactCrop = _interopRequireWildcard(require("./ImageCrop/ReactCrop"));

require("./LwjImageView.less");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LwjImageView =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(LwjImageView, _PureComponent);

  function LwjImageView(props) {
    var _this;

    _classCallCheck(this, LwjImageView);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LwjImageView).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "onCropChange", function (crop, pixelCrop) {
      _this.setState({
        crop: crop,
        pixelCrop: pixelCrop
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onImageLoaded", function (image) {
      _this.setState({
        // crop: makeAspectCrop({     x: 0,     y: 0,     aspect: 10 / 4,     width: 25
        // }, image.naturalWidth / image.naturalHeight),
        image: image
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onCropComplete", function (crop, pixelCrop) {
      var img = new Image();
      img.src = _this.props.dataUrl;

      _this.test(img, pixelCrop, 'LwjNewPic');

      if (_this.props.getCropData) {
        _this.props.getCropData(pixelCrop);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "getCroppedImg", function (image, pixelCrop, fileName) {
      var canvas = document.createElement('canvas');
      canvas.width = pixelCrop.width;
      canvas.height = pixelCrop.height;
      var ctx = canvas.getContext('2d');
      ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);
      /**
           * As Base64 string const base64Image = canvas.toDataURL('image/jpeg');
           *    As ablob
           */

      return new Promise(function (resolve, reject) {
        canvas.toBlob(function (file) {
          file.name = fileName;
          resolve(file);
        }, 'image/jpeg');
      });
    });

    _this.state = {
      crop: {
        x: 0,
        y: 0 // aspect: 16 / 9,

      },
      // 最大高度 maxHeight: 80
      pixelCrop: {},
      isOK: false
    };
    return _this;
  }

  _createClass(LwjImageView, [{
    key: "test",
    value: function test(image, pixelCrop, returnedFileName) {
      var croppedImg;
      return regeneratorRuntime.async(function test$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return regeneratorRuntime.awrap(this.getCroppedImg(image, pixelCrop, returnedFileName));

            case 2:
              croppedImg = _context.sent;

              if (this.props.getFile) {
                this.props.getFile(croppedImg);
              }

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "render",
    value: function render() {
      var dataUrl = this.props.dataUrl; // const dataUrlCheck = (value) => {     let image = new Image(); image.onload =
      // ()=> {         let width = this.width;         let height = this.height;
      // if (width > 1920 || height > 1080) { this.props.isOK=true; console.log(999);
      //            return
      // 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAEUlEQV
      // Q ImWP8DwQMQMDEAAUAPfgEAJtlyeIAAAAASUVORK5CYII=';         } else {
      // this.state.isOK=false;             return value;         }     } image.src =
      // value; }

      return _react["default"].createElement("div", null, _react["default"].createElement(_ReactCrop["default"], _extends({}, this.state, {
        src: dataUrl,
        onImageLoaded: this.onImageLoaded,
        onComplete: this.onCropComplete,
        onChange: this.onCropChange
      })), " ", (this.state.pixelCrop.width || this.state.pixelCrop.height) && _react["default"].createElement("div", null, this.state.pixelCrop.width, "X", this.state.pixelCrop.height));
    }
  }]);

  return LwjImageView;
}(_react.PureComponent);

exports["default"] = LwjImageView;
LwjImageView.propTypes = {
  getCropData: _propTypes["default"].func,
  getFile: _propTypes["default"].func
};