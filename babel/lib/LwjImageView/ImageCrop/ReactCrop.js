"use strict";

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Feature detection
// https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#Improving_scrolling_performance_with_passive_listeners
var passiveSupported = false;

try {
  window.addEventListener('test', null, Object.defineProperty({}, 'passive', {
    get: function get() {
      passiveSupported = true;
      return true;
    }
  }));
} catch (err) {} // eslint-disable-line no-empty


var EMPTY_GIF = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

function getElementOffset(el) {
  var rect = el.getBoundingClientRect();
  var docEl = document.documentElement;
  var rectTop = rect.top + window.pageYOffset - docEl.clientTop;
  var rectLeft = rect.left + window.pageXOffset - docEl.clientLeft;
  return {
    top: rectTop,
    left: rectLeft
  };
}

function getClientPos(e) {
  var pageX;
  var pageY;

  if (e.touches) {
    pageX = e.touches[0].pageX;
    pageY = e.touches[0].pageY;
  } else {
    pageX = e.pageX;
    pageY = e.pageY;
  }

  return {
    x: pageX,
    y: pageY
  };
}

function clamp(num, min, max) {
  return Math.min(Math.max(num, min), max);
}

function isCropValid(crop) {
  return crop && crop.width && crop.height;
}

function inverseOrd(ord) {
  var inversedOrd;
  if (ord === 'n') inversedOrd = 's';else if (ord === 'ne') inversedOrd = 'sw';else if (ord === 'e') inversedOrd = 'w';else if (ord === 'se') inversedOrd = 'nw';else if (ord === 's') inversedOrd = 'n';else if (ord === 'sw') inversedOrd = 'ne';else if (ord === 'w') inversedOrd = 'e';else if (ord === 'nw') inversedOrd = 'se';
  return inversedOrd;
}

function makeAspectCrop(crop, imageAspect) {
  var completeCrop = _objectSpread({}, crop);

  if (crop.width) {
    completeCrop.height = crop.width / crop.aspect * imageAspect;
  }

  if (crop.height) {
    completeCrop.width = (completeCrop.height || crop.height) * (crop.aspect / imageAspect);
  }

  if (crop.y + (completeCrop.height || crop.height) > 100) {
    completeCrop.height = 100 - crop.y;
    completeCrop.width = completeCrop.height * crop.aspect / imageAspect;
  }

  if (crop.x + (completeCrop.width || crop.width) > 100) {
    completeCrop.width = 100 - crop.x;
    completeCrop.height = completeCrop.width / crop.aspect * imageAspect;
  }

  return completeCrop;
}

function getPixelCrop(image, percentCrop) {
  var x = Math.round(image.naturalWidth * (percentCrop.x / 100));
  var y = Math.round(image.naturalHeight * (percentCrop.y / 100));
  var width = Math.round(image.naturalWidth * (percentCrop.width / 100));
  var height = Math.round(image.naturalHeight * (percentCrop.height / 100));
  return {
    x: x,
    y: y,
    // Clamp width and height so rounding doesn't cause the crop to exceed bounds.
    width: clamp(width, 0, image.naturalWidth - x),
    height: clamp(height, 0, image.naturalHeight - y)
  };
}

function containCrop(crop, imageAspect) {
  var contained = _objectSpread({}, crop); // Don't let the crop grow on the opposite side when hitting an x image boundary.


  var cropXAdjusted = false;

  if (contained.x + contained.width > 100) {
    contained.width = crop.width + (100 - (crop.x + crop.width));
    contained.x = crop.x + (100 - (crop.x + contained.width));
    cropXAdjusted = true;
  } else if (contained.x < 0) {
    contained.width = crop.x + crop.width;
    contained.x = 0;
    cropXAdjusted = true;
  }

  if (cropXAdjusted && crop.aspect) {
    // Adjust height to the resized width to maintain aspect.
    contained.height = contained.width / crop.aspect * imageAspect; // If sizing in up direction we need to pin Y at the point it
    // would be at the boundary.

    if (contained.y < crop.y) {
      contained.y = crop.y + (crop.height - contained.height);
    }
  } // Don't let the crop grow on the opposite side when hitting a y image boundary.


  var cropYAdjusted = false;

  if (contained.y + contained.height > 100) {
    contained.height = crop.height + (100 - (crop.y + crop.height));
    contained.y = crop.y + (100 - (crop.y + contained.height));
    cropYAdjusted = true;
  } else if (contained.y < 0) {
    contained.height = crop.y + crop.height;
    contained.y = 0;
    cropYAdjusted = true;
  }

  if (cropYAdjusted && crop.aspect) {
    // Adjust width to the resized height to maintain aspect.
    contained.width = contained.height * crop.aspect / imageAspect; // If sizing in up direction we need to pin X at the point it
    // would be at the boundary.

    if (contained.x < crop.x) {
      contained.x = crop.x + (crop.width - contained.width);
    }
  }

  return contained;
}

var ReactCrop =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(ReactCrop, _PureComponent);

  function ReactCrop() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, ReactCrop);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(ReactCrop)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {});

    _defineProperty(_assertThisInitialized(_this), "onCropMouseTouchDown", function (e) {
      var _this$props = _this.props,
          crop = _this$props.crop,
          disabled = _this$props.disabled;

      if (disabled) {
        return;
      }

      e.preventDefault(); // Stop drag selection.

      var clientPos = getClientPos(e); // Focus for detecting keypress.

      _this.componentRef.focus();

      var ord = e.target.dataset.ord;
      var xInversed = ord === 'nw' || ord === 'w' || ord === 'sw';
      var yInversed = ord === 'nw' || ord === 'n' || ord === 'ne';
      var cropOffset;

      if (crop.aspect) {
        cropOffset = getElementOffset(_this.cropSelectRef);
      }

      _this.evData = {
        clientStartX: clientPos.x,
        clientStartY: clientPos.y,
        cropStartWidth: crop.width,
        cropStartHeight: crop.height,
        cropStartX: xInversed ? crop.x + crop.width : crop.x,
        cropStartY: yInversed ? crop.y + crop.height : crop.y,
        xInversed: xInversed,
        yInversed: yInversed,
        xCrossOver: xInversed,
        yCrossOver: yInversed,
        startXCrossOver: xInversed,
        startYCrossOver: yInversed,
        isResize: e.target !== _this.cropSelectRef,
        ord: ord,
        cropOffset: cropOffset
      };
      _this.mouseDownOnCrop = true;

      _this.setState({
        cropIsActive: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onComponentMouseTouchDown", function (e) {
      var _this$props2 = _this.props,
          crop = _this$props2.crop,
          disabled = _this$props2.disabled,
          keepSelection = _this$props2.keepSelection,
          onChange = _this$props2.onChange;

      if (e.target !== _this.imageRef) {
        return;
      }

      if (disabled || keepSelection && isCropValid(crop)) {
        return;
      }

      e.preventDefault(); // Stop drag selection.

      var clientPos = getClientPos(e); // Focus for detecting keypress.

      _this.componentRef.focus();

      var imageOffset = getElementOffset(_this.imageRef);
      var xPc = (clientPos.x - imageOffset.left) / _this.imageRef.width * 100;
      var yPc = (clientPos.y - imageOffset.top) / _this.imageRef.height * 100;
      var nextCrop = {
        aspect: crop ? crop.aspect : undefined,
        x: xPc,
        y: yPc,
        width: 0,
        height: 0
      };
      _this.evData = {
        clientStartX: clientPos.x,
        clientStartY: clientPos.y,
        cropStartWidth: nextCrop.width,
        cropStartHeight: nextCrop.height,
        cropStartX: nextCrop.x,
        cropStartY: nextCrop.y,
        xInversed: false,
        yInversed: false,
        xCrossOver: false,
        yCrossOver: false,
        startXCrossOver: false,
        startYCrossOver: false,
        isResize: true,
        ord: 'nw'
      };
      _this.mouseDownOnCrop = true;
      onChange(nextCrop, getPixelCrop(_this.imageRef, nextCrop));

      _this.setState({
        cropIsActive: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onDocMouseTouchMove", function (e) {
      var _this$props3 = _this.props,
          crop = _this$props3.crop,
          disabled = _this$props3.disabled,
          onChange = _this$props3.onChange,
          onDragStart = _this$props3.onDragStart;
      onDragStart();

      if (disabled) {
        return;
      }

      if (!_this.mouseDownOnCrop) {
        return;
      }

      e.preventDefault(); // Stop drag selection.

      var _assertThisInitialize = _assertThisInitialized(_this),
          evData = _assertThisInitialize.evData;

      var clientPos = getClientPos(e);

      if (evData.isResize && crop.aspect && evData.cropOffset) {
        clientPos.y = _this.straightenYPath(clientPos.x);
      }

      var xDiffPx = clientPos.x - evData.clientStartX;
      evData.xDiffPc = xDiffPx / _this.imageRef.width * 100;
      var yDiffPx = clientPos.y - evData.clientStartY;
      evData.yDiffPc = yDiffPx / _this.imageRef.height * 100;
      var nextCrop;

      if (evData.isResize) {
        nextCrop = _this.resizeCrop();
      } else {
        nextCrop = _this.dragCrop();
      }

      onChange(nextCrop, getPixelCrop(_this.imageRef, nextCrop));
    });

    _defineProperty(_assertThisInitialized(_this), "onComponentKeyDown", function (e) {
      var _this$props4 = _this.props,
          crop = _this$props4.crop,
          disabled = _this$props4.disabled,
          onChange = _this$props4.onChange,
          onComplete = _this$props4.onComplete;

      if (disabled) {
        return;
      }

      var keyCode = e.which;
      var nudged = false;

      if (!isCropValid(crop)) {
        return;
      }

      var nextCrop = _this.makeNewCrop();

      if (keyCode === ReactCrop.arrowKey.left) {
        nextCrop.x -= ReactCrop.nudgeStep;
        nudged = true;
      } else if (keyCode === ReactCrop.arrowKey.right) {
        nextCrop.x += ReactCrop.nudgeStep;
        nudged = true;
      } else if (keyCode === ReactCrop.arrowKey.up) {
        nextCrop.y -= ReactCrop.nudgeStep;
        nudged = true;
      } else if (keyCode === ReactCrop.arrowKey.down) {
        nextCrop.y += ReactCrop.nudgeStep;
        nudged = true;
      }

      if (nudged) {
        e.preventDefault(); // Stop drag selection.

        nextCrop.x = clamp(nextCrop.x, 0, 100 - nextCrop.width);
        nextCrop.y = clamp(nextCrop.y, 0, 100 - nextCrop.height);
        onChange(nextCrop, getPixelCrop(_this.imageRef, nextCrop));
        onComplete(nextCrop, getPixelCrop(_this.imageRef, nextCrop));
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onDocMouseTouchEnd", function () {
      var _this$props5 = _this.props,
          crop = _this$props5.crop,
          disabled = _this$props5.disabled,
          onComplete = _this$props5.onComplete,
          onDragEnd = _this$props5.onDragEnd;
      onDragEnd();

      if (disabled) {
        return;
      }

      if (_this.mouseDownOnCrop) {
        _this.mouseDownOnCrop = false;
        onComplete(crop, getPixelCrop(_this.imageRef, crop));

        _this.setState({
          cropIsActive: false
        });
      }
    });

    return _this;
  }

  _createClass(ReactCrop, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var options = passiveSupported ? {
        passive: false
      } : false;
      document.addEventListener('mousemove', this.onDocMouseTouchMove, options);
      document.addEventListener('touchmove', this.onDocMouseTouchMove, options);
      document.addEventListener('mouseup', this.onDocMouseTouchEnd, options);
      document.addEventListener('touchend', this.onDocMouseTouchEnd, options);
      document.addEventListener('touchcancel', this.onDocMouseTouchEnd, options);

      if (this.imageRef.complete || this.imageRef.readyState) {
        if (this.imageRef.naturalWidth === 0) {
          // Broken load on iOS, PR #51
          // https://css-tricks.com/snippets/jquery/fixing-load-in-ie-for-cached-images/
          // http://stackoverflow.com/questions/821516/browser-independent-way-to-detect-when-image-has-been-loaded
          var src = this.imageRef.src;
          this.imageRef.src = EMPTY_GIF;
          this.imageRef.src = src;
        } else {
          this.onImageLoad(this.imageRef);
        }
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.removeEventListener('mousemove', this.onDocMouseTouchMove);
      document.removeEventListener('touchmove', this.onDocMouseTouchMove);
      document.removeEventListener('mouseup', this.onDocMouseTouchEnd);
      document.removeEventListener('touchend', this.onDocMouseTouchEnd);
      document.removeEventListener('touchcancel', this.onDocMouseTouchEnd);
    }
  }, {
    key: "onImageLoad",
    value: function onImageLoad(image) {
      this.props.onImageLoaded(image);
    }
  }, {
    key: "getCropStyle",
    value: function getCropStyle() {
      var crop = this.props.crop;
      return {
        top: "".concat(crop.y, "%"),
        left: "".concat(crop.x, "%"),
        width: "".concat(crop.width, "%"),
        height: "".concat(crop.height, "%")
      };
    }
  }, {
    key: "getNewSize",
    value: function getNewSize() {
      var _this$props6 = this.props,
          crop = _this$props6.crop,
          minWidth = _this$props6.minWidth,
          maxWidth = _this$props6.maxWidth,
          minHeight = _this$props6.minHeight,
          maxHeight = _this$props6.maxHeight;
      var evData = this.evData;
      var imageAspect = this.imageRef.width / this.imageRef.height; // New width.

      var newWidth = evData.cropStartWidth + evData.xDiffPc;

      if (evData.xCrossOver) {
        newWidth = Math.abs(newWidth);
      }

      newWidth = clamp(newWidth, minWidth, maxWidth); // New height.

      var newHeight;

      if (crop.aspect) {
        newHeight = newWidth / crop.aspect * imageAspect;
      } else {
        newHeight = evData.cropStartHeight + evData.yDiffPc;
      }

      if (evData.yCrossOver) {
        // Cap if polarity is inversed and the height fills the y space.
        newHeight = Math.min(Math.abs(newHeight), evData.cropStartY);
      }

      newHeight = clamp(newHeight, minHeight, maxHeight);

      if (crop.aspect) {
        newWidth = clamp(newHeight * crop.aspect / imageAspect, 0, 100);
      }

      return {
        width: newWidth,
        height: newHeight
      };
    }
  }, {
    key: "dragCrop",
    value: function dragCrop() {
      var nextCrop = this.makeNewCrop();
      var evData = this.evData;
      nextCrop.x = clamp(evData.cropStartX + evData.xDiffPc, 0, 100 - nextCrop.width);
      nextCrop.y = clamp(evData.cropStartY + evData.yDiffPc, 0, 100 - nextCrop.height);
      return nextCrop;
    }
  }, {
    key: "resizeCrop",
    value: function resizeCrop() {
      var nextCrop = this.makeNewCrop();
      var evData = this.evData;
      var ord = evData.ord;
      var imageAspect = this.imageRef.width / this.imageRef.height; // On the inverse change the diff so it's the same and
      // the same algo applies.

      if (evData.xInversed) {
        evData.xDiffPc -= evData.cropStartWidth * 2;
      }

      if (evData.yInversed) {
        evData.yDiffPc -= evData.cropStartHeight * 2;
      } // New size.


      var newSize = this.getNewSize(); // Adjust x/y to give illusion of 'staticness' as width/height is increased
      // when polarity is inversed.

      var newX = evData.cropStartX;
      var newY = evData.cropStartY;

      if (evData.xCrossOver) {
        newX = nextCrop.x + (nextCrop.width - newSize.width);
      }

      if (evData.yCrossOver) {
        // This not only removes the little "shake" when inverting at a diagonal, but for some
        // reason y was way off at fast speeds moving sw->ne with fixed aspect only, I couldn't
        // figure out why.
        if (evData.lastYCrossover === false) {
          newY = nextCrop.y - newSize.height;
        } else {
          newY = nextCrop.y + (nextCrop.height - newSize.height);
        }
      }

      var containedCrop = containCrop({
        x: newX,
        y: newY,
        width: newSize.width,
        height: newSize.height,
        aspect: nextCrop.aspect
      }, imageAspect); // Apply x/y/width/height changes depending on ordinate (fixed aspect always applies both).

      if (nextCrop.aspect || ReactCrop.xyOrds.indexOf(ord) > -1) {
        nextCrop.x = containedCrop.x;
        nextCrop.y = containedCrop.y;
        nextCrop.width = containedCrop.width;
        nextCrop.height = containedCrop.height;
      } else if (ReactCrop.xOrds.indexOf(ord) > -1) {
        nextCrop.x = containedCrop.x;
        nextCrop.width = containedCrop.width;
      } else if (ReactCrop.yOrds.indexOf(ord) > -1) {
        nextCrop.y = containedCrop.y;
        nextCrop.height = containedCrop.height;
      }

      evData.lastYCrossover = evData.yCrossOver;
      this.crossOverCheck();
      return nextCrop;
    }
  }, {
    key: "straightenYPath",
    value: function straightenYPath(clientX) {
      var evData = this.evData;
      var ord = evData.ord;
      var cropOffset = evData.cropOffset;
      var cropStartWidth = evData.cropStartWidth / 100 * this.imageRef.width;
      var cropStartHeight = evData.cropStartHeight / 100 * this.imageRef.height;
      var k;
      var d;

      if (ord === 'nw' || ord === 'se') {
        k = cropStartHeight / cropStartWidth;
        d = cropOffset.top - cropOffset.left * k;
      } else {
        k = -cropStartHeight / cropStartWidth;
        d = cropOffset.top + (cropStartHeight - cropOffset.left * k);
      }

      return k * clientX + d;
    }
  }, {
    key: "createCropSelection",
    value: function createCropSelection() {
      var _this2 = this;

      var disabled = this.props.disabled;
      var style = this.getCropStyle();
      return _react["default"].createElement("div", {
        ref: function ref(n) {
          _this2.cropSelectRef = n;
        },
        style: style,
        className: "ReactCrop__crop-selection",
        onMouseDown: this.onCropMouseTouchDown,
        onTouchStart: this.onCropMouseTouchDown
      }, !disabled && _react["default"].createElement("div", {
        className: "ReactCrop__drag-elements"
      }, _react["default"].createElement("div", {
        className: "ReactCrop__drag-bar ord-n",
        "data-ord": "n"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-bar ord-e",
        "data-ord": "e"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-bar ord-s",
        "data-ord": "s"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-bar ord-w",
        "data-ord": "w"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-nw",
        "data-ord": "nw"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-n",
        "data-ord": "n"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-ne",
        "data-ord": "ne"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-e",
        "data-ord": "e"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-se",
        "data-ord": "se"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-s",
        "data-ord": "s"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-sw",
        "data-ord": "sw"
      }), _react["default"].createElement("div", {
        className: "ReactCrop__drag-handle ord-w",
        "data-ord": "w"
      })));
    }
  }, {
    key: "makeNewCrop",
    value: function makeNewCrop() {
      return _objectSpread({}, ReactCrop.defaultCrop, {}, this.props.crop);
    }
  }, {
    key: "crossOverCheck",
    value: function crossOverCheck() {
      var evData = this.evData;

      if (!evData.xCrossOver && -Math.abs(evData.cropStartWidth) - evData.xDiffPc >= 0 || evData.xCrossOver && -Math.abs(evData.cropStartWidth) - evData.xDiffPc <= 0) {
        evData.xCrossOver = !evData.xCrossOver;
      }

      if (!evData.yCrossOver && -Math.abs(evData.cropStartHeight) - evData.yDiffPc >= 0 || evData.yCrossOver && -Math.abs(evData.cropStartHeight) - evData.yDiffPc <= 0) {
        evData.yCrossOver = !evData.yCrossOver;
      }

      var swapXOrd = evData.xCrossOver !== evData.startXCrossOver;
      var swapYOrd = evData.yCrossOver !== evData.startYCrossOver;
      evData.inversedXOrd = swapXOrd ? inverseOrd(evData.ord) : false;
      evData.inversedYOrd = swapYOrd ? inverseOrd(evData.ord) : false;
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props7 = this.props,
          children = _this$props7.children,
          crossorigin = _this$props7.crossorigin,
          crop = _this$props7.crop,
          disabled = _this$props7.disabled,
          imageAlt = _this$props7.imageAlt,
          src = _this$props7.src,
          style = _this$props7.style,
          imageStyle = _this$props7.imageStyle;
      var cropIsActive = this.state.cropIsActive;
      var cropSelection;

      if (isCropValid(crop)) {
        cropSelection = this.createCropSelection();
      }

      var componentClasses = ['ReactCrop'];

      if (cropIsActive) {
        componentClasses.push('ReactCrop--active');
      }

      if (crop) {
        if (crop.aspect) {
          componentClasses.push('ReactCrop--fixed-aspect');
        } // In this case we have to shadow the image, since the box-shadow
        // on the crop won't work.


        if (cropIsActive && (!crop.width || !crop.height)) {
          componentClasses.push('ReactCrop--crop-invisible');
        }
      }

      if (disabled) {
        componentClasses.push('ReactCrop--disabled');
      }

      return _react["default"].createElement("div", {
        ref: function ref(n) {
          _this3.componentRef = n;
        },
        className: componentClasses.join(' '),
        style: style,
        onTouchStart: this.onComponentMouseTouchDown,
        onMouseDown: this.onComponentMouseTouchDown,
        tabIndex: "1",
        onKeyDown: this.onComponentKeyDown
      }, _react["default"].createElement("img", {
        ref: function ref(n) {
          _this3.imageRef = n;
        },
        crossOrigin: crossorigin,
        className: "ReactCrop__image",
        style: imageStyle,
        src: src,
        onLoad: function onLoad(e) {
          return _this3.onImageLoad(e.target);
        },
        alt: imageAlt
      }), cropSelection, children);
    }
  }]);

  return ReactCrop;
}(_react.PureComponent);

ReactCrop.xOrds = ['e', 'w'];
ReactCrop.yOrds = ['n', 's'];
ReactCrop.xyOrds = ['nw', 'ne', 'se', 'sw'];
ReactCrop.arrowKey = {
  left: 37,
  up: 38,
  right: 39,
  down: 40
};
ReactCrop.nudgeStep = 0.2;
ReactCrop.defaultCrop = {
  x: 0,
  y: 0,
  width: 0,
  height: 0
};
ReactCrop.propTypes = {
  src: _propTypes["default"].string.isRequired,
  crop: _propTypes["default"].shape({
    aspect: _propTypes["default"].number,
    x: _propTypes["default"].number,
    y: _propTypes["default"].number,
    width: _propTypes["default"].number,
    height: _propTypes["default"].number
  }),
  imageAlt: _propTypes["default"].string,
  minWidth: _propTypes["default"].number,
  minHeight: _propTypes["default"].number,
  maxWidth: _propTypes["default"].number,
  maxHeight: _propTypes["default"].number,
  keepSelection: _propTypes["default"].bool,
  onChange: _propTypes["default"].func.isRequired,
  onComplete: _propTypes["default"].func,
  onImageLoaded: _propTypes["default"].func,
  onDragStart: _propTypes["default"].func,
  onDragEnd: _propTypes["default"].func,
  disabled: _propTypes["default"].bool,
  crossorigin: _propTypes["default"].string,
  children: _propTypes["default"].oneOfType([_propTypes["default"].arrayOf(_propTypes["default"].node), _propTypes["default"].node]),
  style: _propTypes["default"].shape({}),
  imageStyle: _propTypes["default"].shape({})
};
ReactCrop.defaultProps = {
  crop: undefined,
  crossorigin: undefined,
  disabled: false,
  imageAlt: '',
  maxWidth: 100,
  maxHeight: 100,
  minWidth: 0,
  minHeight: 0,
  keepSelection: false,
  onComplete: function onComplete() {},
  onImageLoaded: function onImageLoaded() {},
  onDragStart: function onDragStart() {},
  onDragEnd: function onDragEnd() {},
  children: undefined,
  style: undefined,
  imageStyle: undefined
};
module.exports = ReactCrop;
module.exports.getPixelCrop = getPixelCrop;
module.exports.makeAspectCrop = makeAspectCrop;
module.exports.containCrop = containCrop;