"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _antd = require("antd");

var _Charts = require("components/Charts");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _enumConfig = require("utils/enumConfig");

var _moment = _interopRequireDefault(require("moment"));

var _NormalFilter = _interopRequireDefault(require("components/Filter/NormalFilter"));

var _DescriptionList = _interopRequireDefault(require("components/DescriptionList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var LwjCardTable = function LwjCardTable(_ref) {
  var dataSource = _ref.dataSource,
      total = _ref.total,
      pagination = _ref.pagination,
      loading = _ref.loading,
      dispatch = _ref.dispatch,
      pageChangeAction = _ref.pageChangeAction,
      currentItem = _ref.currentItem,
      extConfigs = _objectWithoutProperties(_ref, ["dataSource", "total", "pagination", "loading", "dispatch", "pageChangeAction", "currentItem"]);

  var Description = _DescriptionList["default"].Description;
  var Grid = _antd.Card.Grid;

  var onShowSizeChange = function onShowSizeChange(current, pageSize) {
    pagination.current = current;
    pagination.defaultPageSize = pageSize;
    dispatch({
      type: pageChangeAction,
      payload: {
        limit: pageSize,
        start: current - 1,
        isTablePagination: true
      }
    });
  };

  var onChange = function onChange(page, pageSize) {
    pagination.current = page;
    pagination.defaultPageSize = pageSize;
    dispatch({
      type: pageChangeAction,
      payload: {
        limit: pageSize,
        start: page - 1,
        isTablePagination: true
      }
    });
  };

  var filterProps = {
    filterItems: [{
      type: 'select',
      id: 'couponsStatus',
      title: '优惠券状态',
      options: [{
        value: "",
        name: '全部'
      }, {
        value: 'CAN_USE',
        name: '可用'
      }, {
        value: 'USED',
        name: '已使用'
      }, {
        value: 'ABANDONED',
        name: '已作废'
      }]
    }, {
      type: 'dateRule',
      id: 'createTime',
      dateFormat: 'YYYY/MM/DD HH:mm:ss',
      title: '添加时间'
    }],
    onFilterChange: function onFilterChange(value) {
      dispatch({
        type: 'coupon/queryByDistributor',
        payload: _objectSpread({}, value, {
          limit: pagination.defaultPageSize,
          start: 0
        })
      });
    }
  };

  var renderCardList = function renderCardList(source) {
    if (!_.isArray(source)) {
      return;
    }

    return source.map(function (value) {
      return _react["default"].createElement(Grid, {
        key: value.createTime
      }, _react["default"].createElement(_Charts.ChartCard, {
        key: value.createTime,
        loading: loading,
        bordered: false,
        title: value.couponsNum,
        total: _react["default"].createElement("span", {
          style: {
            color: value.status === 'CAN_USE' ? "black" : "gainsboro"
          }
        }, _enumConfig.couponStatus[value.status]),
        footer: _react["default"].createElement("div", {
          style: {
            whiteSpace: 'nowrap',
            overflow: 'hidden'
          }
        }, _react["default"].createElement("span", {
          style: {
            color: value.status === 'CAN_USE' ? "black" : "gainsboro"
          }
        }, "\u521B\u5EFA\u65E5\u671F\uFF1A", (0, _moment["default"])(value.createTime).format("YYYY-MM-DD"), _react["default"].createElement(_antd.Popover, {
          placement: "right",
          title: "\u4F18\u60E0\u5238\u8BE6\u60C5",
          content: _react["default"].createElement("div", null, _react["default"].createElement("div", null, "\u4F18\u60E0\u5238ID:", value.couponsNum), _react["default"].createElement("div", null, "\u72B6\u6001:", _enumConfig.couponStatus[value.status]), _react["default"].createElement("div", null, "\u4E1A\u52A1\u7C7B\u578B:\u4E0B\u5355"), _react["default"].createElement("div", null, "\u603B\u91D1\u989D:", value.amount), _react["default"].createElement("div", null, "\u5DF2\u7528\u91D1\u989D:", value.usedAmount), _react["default"].createElement("div", null, "\u4F7F\u7528\u5408\u540C\u53F7:", value.contractNum), _react["default"].createElement("div", null, "\u521B\u5EFA\u65F6\u95F4:", (0, _moment["default"])(value.createTime).format("YYYY-MM-DD HH:mm:ss")), value.useTime && _react["default"].createElement("div", null, "\u4F7F\u7528\u65F6\u95F4:", (0, _moment["default"])(value.useTime).format("YYYY-MM-DD HH:mm:ss"))),
          trigger: "click"
        }, _react["default"].createElement("a", {
          style: {
            marginLeft: 15
          }
        }, "\u4F7F\u7528\u8BE6\u60C5")))),
        contentHeight: 46
      }, _react["default"].createElement(_Charts.MiniProgress, {
        percent: (value.amount - value.usedAmount) / value.amount * 100,
        strokeWidth: 7,
        target: 100,
        color: value.status === 'CAN_USE' ? "#13C2C2" : "gainsboro"
      }), _react["default"].createElement("span", {
        style: {
          color: value.status === 'CAN_USE' ? "black" : "gainsboro"
        }
      }, "\u603B\u91D1\u989D\uFF1A\xA5 ", value.amount, " ")));
    });
  };

  return _react["default"].createElement("div", null, _react["default"].createElement(_antd.Card, null, _react["default"].createElement(_NormalFilter["default"], filterProps), renderCardList(dataSource)), _react["default"].createElement(_antd.Pagination, {
    showSizeChanger: true,
    onChange: onChange,
    onShowSizeChange: onShowSizeChange,
    defaultCurrent: 1,
    total: total
  }));
};

LwjCardTable.propTypes = {
  total: _propTypes["default"].any,
  dataSource: _propTypes["default"].array,
  pagination: _propTypes["default"].any,
  loading: _propTypes["default"].bool,
  dispatch: _propTypes["default"].func,
  pageChangeAction: _propTypes["default"].string
};
var _default = LwjCardTable;
exports["default"] = _default;