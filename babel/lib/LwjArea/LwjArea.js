"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _antd = require("antd");

var _area = require("./area");

var _city = require("./city");

var _province = require("./province");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var setCascaderOption = function setCascaderOption(regionArr, isArea) {
  var options = [];

  if (!regionArr || regionArr.length === 0) {
    return;
  }

  _.each(regionArr, function (region) {
    var option = {
      value: region.code,
      label: region.name,
      isLeaf: isArea
    };
    options.push(option);
  });

  return options;
};

var provinces = _province.province;
var cities = _city.city;
var areas = _area.area;

var LwjArea =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LwjArea, _React$Component);

  function LwjArea(props) {
    var _this;

    _classCallCheck(this, LwjArea);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LwjArea).call(this, props)); // 数据源（默认为前端配置，如需使用后端的地区信息则设置为backEnd）

    _defineProperty(_assertThisInitialized(_this), "loadData", function (selectedOptions) {
      var targetOption = selectedOptions[selectedOptions.length - 1];
      var regionId = targetOption.value;
      targetOption.loading = true;

      if (selectedOptions.length === 1) {
        // 点击省,获取市
        targetOption.loading = false;
        var citiesInProvinces = []; // 根据省份ID获取该省份下的城市信息

        citiesInProvinces = cities[regionId];
        targetOption.children = setCascaderOption(citiesInProvinces, false);

        if (!targetOption.children || targetOption.children.length === 0) {
          targetOption.isLeaf = true;
        }

        _this.setState({
          options: _toConsumableArray(_this.state.options)
        });
      } else if (selectedOptions.length === 2) {
        // 点击市,获取区域
        targetOption.loading = false;
        var areaInCity = []; // 根据城市ID获取该城市下的区域信息

        areaInCity = areas[regionId];
        targetOption.children = setCascaderOption(areaInCity, true);

        if (!targetOption.children || targetOption.children.length === 0) {
          targetOption.isLeaf = true;
        }

        _this.setState({
          options: _toConsumableArray(_this.state.options)
        });
      } else {
        targetOption.loading = false;
      }
    });

    _this.dataSource = props.dataSource;

    if (_this.dataSource === "backEnd" && (_.isEmpty(props.areaSummary) || _.isEmpty(props.citySummary) || _.isEmpty(props.provinceSummary))) {
      return _possibleConstructorReturn(_this);
    } // 判断数据来源


    if (!_.isEmpty(props.areaSummary) && !_.isEmpty(props.citySummary) && !_.isEmpty(props.provinceSummary)) {
      assembleRegionData();
    } // initial options


    var options = setCascaderOption(provinces, false); // initial state onChange由调用者传入

    _this.onChange = props.onChange; // 默认值也由调用者提供 设置组件宽度

    _this.width = props.width; // ES6 类中函数必须手动绑定

    _this.loadData = _this.loadData.bind(_assertThisInitialized(_this));
    _this.state = {
      options: options
    };
    /**
     * 组装后端传来的地区数据
     */

    function assembleRegionData() {
      provinces = props.provinceSummary;
      var cityObj = {};

      _.each(props.provinceSummary, function (p) {
        var cityOfProvince = _.filter(props.citySummary, function (c) {
          return c.provinceCode === p.code;
        });

        cityObj[p.code] = cityOfProvince;
      });

      cities = cityObj;
      var areaObj = {};

      _.each(props.citySummary, function (c) {
        var areaOfCity = _.filter(props.areaSummary, function (a) {
          return a.cityCode === c.code;
        });

        areaObj[c.code] = areaOfCity;
      });

      areas = areaObj;
    }

    return _this;
  }

  _createClass(LwjArea, [{
    key: "render",
    value: function render() {
      return _react["default"].createElement(_antd.Cascader, {
        defaultValue: this.state.defaultValue,
        options: this.state.options,
        loadData: this.loadData,
        onChange: this.onChange,
        placeholder: "\u8BF7\u9009\u62E9\u7701\u5E02\u533A",
        changeOnSelect: true,
        style: {
          width: this.width
        },
        value: this.state.value
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (props.value) {
        var defaultProvince = props.value[0];
        var defaultCity = props.value[1];
        var targetProvince = state.options.filter(function (op) {
          return op.value === defaultProvince;
        })[0];
        targetProvince = targetProvince || {};
        targetProvince.children = setCascaderOption(cities[defaultProvince], false) || [];
        var targetCity = {};

        if (!_.isEmpty(defaultCity)) {
          targetCity = targetProvince.children.filter(function (op) {
            return op.value === defaultCity;
          })[0] || {};
          targetCity.children = setCascaderOption(areas[defaultCity], true);
        }
      }

      return _objectSpread({}, state, {
        defaultValue: props.value,
        value: props.value
      });
    }
  }]);

  return LwjArea;
}(_react["default"].Component);

LwjArea.propTypes = {
  onChange: _propTypes["default"].func,
  dataSource: _propTypes["default"].string,
  getProvinces: _propTypes["default"].func,
  getCitiesByProvince: _propTypes["default"].func,
  getAreaByCity: _propTypes["default"].func
};
LwjArea.defaultProps = {
  width: 230
};
var _default = LwjArea;
exports["default"] = _default;