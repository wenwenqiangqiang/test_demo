"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _moment = _interopRequireDefault(require("moment"));

var _components = require("components");

var _antd = require("antd");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var FormItem = _antd.Form.Item;
var RangePicker = _antd.DatePicker.RangePicker;
var SHOW_PARENT = _antd.TreeSelect.SHOW_PARENT;
var filterStyle = {
  style: {
    marginBottom: 16
  }
};
var formItemLayout = {
  labelCol: {
    span: 6
  },
  wrapperCol: {
    span: 18
  }
};
var Option = _antd.Select.Option;

var OrderFilterUnExpand = function OrderFilterUnExpand(_ref) {
  var filterItems = _ref.filterItems,
      onFilterChange = _ref.onFilterChange,
      isExpand = _ref.isExpand,
      _ref$form = _ref.form,
      getFieldDecorator = _ref$form.getFieldDecorator,
      getFieldsValue = _ref$form.getFieldsValue,
      setFieldsValue = _ref$form.setFieldsValue,
      expand = _ref.expand,
      noExpand = _ref.noExpand,
      noExpandButton = _ref.noExpandButton;
  var filterItemsDealed;

  if (isExpand) {
    filterItemsDealed = filterItems;
  } else {
    if (filterItems.length > 3) {
      filterItemsDealed = filterItems.slice(0, 2);
    } else {
      filterItemsDealed = filterItems;
    }
  }

  var handleFields = function handleFields(fields) {
    for (var key in fields) {
      var field = fields[key]; // 删除请求中为空的对象

      if (!field || _.isString(field) && field.replace(/(^\s*)|(\s*$)/g, "").length == 0 || _.isUndefined(field) || _.isNull(field) || _.isArray(field) && field.length === 0) {
        delete fields[key];
        continue;
      } // 处理日期类型


      if (_.endsWith(key, "Time") && _.isArray(fields[key]) && fields[key].length > 0) {
        var timeStartValue = fields[key][0].valueOf();
        var timeEndValue = fields[key][1].valueOf();
        var timeStartKey = key + "Start";
        var timeEndKey = key + "End";
        fields[timeStartKey] = timeStartValue;
        fields[timeEndKey] = timeEndValue;
        delete fields[key];
      }
    }

    return fields;
  };

  var handleSubmit = function handleSubmit() {
    var fields = getFieldsValue();
    fields = handleFields(fields);
    onFilterChange(fields);
  };

  var handleReset = function handleReset() {
    var fields = getFieldsValue();

    for (var item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = [];
        } else {
          fields[item] = undefined;
        }
      }
    }

    setFieldsValue(fields);
    handleSubmit();
  };

  var handleChange = function handleChange(key, values) {
    var fields = getFieldsValue();
    fields[key] = values;
    fields = handleFields(fields);
    onFilterChange(fields);
  };

  var dateChange = function dateChange(date, dateString) {
    console.log(date, dateString);
  };

  var getFilterItemCol = function getFilterItemCol(filterItems) {
    var result = filterItems.map(function (filterItem, index) {
      return _react["default"].createElement(_antd.Col, {
        key: index,
        span: 8
      }, _react["default"].createElement("div", {
        id: "parent"
      }, _react["default"].createElement(FormItem, _extends({
        label: filterItem.title
      }, formItemLayout), filterItem.type == 'input' && getFieldDecorator("".concat(filterItem.id), {})(_react["default"].createElement(_antd.Input, {
        placeholder: filterItem.placeholder,
        onPressEnter: handleSubmit
      })), filterItem.type === 'inputNumber' && getFieldDecorator("".concat(filterItem.id), {})(_react["default"].createElement(_antd.InputNumber, {
        placeholder: filterItem.placeholder,
        onPressEnter: handleSubmit,
        step: 0.01,
        precision: 2,
        style: {
          width: '100%'
        }
      })), filterItem.type == 'select' && getFieldDecorator("".concat(filterItem.id), {
        initialValue: filterItem.defaultValue
      })(_react["default"].createElement(_antd.Select, {
        initialValue: filterItem.defaultValue,
        allowClear: true,
        style: {
          width: '100%'
        },
        showSearch: filterItem.showSearch,
        filterOption: function filterOption(input, option) {
          return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
        },
        getPopupContainer: function getPopupContainer() {
          return document.getElementById("parent");
        }
      }, getOptions(filterItem.options))), filterItem.type === 'treeSelect' && getFieldDecorator("".concat(filterItem.id), {
        initialValue: filterItem.defaultValue
      })(_react["default"].createElement(_antd.TreeSelect, {
        initialValue: filterItem.defaultValue,
        treeData: filterItem.options,
        treeCheckable: filterItem.checkable ? filterItem.checkable : false,
        maxTagCount: filterItem.maxTagCount ? filterItem.maxTagCount : 1,
        showCheckedStrategy: SHOW_PARENT,
        searchPlaceholder: filterItem.searchPlaceholder,
        placeholder: filterItem.placeholder,
        style: {
          width: '100%'
        },
        getPopupContainer: function getPopupContainer() {
          return document.getElementById("parent");
        }
      })), filterItem.type == 'dateRule' && getFieldDecorator("".concat(filterItem.id), {})(_react["default"].createElement(RangePicker, {
        style: {
          width: '100%'
        },
        showTime: true,
        initialValue: [(0, _moment["default"])(filterItem.bdTime || '', filterItem.dateFormat || "YYYY/MM/DD"), (0, _moment["default"])(filterItem.edTime || '', filterItem.dateFormat || "YYYY/MM/DD")],
        format: filterItem.dateFormat || "YYYY/MM/DD",
        onChange: dateChange
      })), filterItem.type == 'date' && getFieldDecorator("".concat(filterItem.id), {})(_react["default"].createElement(_antd.DatePicker, {
        style: {
          width: '100%'
        },
        onChange: dateChange
      })))));
    });
    return result;
  }; // 处理下拉选项


  var getOptions = function getOptions(value) {
    return value.map(function (option) {
      return _react["default"].createElement(Option, {
        value: option.value,
        key: option.value
      }, option.name);
    });
  };

  var filterItemCols = getFilterItemCol(filterItemsDealed);
  console.info(noExpandButton);
  return _react["default"].createElement(_antd.Row, _extends({
    gutter: 24
  }, filterStyle), filterItemCols, _react["default"].createElement(_antd.Col, {
    span: 8,
    style: {
      textAlign: 'left'
    }
  }, _react["default"].createElement(_antd.Button, {
    type: "primary",
    onClick: handleSubmit
  }, "\u67E5\u8BE2"), _react["default"].createElement(_antd.Divider, {
    type: "vertical"
  }), _react["default"].createElement(_antd.Button, {
    type: "ghost",
    onClick: handleReset
  }, "\u91CD\u7F6E"), _react["default"].createElement(_antd.Divider, {
    type: "vertical"
  }), " ", _.isEmpty(noExpandButton) && !isExpand && _react["default"].createElement("a", {
    style: {
      marginRight: 40
    },
    onClick: expand
  }, "\u5C55\u5F00 ", _react["default"].createElement(_antd.Icon, {
    type: "down"
  })), _.isEmpty(noExpandButton) && isExpand && _react["default"].createElement("a", {
    style: {
      marginRight: 40
    },
    onClick: noExpand
  }, "\u6536\u8D77 ", _react["default"].createElement(_antd.Icon, {
    type: "up"
  }))));
};

OrderFilterUnExpand.propTypes = {
  form: _propTypes["default"].object,
  filter: _propTypes["default"].object,
  onFilterChange: _propTypes["default"].func
};

var _default = _antd.Form.create({
  onFieldsChange: function onFieldsChange(props, changedFields) {
    if (props.onChange) {
      props.onChange(changedFields);
    }
  }
})(OrderFilterUnExpand);

exports["default"] = _default;