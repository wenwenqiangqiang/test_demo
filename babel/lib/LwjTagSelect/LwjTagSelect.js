"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LwjTagSelect =
/*#__PURE__*/
function (_Component) {
  _inherits(LwjTagSelect, _Component);

  function LwjTagSelect() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LwjTagSelect);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LwjTagSelect)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      selectedTabs: [],
      canselectTabs: []
    });

    return _this;
  }

  _createClass(LwjTagSelect, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({
        canselectTabs: this.props.tabLists
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          tabLists = _this$props.tabLists,
          onChange = _this$props.onChange,
          title = _this$props.title;
      var _this$state = this.state,
          selectedTabs = _this$state.selectedTabs,
          canselectTabs = _this$state.canselectTabs;

      var preventDefault = function preventDefault(tag) {
        selectedTabs.splice(canselectTabs.indexOf(tag), 1);
        canselectTabs.push(tag);

        _this2.setState({
          selectedTabs: selectedTabs,
          canselectTabs: canselectTabs
        });

        onChange(selectedTabs);
      };

      var handleChange = function handleChange(tag) {
        // let flag = true; for (let i = 0; i < selectedTabs.length; i++) {   if
        // (selectedTabs[i].id == tag.id) {     flag = false;   } } if(!flag){
        // Modal.error({     title:'请勿重复添加'   }) }else{   console.log(8888) }
        canselectTabs.splice(canselectTabs.indexOf(tag), 1);
        selectedTabs.push(tag);

        _this2.setState({
          selectedTabs: selectedTabs,
          canselectTabs: canselectTabs
        });

        onChange(selectedTabs);
      };

      return _react["default"].createElement("div", {
        className: "clearfix",
        style: {
          border: '1px solid #ccc',
          padding: 10,
          marginBottom: 10
        }
      }, _react["default"].createElement("div", {
        style: {
          marginBottom: 10
        }
      }, selectedTabs.map(function (tag) {
        return _react["default"].createElement(_antd.Tag, {
          key: tag.id,
          closable: true,
          onClose: function onClose() {
            return preventDefault(tag);
          },
          color: "#f50"
        }, tag.name);
      })), _react["default"].createElement("div", null, _react["default"].createElement("h6", {
        style: {
          marginRight: 8,
          display: 'inline'
        }
      }, title, ":"), canselectTabs.map(function (tag) {
        return _react["default"].createElement(_antd.Tag, {
          key: tag.id,
          onClick: function onClick() {
            return handleChange(tag);
          }
        }, tag.name);
      })));
    }
  }]);

  return LwjTagSelect;
}(_react.Component);

exports["default"] = LwjTagSelect;
LwjTagSelect.props = {};