"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBreadcrumb = getBreadcrumb;
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _pathToRegexp = _interopRequireDefault(require("path-to-regexp"));

var _antd = require("antd");

var _classnames = _interopRequireDefault(require("classnames"));

var _index = _interopRequireDefault(require("./index.less"));

var _pathTools = require("../_utils/pathTools");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TabPane = _antd.Tabs.TabPane;

function getBreadcrumb(breadcrumbNameMap, url) {
  var breadcrumb = breadcrumbNameMap[url];

  if (!breadcrumb) {
    Object.keys(breadcrumbNameMap).forEach(function (item) {
      if ((0, _pathToRegexp["default"])(item).test(url)) {
        breadcrumb = breadcrumbNameMap[item];
      }
    });
  }

  return breadcrumb || {};
}

var PageHeader =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(PageHeader, _PureComponent);

  function PageHeader() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, PageHeader);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(PageHeader)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      breadcrumb: null
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (key) {
      if (_this.props.onTabChange) {
        _this.props.onTabChange(key);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "getBreadcrumbProps", function () {
      return {
        routes: _this.props.routes || _this.context.routes,
        params: _this.props.params || _this.context.params,
        routerLocation: _this.props.location || _this.context.location,
        breadcrumbNameMap: _this.props.breadcrumbNameMap || _this.context.breadcrumbNameMap
      };
    });

    _defineProperty(_assertThisInitialized(_this), "getBreadcrumbDom", function () {
      var breadcrumb = _this.conversionBreadcrumbList();

      _this.setState({
        breadcrumb: breadcrumb
      });
    });

    _defineProperty(_assertThisInitialized(_this), "conversionFromProps", function () {
      var _this$props = _this.props,
          breadcrumbList = _this$props.breadcrumbList,
          breadcrumbSeparator = _this$props.breadcrumbSeparator,
          _this$props$linkEleme = _this$props.linkElement,
          linkElement = _this$props$linkEleme === void 0 ? 'a' : _this$props$linkEleme;
      return _react["default"].createElement(_antd.Breadcrumb, {
        className: _index["default"].breadcrumb,
        separator: breadcrumbSeparator
      }, breadcrumbList.map(function (item) {
        return _react["default"].createElement(_antd.Breadcrumb.Item, {
          key: item.title
        }, item.href ? (0, _react.createElement)(linkElement, _defineProperty({}, linkElement === 'a' ? 'href' : 'to', item.href), item.title) : item.title);
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "conversionFromLocation", function (routerLocation, breadcrumbNameMap) {
      var _this$props2 = _this.props,
          breadcrumbSeparator = _this$props2.breadcrumbSeparator,
          _this$props2$linkElem = _this$props2.linkElement,
          linkElement = _this$props2$linkElem === void 0 ? 'a' : _this$props2$linkElem; // Convert the url to an array

      var pathSnippets = (0, _pathTools.urlToList)(routerLocation.pathname); // Loop data mosaic routing

      var extraBreadcrumbItems = pathSnippets.map(function (url, index) {
        var currentBreadcrumb = getBreadcrumb(breadcrumbNameMap, url);
        var isLinkable = index !== pathSnippets.length - 1 && currentBreadcrumb.component;
        return currentBreadcrumb.name && !currentBreadcrumb.hideInBreadcrumb ? _react["default"].createElement(_antd.Breadcrumb.Item, {
          key: url
        }, (0, _react.createElement)(isLinkable ? linkElement : 'span', _defineProperty({}, linkElement === 'a' ? 'href' : 'to', url), currentBreadcrumb.name)) : null;
      }); // Add home breadcrumbs to your head

      extraBreadcrumbItems.unshift(_react["default"].createElement(_antd.Breadcrumb.Item, {
        key: "home"
      }, (0, _react.createElement)(linkElement, _defineProperty({}, linkElement === 'a' ? 'href' : 'to', '/'), '首页')));
      return _react["default"].createElement(_antd.Breadcrumb, {
        className: _index["default"].breadcrumb,
        separator: breadcrumbSeparator
      }, extraBreadcrumbItems);
    });

    _defineProperty(_assertThisInitialized(_this), "conversionBreadcrumbList", function () {
      var _this$props3 = _this.props,
          breadcrumbList = _this$props3.breadcrumbList,
          breadcrumbSeparator = _this$props3.breadcrumbSeparator;

      var _this$getBreadcrumbPr = _this.getBreadcrumbProps(),
          routes = _this$getBreadcrumbPr.routes,
          params = _this$getBreadcrumbPr.params,
          routerLocation = _this$getBreadcrumbPr.routerLocation,
          breadcrumbNameMap = _this$getBreadcrumbPr.breadcrumbNameMap;

      if (breadcrumbList && breadcrumbList.length) {
        return _this.conversionFromProps();
      } // 如果传入 routes 和 params 属性
      // If pass routes and params attributes


      if (routes && params) {
        return _react["default"].createElement(_antd.Breadcrumb, {
          className: _index["default"].breadcrumb,
          routes: routes.filter(function (route) {
            return route.breadcrumbName;
          }),
          params: params,
          itemRender: _this.itemRender,
          separator: breadcrumbSeparator
        });
      } // 根据 location 生成 面包屑
      // Generate breadcrumbs based on location


      if (routerLocation && routerLocation.pathname) {
        return _this.conversionFromLocation(routerLocation, breadcrumbNameMap);
      }

      return null;
    });

    _defineProperty(_assertThisInitialized(_this), "itemRender", function (route, params, routes, paths) {
      var _this$props$linkEleme2 = _this.props.linkElement,
          linkElement = _this$props$linkEleme2 === void 0 ? 'a' : _this$props$linkEleme2;
      var last = routes.indexOf(route) === routes.length - 1;
      return last || !route.component ? _react["default"].createElement("span", null, route.breadcrumbName) : (0, _react.createElement)(linkElement, {
        href: paths.join('/') || '/',
        to: paths.join('/') || '/'
      }, route.breadcrumbName);
    });

    return _this;
  }

  _createClass(PageHeader, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getBreadcrumbDom();
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps() {
      this.getBreadcrumbDom();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props4 = this.props,
          title = _this$props4.title,
          logo = _this$props4.logo,
          action = _this$props4.action,
          content = _this$props4.content,
          extraContent = _this$props4.extraContent,
          tabList = _this$props4.tabList,
          className = _this$props4.className,
          tabActiveKey = _this$props4.tabActiveKey,
          tabDefaultActiveKey = _this$props4.tabDefaultActiveKey,
          tabBarExtraContent = _this$props4.tabBarExtraContent;
      var clsString = (0, _classnames["default"])(_index["default"].pageHeader, className);
      var activeKeyProps = {};

      if (tabDefaultActiveKey !== undefined) {
        activeKeyProps.defaultActiveKey = tabDefaultActiveKey;
      }

      if (tabActiveKey !== undefined) {
        activeKeyProps.activeKey = tabActiveKey;
      }

      return _react["default"].createElement("div", {
        className: this.state.breadcrumb || this.state.title ? clsString : null
      }, this.state.breadcrumb, _react["default"].createElement("div", {
        className: _index["default"].detail
      }, logo && _react["default"].createElement("div", {
        className: _index["default"].logo
      }, logo), _react["default"].createElement("div", {
        className: _index["default"].main
      }, _react["default"].createElement("div", {
        className: _index["default"].row
      }, title && _react["default"].createElement("h1", {
        className: _index["default"].title
      }, title), action && _react["default"].createElement("div", {
        className: _index["default"].action
      }, action)), _react["default"].createElement("div", {
        className: _index["default"].row
      }, content && _react["default"].createElement("div", {
        className: _index["default"].content
      }, content), extraContent && _react["default"].createElement("div", {
        className: _index["default"].extraContent
      }, extraContent)))), tabList && tabList.length && _react["default"].createElement(_antd.Tabs, _extends({
        className: _index["default"].tabs
      }, activeKeyProps, {
        onChange: this.onChange,
        tabBarExtraContent: tabBarExtraContent
      }), tabList.map(function (item) {
        return _react["default"].createElement(TabPane, {
          tab: item.tab,
          key: item.key
        });
      })));
    }
  }]);

  return PageHeader;
}(_react.PureComponent);

exports["default"] = PageHeader;

_defineProperty(PageHeader, "contextTypes", {
  routes: _propTypes["default"].array,
  params: _propTypes["default"].object,
  location: _propTypes["default"].object,
  breadcrumbNameMap: _propTypes["default"].object
});