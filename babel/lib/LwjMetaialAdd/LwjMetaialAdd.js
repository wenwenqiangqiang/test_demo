"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var LwjMetaialAdd = function LwjMetaialAdd(_ref) {
  var _ref$data = _ref.data,
      data = _ref$data === void 0 ? [] : _ref$data,
      removeOne = _ref.removeOne,
      addOne = _ref.addOne;
  var columns = [{
    title: '物料编号',
    dataIndex: 'name',
    key: 'name',
    render: function render(text) {
      return _react["default"].createElement(_antd.Input, {
        placeholder: "\u8BF7\u8F93\u5165\u7269\u6599\u7F16\u53F7"
      });
    }
  }, {
    title: '物料数量',
    dataIndex: 'age',
    key: 'age',
    render: function render(text) {
      return _react["default"].createElement(_antd.InputNumber, {
        min: 1
      });
    }
  }, {
    title: '物料名称',
    dataIndex: 'address',
    key: 'address'
  }, {
    title: '操作',
    key: 'action',
    render: function render(text, record) {
      return _react["default"].createElement("span", null, _react["default"].createElement("a", {
        href: "javascript:;",
        onClick: removeOne(record)
      }, "\u5220\u9664"));
    }
  }];
  return _react["default"].createElement("div", {
    className: "clearfix",
    style: {
      padding: 10,
      marginBottom: 10
    }
  }, _react["default"].createElement(_antd.Table, {
    columns: columns,
    dataSource: data,
    pagination: false
  }), _react["default"].createElement(_antd.Button, {
    type: "primary",
    onClick: addOne,
    style: {
      marginTop: 20,
      marginLeft: 20
    }
  }, "\u7ED1\u5B9A\u7269\u54C1"));
};

var _default = LwjMetaialAdd;
exports["default"] = _default;
LwjMetaialAdd.props = {};