"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _antd = require("antd");

var _utils = require("utils");

var _Layout = _interopRequireDefault(require("./Layout.less"));

var _Menu = _interopRequireDefault(require("./Menu"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Sider = function Sider(_ref) {
  var siderFold = _ref.siderFold,
      darkTheme = _ref.darkTheme,
      location = _ref.location,
      changeTheme = _ref.changeTheme,
      navOpenKeys = _ref.navOpenKeys,
      changeOpenKeys = _ref.changeOpenKeys,
      menu = _ref.menu,
      logo = _ref.logo;
  var menusProps = {
    menu: menu,
    siderFold: siderFold,
    darkTheme: darkTheme,
    location: location,
    navOpenKeys: navOpenKeys,
    changeOpenKeys: changeOpenKeys
  };
  return _react["default"].createElement(_antd.Layout.Sider, {
    theme: "light",
    trigger: null,
    collapsible: true,
    collapsed: siderFold
  }, _react["default"].createElement("div", {
    className: _Layout["default"].logo
  }, _react["default"].createElement("img", {
    alt: 'logo',
    src: logo
  })), _react["default"].createElement(_Menu["default"], menusProps), _react["default"].createElement("div", {
    className: _Layout["default"].poweredBy
  }, _utils.config.footerText));
};

Sider.propTypes = {
  menu: _propTypes["default"].array,
  siderFold: _propTypes["default"].bool,
  darkTheme: _propTypes["default"].bool,
  location: _propTypes["default"].object,
  changeTheme: _propTypes["default"].func,
  navOpenKeys: _propTypes["default"].array,
  changeOpenKeys: _propTypes["default"].func
};
var _default = Sider;
exports["default"] = _default;