"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _antd = require("antd");

var _reactRouterDom = require("react-router-dom");

var _pathToRegexp = _interopRequireDefault(require("path-to-regexp"));

var _utils = require("utils");

var _Bread = _interopRequireDefault(require("./Bread.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Bread = function Bread(_ref) {
  var menu = _ref.menu,
      location = _ref.location;
  // 匹配当前路由
  var pathArray = [];
  var current;

  for (var index in menu) {
    if (menu[index].route && (0, _pathToRegexp["default"])(menu[index].route).exec(location.pathname)) {
      current = menu[index];
      break;
    }
  }

  var getPathArray = function getPathArray(item) {
    pathArray.unshift(item);

    if (item.bpid) {
      getPathArray((0, _utils.queryArray)(menu, item.bpid, 'id'));
    }
  };

  if (!current) {
    pathArray.push(menu[0] || {
      id: 1,
      icon: 'laptop',
      name: 'Dashboard'
    });
    pathArray.push({
      id: 404,
      name: 'Not Found'
    });
  } else {
    getPathArray(current);
  } // 递归查找父级


  var breads = pathArray.map(function (item, key) {
    var content = _react["default"].createElement("span", null, item.icon ? _react["default"].createElement(_antd.Icon, {
      type: item.icon,
      style: {
        marginRight: 4
      }
    }) : '', item.name);

    return _react["default"].createElement(_antd.Breadcrumb.Item, {
      key: key
    }, pathArray.length - 1 !== key ? _react["default"].createElement(_reactRouterDom.Link, {
      to: item.route || '#'
    }, content) : content);
  });
  return _react["default"].createElement("div", {
    className: _Bread["default"].bread
  }, _react["default"].createElement(_antd.Breadcrumb, null, breads));
};

Bread.propTypes = {
  menu: _propTypes["default"].array,
  location: _propTypes["default"].object
};
var _default = Bread;
exports["default"] = _default;