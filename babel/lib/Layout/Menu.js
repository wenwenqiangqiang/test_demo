"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _antd = require("antd");

var _reactRouterDom = require("react-router-dom");

var _utils = require("utils");

var _pathToRegexp = _interopRequireDefault(require("path-to-regexp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Menus = function Menus(_ref) {
  var siderFold = _ref.siderFold,
      darkTheme = _ref.darkTheme,
      handleClickNavMenu = _ref.handleClickNavMenu,
      navOpenKeys = _ref.navOpenKeys,
      changeOpenKeys = _ref.changeOpenKeys,
      menu = _ref.menu,
      location = _ref.location;
  // 生成树状
  var menuTree = (0, _utils.arrayToTree)(menu.filter(function (_) {
    return _.mpid !== '-1';
  }), 'id', 'mpid');
  var levelMap = {};

  var refreshThisPage = function refreshThisPage() {
    //  window.location.href=location.pathname
    return false;
  }; // 递归生成菜单


  var getMenus = function getMenus(menuTreeN, siderFoldN) {
    return menuTreeN.map(function (item) {
      if (item.children) {
        if (item.mpid) {
          levelMap[item.id] = item.mpid;
        }

        return _react["default"].createElement(_antd.Menu.SubMenu, {
          key: item.id,
          title: _react["default"].createElement("span", null, item.icon && _react["default"].createElement(_antd.Icon, {
            type: item.icon
          }), (!siderFoldN || !menuTree.includes(item)) && item.name)
        }, getMenus(item.children, siderFoldN));
      }

      return _react["default"].createElement(_antd.Menu.Item, {
        key: item.id
      }, item.route === location.pathname && _react["default"].createElement("div", {
        onClick: function onClick() {
          refreshThisPage();
        }
      }, item.icon && _react["default"].createElement(_antd.Icon, {
        type: item.icon
      }), item.name), item.route !== location.pathname && _react["default"].createElement(_reactRouterDom.Link, {
        to: item.route || '#'
      }, item.icon && _react["default"].createElement(_antd.Icon, {
        type: item.icon
      }), item.name));
    });
  };

  var menuItems = getMenus(menuTree, siderFold); // 保持选中

  var getAncestorKeys = function getAncestorKeys(key) {
    var map = {};

    var getParent = function getParent(index) {
      var result = [String(levelMap[index])];

      if (levelMap[result[0]]) {
        result.unshift(getParent(result[0])[0]);
      }

      return result;
    };

    for (var index in levelMap) {
      if ({}.hasOwnProperty.call(levelMap, index)) {
        map[index] = getParent(index);
      }
    }

    return map[key] || [];
  };

  var onOpenChange = function onOpenChange(openKeys) {
    var latestOpenKey = openKeys.find(function (key) {
      return !navOpenKeys.includes(key);
    });
    var latestCloseKey = navOpenKeys.find(function (key) {
      return !openKeys.includes(key);
    });
    var nextOpenKeys = [];

    if (latestOpenKey) {
      nextOpenKeys = getAncestorKeys(latestOpenKey).concat(latestOpenKey);
    }

    if (latestCloseKey) {
      nextOpenKeys = getAncestorKeys(latestCloseKey);
    }

    changeOpenKeys(nextOpenKeys);
  };

  var menuProps = !siderFold ? {
    onOpenChange: onOpenChange,
    openKeys: navOpenKeys
  } : {}; // 寻找选中路由

  var currentMenu;
  var defaultSelectedKeys;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = menu[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var item = _step.value;

      if (item.route && (0, _pathToRegexp["default"])(item.route).exec(location.pathname)) {
        currentMenu = item;
        break;
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  var getPathArray = function getPathArray(array, current, pid, id) {
    var result = [String(current[id])];

    var getPath = function getPath(item) {
      if (item && item[pid]) {
        result.unshift(String(item[pid]));
        getPath((0, _utils.queryArray)(array, item[pid], id));
      }
    };

    getPath(current);
    return result;
  };

  if (currentMenu) {
    defaultSelectedKeys = getPathArray(menu, currentMenu, 'mpid', 'id');
  }

  if (!defaultSelectedKeys) {
    defaultSelectedKeys = ['11'];
  }

  return _react["default"].createElement(_antd.Menu, _extends({}, menuProps, {
    mode: "inline",
    theme: darkTheme ? 'dark' : 'light',
    onClick: handleClickNavMenu,
    selectedKeys: defaultSelectedKeys,
    inlineIndent: "20"
  }), menuItems);
};

Menus.propTypes = {
  menu: _propTypes["default"].array,
  siderFold: _propTypes["default"].bool,
  darkTheme: _propTypes["default"].bool,
  handleClickNavMenu: _propTypes["default"].func,
  navOpenKeys: _propTypes["default"].array,
  changeOpenKeys: _propTypes["default"].func,
  location: _propTypes["default"].object
};
var _default = Menus;
exports["default"] = _default;