"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _router = require("dva/router");

var _PageHeader = _interopRequireDefault(require("../PageHeader"));

var _PageHeaderLayoutForSelect = _interopRequireDefault(require("./PageHeaderLayoutForSelect.less"));

var _antd = require("antd");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Option = _antd.Select.Option;

var _default = function _default(_ref) {
  var children = _ref.children,
      wrapperClassName = _ref.wrapperClassName,
      top = _ref.top,
      selecData = _ref.selecData,
      selectedMendian = _ref.selectedMendian,
      selectedMendianData = _ref.selectedMendianData,
      handleMenuClick = _ref.handleMenuClick,
      restProps = _objectWithoutProperties(_ref, ["children", "wrapperClassName", "top", "selecData", "selectedMendian", "selectedMendianData", "handleMenuClick"]);

  // 处理下拉选项
  var getOptions = function getOptions(value) {
    return value.map(function (item, index) {
      return _react["default"].createElement(_antd.Menu.Item, {
        key: index
      }, item.name);
    });
  };

  var menu = _react["default"].createElement(_antd.Menu, {
    onClick: handleMenuClick
  }, getOptions(selecData));

  var getSelectOptions = function getSelectOptions(value) {
    return value.map(function (item, index) {
      return _react["default"].createElement(Option, {
        value: index,
        key: index
      }, item.name);
    });
  };

  var getdefaultValue = function getdefaultValue() {
    var indexdata = 0;
    selecData.forEach(function (element, index) {
      if (element.id == selectedMendianData.id) {
        indexdata = index;
      }
    });
    return indexdata;
  };

  return _react["default"].createElement("div", {
    style: {
      margin: '-24px -24px 0'
    },
    className: wrapperClassName
  }, top, _react["default"].createElement(_PageHeader["default"], _extends({
    key: "pageheader"
  }, restProps, {
    linkElement: _router.Link
  })), _react["default"].createElement("div", {
    className: _PageHeaderLayoutForSelect["default"].Dropdown
  }, _react["default"].createElement(_antd.Select, {
    onChange: handleMenuClick,
    style: {
      minWidth: '180px'
    },
    value: selectedMendian
  }, getSelectOptions(selecData))), children ? _react["default"].createElement("div", {
    className: _PageHeaderLayoutForSelect["default"].content
  }, children) : null);
};

exports["default"] = _default;