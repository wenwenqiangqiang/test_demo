"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _utils = require("utils");

var _Footer = _interopRequireDefault(require("./Footer.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Footer = function Footer() {
  return _react["default"].createElement("div", {
    className: _Footer["default"].footer
  }, "\u7531\u4E3D\u7EF4\u5BB6\u4EA7\u4E1A\u4E92\u8054\u7F51\u59D4\u5458\u4F1A\u63D0\u4F9B\u6280\u672F\u652F\u6301");
};

var _default = Footer;
exports["default"] = _default;