"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _antd = require("antd");

var _classnames = _interopRequireDefault(require("classnames"));

var _Header = _interopRequireDefault(require("./Header.less"));

var _Menu = _interopRequireDefault(require("./Menu"));

var _router = require("dva/router");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var SubMenu = _antd.Menu.SubMenu;

var Header = function Header(_ref) {
  var user = _ref.user,
      tenant = _ref.tenant,
      logout = _ref.logout,
      switchSider = _ref.switchSider,
      siderFold = _ref.siderFold,
      isNavbar = _ref.isNavbar,
      menuPopoverVisible = _ref.menuPopoverVisible,
      location = _ref.location,
      switchMenuPopover = _ref.switchMenuPopover,
      navOpenKeys = _ref.navOpenKeys,
      changeOpenKeys = _ref.changeOpenKeys,
      menu = _ref.menu;

  var handleClickMenu = function handleClickMenu(e) {
    return e.key === 'logout' && logout();
  };

  var menusProps = {
    menu: menu,
    siderFold: false,
    darkTheme: false,
    isNavbar: isNavbar,
    handleClickNavMenu: switchMenuPopover,
    location: location,
    navOpenKeys: navOpenKeys,
    changeOpenKeys: changeOpenKeys
  };
  return _react["default"].createElement("div", {
    className: _Header["default"].header,
    style: {
      left: isNavbar ? '0px' : siderFold ? '80px' : '200px'
    }
  }, isNavbar ? _react["default"].createElement(_antd.Popover, {
    placement: "bottomLeft",
    onVisibleChange: switchMenuPopover,
    visible: menuPopoverVisible,
    overlayClassName: _Header["default"].popovermenu,
    trigger: "click",
    content: _react["default"].createElement(_Menu["default"], menusProps)
  }, _react["default"].createElement("div", {
    className: _Header["default"].button
  }, _react["default"].createElement(_antd.Icon, {
    type: "bars"
  }))) : _react["default"].createElement("div", {
    className: _Header["default"].button,
    onClick: switchSider
  }, _react["default"].createElement(_antd.Icon, {
    type: (0, _classnames["default"])({
      'menu-unfold': siderFold,
      'menu-fold': !siderFold
    })
  })), _react["default"].createElement("div", {
    className: _Header["default"].rightWarpper
  }, _react["default"].createElement("a", {
    href: "http://siteadmin.liweijia.com/",
    target: "_blank",
    className: _Header["default"].site
  }, "\u5546\u54C1\u7BA1\u7406"), _react["default"].createElement(_router.Link, {
    className: _Header["default"].question,
    to: '/faq'
  }, "\u5E38\u89C1\u95EE\u9898", _react["default"].createElement(_antd.Icon, {
    type: "question-circle"
  })), _react["default"].createElement("div", {
    style: {
      marginTop: 13
    }
  }, tenant.displayName), _react["default"].createElement("div", {
    className: _Header["default"].button
  }, _react["default"].createElement(_antd.Icon, {
    type: "mail"
  })), _react["default"].createElement(_antd.Menu, {
    mode: "horizontal",
    onClick: handleClickMenu
  }, _react["default"].createElement(SubMenu, {
    style: {
      "float": 'right'
    },
    title: _react["default"].createElement("span", null, _react["default"].createElement(_antd.Icon, {
      type: "user"
    }), " ", user.name)
  }, _react["default"].createElement(_antd.Menu.Item, {
    key: "logout"
  }, "\u6CE8\u9500")))));
};

Header.propTypes = {
  menu: _propTypes["default"].array,
  user: _propTypes["default"].object,
  logout: _propTypes["default"].func,
  switchSider: _propTypes["default"].func,
  siderFold: _propTypes["default"].bool,
  isNavbar: _propTypes["default"].bool,
  menuPopoverVisible: _propTypes["default"].bool,
  location: _propTypes["default"].object,
  switchMenuPopover: _propTypes["default"].func,
  navOpenKeys: _propTypes["default"].array,
  changeOpenKeys: _propTypes["default"].func
};
var _default = Header;
exports["default"] = _default;