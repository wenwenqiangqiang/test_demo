"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _Loader = _interopRequireDefault(require("./Loader.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Loader = function Loader(_ref) {
  var _classNames;

  var spinning = _ref.spinning,
      fullScreen = _ref.fullScreen;
  return _react["default"].createElement("div", {
    className: (0, _classnames["default"])(_Loader["default"].loader, (_classNames = {}, _defineProperty(_classNames, _Loader["default"].hidden, !spinning), _defineProperty(_classNames, _Loader["default"].fullScreen, fullScreen), _classNames))
  }, _react["default"].createElement("div", {
    className: _Loader["default"].warpper
  }, _react["default"].createElement("div", {
    className: _Loader["default"].inner
  }), _react["default"].createElement("div", {
    className: _Loader["default"].text
  }, "LOADING")));
};

Loader.propTypes = {
  spinning: _propTypes["default"].bool,
  fullScreen: _propTypes["default"].bool
};
var _default = Loader;
exports["default"] = _default;