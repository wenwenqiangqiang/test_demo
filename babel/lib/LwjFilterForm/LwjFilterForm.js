"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _http = require("http");

var _LwjFilterForm = _interopRequireDefault(require("./LwjFilterForm.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var FormItem = _antd.Form.Item;
var Option = _antd.Select.Option;

var FilterForm = function FilterForm(_ref) {
  var filterItems = _ref.filterItems,
      getBackData = _ref.getBackData,
      _ref$form = _ref.form,
      getFieldDecorator = _ref$form.getFieldDecorator,
      getFieldsValue = _ref$form.getFieldsValue,
      setFieldsValue = _ref$form.setFieldsValue;
  // 表单项布局
  var formItemLayout = {
    labelCol: {
      xs: {
        span: 24
      },
      sm: {
        span: 8
      }
    },
    wrapperCol: {
      xs: {
        span: 24
      },
      sm: {
        span: 16
      }
    }
  };

  var getItems = function getItems(filterItems) {
    return filterItems.map(function (value) {
      return _react["default"].createElement(_antd.Col, {
        span: 8,
        key: value.id
      }, getItem(value));
    });
  };

  var getItem = function getItem(value) {
    switch (value.type) {
      case 'input':
        return _react["default"].createElement(FormItem, _extends({
          label: value.title
        }, formItemLayout), getFieldDecorator(value.id, {})(_react["default"].createElement(_antd.Input, {
          placeholder: value.placeholder
        })));

      case 'select':
        return _react["default"].createElement(FormItem, _extends({
          label: value.title
        }, formItemLayout), getFieldDecorator(value.id, {
          initialValue: value.defaultValue
        })(_react["default"].createElement(_antd.Select, {
          initialValue: value.defaultValue
        }, getOptions(value.options))));

      case 'date':
        return _react["default"].createElement(FormItem, _extends({
          label: value.title
        }, formItemLayout), getFieldDecorator(value.bid, {
          initialValue: value.defaultValue
        })(_react["default"].createElement(_antd.DatePicker, {
          onChange: dateChange,
          showTime: true,
          format: value.format || "YYYY/MM/DD",
          style: {
            width: 160
          }
        })), "\u81F3", getFieldDecorator(value.eid, {
          initialValue: value.defaultValue
        })(_react["default"].createElement(_antd.DatePicker, {
          showTime: true,
          format: value.format || "YYYY/MM/DD",
          style: {
            width: 160
          }
        })));
    }
  };

  var dateChange = function dateChange(value) {
    console.log(value);
  }; // 处理下拉选项


  var getOptions = function getOptions(value) {
    return value.map(function (option) {
      return _react["default"].createElement(Option, {
        value: option.value,
        key: option.value
      }, option.name);
    });
  };

  var pullOut = function pullOut() {
    var fields = getFieldsValue();
    getBackData(fields);
  };

  return _react["default"].createElement("div", null, _react["default"].createElement(_antd.Form, {
    inline: 'true'
  }, getItems(filterItems), _react["default"].createElement(_antd.Col, {
    span: 8,
    className: _LwjFilterForm["default"].center
  }, _react["default"].createElement(_antd.Button, {
    type: 'primary',
    onClick: pullOut
  }, "\u641C\u7D22"))));
};

var _default = _antd.Form.create()(FilterForm);

exports["default"] = _default;