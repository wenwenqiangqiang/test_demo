"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _antd = require("antd");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _LwjTable = _interopRequireDefault(require("./LwjTable.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var LwjTable = function LwjTable(_ref) {
  var dataSource = _ref.dataSource,
      pagination = _ref.pagination,
      loading = _ref.loading,
      columns = _ref.columns,
      selectType = _ref.selectType,
      onSelectRows = _ref.onSelectRows,
      dispatch = _ref.dispatch,
      pageChangeAction = _ref.pageChangeAction,
      onRowClick = _ref.onRowClick,
      onRowMouseEnter = _ref.onRowMouseEnter,
      _ref$selectedRowKeys = _ref.selectedRowKeys,
      selectedRowKeys = _ref$selectedRowKeys === void 0 ? [] : _ref$selectedRowKeys,
      extConfigs = _objectWithoutProperties(_ref, ["dataSource", "pagination", "loading", "columns", "selectType", "onSelectRows", "dispatch", "pageChangeAction", "onRowClick", "onRowMouseEnter", "selectedRowKeys"]);

  /**
   * 分页onchange方法
   * @param page
   */
  var onChange = function onChange(page) {
    dispatch({
      type: pageChangeAction,
      payload: {
        limit: page.pageSize,
        start: page.current - 1,
        isTablePagination: true
      }
    });
  };

  if (!onRowClick) {
    onRowClick = function onRowClick() {};
  }

  if (!onRowMouseEnter) {
    onRowMouseEnter = function onRowMouseEnter() {};
  }

  if (columns) {
    /**
     * 每一列可以使用align来指定排列样式,默认为居中
     * 参数 (left|right)
     */
    columns.map(function (col) {
      if (col.align === "left") {
        col.className = _LwjTable["default"].alignLeft;
      } else if (col.align === "right") {
        col.className = _LwjTable["default"].alignRight;
      } else if (col.align === "center") {
        col.className = _LwjTable["default"].alignCenter;
      } else {
        col.className = _LwjTable["default"].alignCenter;
      }
    });
  }
  /**
   * selectType参数用来指定列表单选或者多选
   * 参数：(multiple|single)
   */


  var TableProps = {};
  var rowSelection = {};

  if (selectType === 'multiple') {
    rowSelection = {
      type: 'check',
      onChange: onSelectRows,
      selectedRowKeys: selectedRowKeys
    };
  } else if (selectType === 'single') {
    rowSelection = {
      type: 'radio',
      onChange: onSelectRows,
      selectedRowKeys: selectedRowKeys
    };
  }

  var isRowSelectionNotNull = Object.keys(rowSelection).length !== 0;
  TableProps = _objectSpread({}, extConfigs);

  if (isRowSelectionNotNull) {
    TableProps.rowSelection = rowSelection;
  }

  return _react["default"].createElement(_antd.Table, _extends({}, TableProps, {
    onRow: function onRow(record) {
      return {
        onClick: function onClick() {
          onRowClick(record);
        },
        // 点击行
        onMouseEnter: function onMouseEnter() {
          onRowMouseEnter(record);
        } // 鼠标移入行

      };
    },
    dataSource: dataSource,
    pagination: pagination,
    onChange: onChange,
    loading: loading,
    className: _LwjTable["default"].table,
    columns: columns,
    size: "small",
    rowKey: function rowKey(record) {
      return record.id;
    }
  }));
};

LwjTable.propTypes = {
  dataSource: _propTypes["default"].array,
  pagination: _propTypes["default"].any,
  loading: _propTypes["default"].bool,
  columns: _propTypes["default"].array,
  selectType: _propTypes["default"].string,
  onSelectRows: _propTypes["default"].func,
  dispatch: _propTypes["default"].func,
  pageChangeAction: _propTypes["default"].string,
  onRowClick: _propTypes["default"].func
};
var _default = LwjTable;
exports["default"] = _default;