"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _imageService = _interopRequireDefault(require("./imageService"));

require("./LwjSowPicList.less");

var _path = require("path");

var _util = require("util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LwjSowPicList =
/*#__PURE__*/
function (_Component) {
  _inherits(LwjSowPicList, _Component);

  function LwjSowPicList() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LwjSowPicList);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LwjSowPicList)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      fileList: [],
      previewVisible: false,
      previewVisibleUrl: '',
      absoluteUrl: ''
    });

    _defineProperty(_assertThisInitialized(_this), "handleCancel", function () {
      return _this.setState({
        previewVisible: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "setAbsoluteUrl", function (url) {
      return _this.setState({
        absoluteUrl: url
      });
    });

    return _this;
  }

  _createClass(LwjSowPicList, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      if (this.props.fileLists) {
        var picUrlsAAA = this.props.fileLists;

        var getUrlAAA = function getUrlAAA(picUrls) {
          var promiseArr = [];
          var IMAGE_THUMBNAIL_PARAM_ALIOSS = '?x-oss-process=image/resize,m_lfit,h_400,w_400';

          if (picUrls && picUrls.split(';').length > 0) {
            var newpicUrls = picUrls.split(';');

            if (newpicUrls[newpicUrls.length - 1] == "") {
              newpicUrls.pop();
            }

            newpicUrls.map(function (img) {
              promiseArr.push(new Promise(function (resolve, reject) {
                _imageService["default"].show(img, function (imgUrl) {
                  var thumbnail = '';

                  if (imgUrl) {
                    thumbnail = imgUrl.split('?')[0] + IMAGE_THUMBNAIL_PARAM_ALIOSS;
                  }

                  resolve({
                    url: imgUrl,
                    thumbnail: thumbnail,
                    orginUrl: img
                  });
                });
              }));
            });
            return promiseArr;
          }
        };

        var dealedTargetUrlAAA = getUrlAAA(picUrlsAAA);
        var sureDealedUrlAAA = [];
        dealedTargetUrlAAA.forEach(function (value, index) {
          value.then(function (val) {
            var newName = val.orginUrl.split('/')[val.orginUrl.split('/').length - 1];
            sureDealedUrlAAA.push({
              name: newName.slice(-10),
              uid: index,
              url: val.url,
              thumbnail: val.thumbnail,
              orginUrl: val.orginUrl,
              response: {
                result: val.orginUrl
              }
            });

            _this2.setState({
              fileList: sureDealedUrlAAA
            });
          });
        });
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.setState({
        fileList: [],
        previewVisible: false,
        previewVisibleUrl: ''
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          title = _this$props.title,
          width = _this$props.width;
      var _this$state = this.state,
          fileList = _this$state.fileList,
          previewVisible = _this$state.previewVisible,
          previewVisibleUrl = _this$state.previewVisibleUrl; // 生成列表

      var getIMGlist = function getIMGlist(fileList) {
        console.info(fileList);

        if (fileList.length > 0) {
          var res = fileList.map(function (val) {
            return _react["default"].createElement(_antd.Card.Grid, {
              className: "LwjSowPicList",
              style: gridStyle,
              key: val.uid
            }, _react["default"].createElement("img", {
              alt: val.name,
              src: val.url,
              style: {
                height: 40
              },
              className: _.isEmpty(width) ? 'LwjSowPicList_showPic' : ''
            }), _react["default"].createElement("div", {
              className: "LwjSowPicList_showPic_preview"
            }, _react["default"].createElement(_antd.Button, {
              shape: "circle",
              icon: "eye",
              style: {
                marginTop: '31%'
              },
              at: val.url,
              size: "small",
              onClick: preView,
              ghost: true
            })));
          });
          return res;
        }
      }; // 布局样式


      var gridStyle = {
        width: _.isEmpty(width) ? '12.5%' : width,
        textAlign: 'center'
      };

      var preView = function preView(e) {
        var targetUrl = e.target.getAttribute('at');
        console.log(e.target);

        _this3.setState({
          previewVisible: true,
          previewVisibleUrl: targetUrl
        });
      };

      return _react["default"].createElement("div", {
        className: "clearfix"
      }, _react["default"].createElement(_antd.Card, {
        title: title
      }, fileList.length > 0 && getIMGlist(fileList)), _react["default"].createElement(_antd.Modal, {
        visible: previewVisible,
        footer: null,
        onCancel: this.handleCancel
      }, _react["default"].createElement("img", {
        alt: "example",
        style: {
          width: '100%'
        },
        src: previewVisibleUrl
      })));
    }
  }]);

  return LwjSowPicList;
}(_react.Component);

exports["default"] = LwjSowPicList;
LwjSowPicList.props = {
  title: _propTypes["default"].string.isRequired,
  fileLists: _propTypes["default"].string.isRequired,
  width: _propTypes["default"].string
};