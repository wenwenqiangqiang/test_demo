"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.ALIYUN_OSS_HW_PARAMS = exports.ALIYUN_OSS_URL = exports.URL_GET_PICTURE_URL = void 0;

var _superagent = _interopRequireDefault(require("superagent"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SERVICE_ENDPOINT = '/services/';
var URL_GET_PICTURE_URL = SERVICE_ENDPOINT + 'fileDownload?returnType=url';
exports.URL_GET_PICTURE_URL = URL_GET_PICTURE_URL;
var ALIYUN_OSS_URL = '.oss-cn-beijing.aliyuncs.com';
exports.ALIYUN_OSS_URL = ALIYUN_OSS_URL;
var ALIYUN_OSS_HW_PARAMS = '?x-oss-process';
exports.ALIYUN_OSS_HW_PARAMS = ALIYUN_OSS_HW_PARAMS;

var ImageService = function ImageService() {
  _classCallCheck(this, ImageService);
};

ImageService.show = function (img) {
  var onComplete = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function (imgUrl) {};

  if (img.indexOf(ALIYUN_OSS_URL) > -1) {
    if (img.indexOf(ALIYUN_OSS_HW_PARAMS) > -1) {
      var imgUrl = img.substring(0, img.indexOf(ALIYUN_OSS_HW_PARAMS));
      onComplete(imgUrl);
    } else {
      onComplete(img);
    }
  } else {
    _superagent["default"].get(URL_GET_PICTURE_URL + '&filePath=' + img).responseType('text').end(function (err, res) {
      try {
        var _imgUrl = err.rawResponse;
        onComplete(_imgUrl);
      } catch (e) {
        onComplete(null);
      }
    });
  }
};

var _default = ImageService;
exports["default"] = _default;