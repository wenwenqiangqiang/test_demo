"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _imageService = _interopRequireDefault(require("./imageService"));

var _qs = _interopRequireDefault(require("qs"));

var _urlencode = _interopRequireDefault(require("urlencode"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var dealName = function dealName(fileList) {
  fileList.forEach(function (val) {
    if (val.name.length > 25) {
      val.name.split('.');
      var count = val.name.lastIndexOf('.');
      var length = val.name.length - count;
      val.name = val.name.substring(0, 22 - length) + '...' + val.name.substring(count, val.name.length);
    }
  });
  return fileList;
};

var LwjUpload =
/*#__PURE__*/
function (_Component) {
  _inherits(LwjUpload, _Component);

  function LwjUpload() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LwjUpload);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LwjUpload)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      fileList: []
    });

    return _this;
  }

  _createClass(LwjUpload, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      // 元处理方式
      if (this.props.fileLists) {
        var picUrlsAAA = this.props.fileLists;

        var getUrlAAA = function getUrlAAA(picUrls) {
          var promiseArr = [];
          var IMAGE_THUMBNAIL_PARAM_ALIOSS = '?x-oss-process=image/resize,m_lfit,h_400,w_400';

          if (picUrls && picUrls.split(';').length > 0) {
            var newpicUrls = picUrls.split(';');

            if (newpicUrls[newpicUrls.length - 1] == "") {
              newpicUrls.pop();
            }

            newpicUrls.map(function (img) {
              promiseArr.push(new Promise(function (resolve, reject) {
                _imageService["default"].show(img, function (imgUrl) {
                  var thumbnail = '';

                  if (imgUrl) {
                    thumbnail = imgUrl.split('?')[0] + IMAGE_THUMBNAIL_PARAM_ALIOSS;
                  }

                  resolve({
                    url: imgUrl,
                    thumbnail: thumbnail,
                    orginUrl: img
                  });
                });
              }));
            });
            return promiseArr;
          }
        };

        var dealedTargetUrlAAA = getUrlAAA(picUrlsAAA);
        var sureDealedUrlAAA = [];
        dealedTargetUrlAAA.forEach(function (value, index) {
          value.then(function (val) {
            // let newName = val   .orginUrl   .split('/')[     val       .orginUrl
            // .split('/')       .length - 1   ];
            var name = _urlencode["default"].decode(_qs["default"].parse(val.url)['response-content-disposition'], 'utf-8');

            var tname = name.slice(name.indexOf("filename*=UTF-8") + 17);

            var reName = _qs["default"].parse(val.url).fileName;

            sureDealedUrlAAA.push({
              name: tname || reName,
              uid: index,
              url: val.url,
              response: {
                result: val.orginUrl
              }
            });
            sureDealedUrlAAA = dealName(sureDealedUrlAAA);

            _this2.setState({
              fileList: sureDealedUrlAAA
            });
          });
        });
      } // 会有时效问题 if (this.props.fileLists) {   let dealedTargetUrlAAA =
      // this.props.fileLists;   let sureDealedUrlAAA = [];   dealedTargetUrlAAA
      // .split(';')     .forEach((value, index) => {       const name =
      // urlencode.decode(qs.parse(value)['response-content-disposition'], 'utf-8')
      // const tname = name.slice(name.indexOf("filename*=UTF-8") + 17)
      // sureDealedUrlAAA.push({         name: tname,         uid: index,         url:
      // value,         response: {           result: value         }       })
      // this.setState({fileList: sureDealedUrlAAA});     }) }

    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          uploadTitle = _this$props.uploadTitle,
          _this$props$uploadIco = _this$props.uploadIcon,
          uploadIcon = _this$props$uploadIco === void 0 ? "" : _this$props$uploadIco,
          _this$props$btnType = _this$props.btnType,
          btnType = _this$props$btnType === void 0 ? "ghost" : _this$props$btnType,
          target = _this$props.target,
          fileType = _this$props.fileType,
          _this$props$fileSize = _this$props.fileSize,
          fileSize = _this$props$fileSize === void 0 ? 99999 : _this$props$fileSize,
          _this$props$showUploa = _this$props.showUploadList,
          showUploadList = _this$props$showUploa === void 0 ? true : _this$props$showUploa,
          onChange = _this$props.onChange,
          accept = _this$props.accept,
          maxfile = _this$props.maxfile;
      var fileList = this.state.fileList;

      var _self = this;

      var proper = {
        action: target,
        showUploadList: showUploadList,
        accept: accept,
        multiple: maxfile > 1,
        beforeUpload: function beforeUpload(file) {
          var flag = true; // 尺寸监测暂时取消 if (imgWidth && imgHeight) {     flag = testInch(file); }

          if (fileType) {
            var isJPG = file.type === fileType;

            if (!isJPG) {
              _antd.Modal.info({
                content: "\u53EA\u80FD\u4E0A\u4F20 ".concat(fileType, " \u683C\u5F0F\u7684\u6587\u4EF6\u54E6\uFF01")
              });

              flag = false;
            }
          }

          return flag;
        },
        fileList: fileList
      };

      var preChange = function preChange(_ref) {
        var file = _ref.file,
            fileList = _ref.fileList,
            event = _ref.event;
        console.log(fileList);
        fileList = dealName(fileList);

        _this3.setState({
          fileList: fileList
        }); // 限制尺寸


        var isLt2M = file.size / 1024 / 1024 < fileSize;

        if (file.size && !isLt2M) {
          fileList.forEach(function (val, index) {
            if (val.uid == file.uid) {
              fileList.splice(index, 1);
            }
          });

          _this3.setState({
            fileList: fileList
          });

          _antd.Modal.error({
            content: "\u53EA\u80FD\u4E0A\u4F20\u5C0F\u4E8E ".concat(fileSize, " M\u7684\u6587\u4EF6\u54E6\uFF01")
          });
        } // 获取下载链接


        if (file.response) {
          var picUrlsAAA = file.response.result;

          var getUrlAAA = function getUrlAAA(picUrls) {
            var promiseArr = [];
            var IMAGE_THUMBNAIL_PARAM_ALIOSS = '?x-oss-process=image/resize,m_lfit,h_400,w_400';

            if (picUrls && picUrls.split(';').length > 0) {
              picUrls.split(';').map(function (img) {
                promiseArr.push(new Promise(function (resolve, reject) {
                  _imageService["default"].show(img, function (imgUrl) {
                    var thumbnail = '';

                    if (imgUrl) {
                      thumbnail = imgUrl.split('?')[0] + IMAGE_THUMBNAIL_PARAM_ALIOSS;
                    }

                    resolve({
                      url: imgUrl,
                      thumbnail: thumbnail
                    });
                  });
                }));
              });
              return promiseArr;
            }
          };

          var dealedTargetUrlAAA = getUrlAAA(picUrlsAAA);
          var sureDealedUrlAAA = [];
          dealedTargetUrlAAA.forEach(function (value, index) {
            value.then(function (val) {
              sureDealedUrlAAA.push({
                name: index,
                uid: file.uid,
                url: val.url,
                response: {
                  result: val.url
                }
              });
              fileList.forEach(function (val) {
                if (val.uid == sureDealedUrlAAA[0].uid) {
                  val.url = sureDealedUrlAAA[0].url;
                }
              });

              _this3.setState({
                fileList: fileList
              }); // 与处理数据


              var targetValue = '';

              for (var i = 0; i < fileList.length; i++) {
                if (fileList[i].response) {
                  targetValue += fileList[i].response.result + ';';
                }
              }

              targetValue = targetValue.substring(0, targetValue.length - 1);

              if (targetValue == '') {
                onChange('', _this3.props.index);
              }

              if (targetValue) {
                onChange(targetValue, _this3.props.index);
              }
            });
          });
        }
      }; // const testInch = (file) => {     let reader = new FileReader();     const
      // tHeight = imgHeight;     const tWidrh = imgWidth;     reader.onload =
      // function (theFile) {         let image = new Image();         image.onload =
      // function () {             let width = this.width;             let height =
      // this.height;             if (width !== tWidrh || height !== tHeight) {
      // Modal.info({content: `上传图片尺寸要求为${tWidrh}*${tHeight},请重新选择！`}) return false; }
      // else {                 return true;        }    } image.src =
      // theFile.target.result;     } reader.readAsDataURL(file); }


      var uploadButton = _react["default"].createElement(_antd.Button, {
        type: btnType,
        disabled: fileList.length >= maxfile ? true : false
      }, _react["default"].createElement(_antd.Icon, {
        type: uploadIcon
      }), " ", uploadTitle);

      return _react["default"].createElement(_antd.Upload, _extends({}, proper, {
        onChange: preChange
      }), uploadButton);
    }
  }]);

  return LwjUpload;
}(_react.Component);

exports["default"] = LwjUpload;
LwjUpload.props = {
  uploadTitle: _propTypes["default"].string,
  uploadIcon: _propTypes["default"].string,
  btnType: _propTypes["default"].string,
  target: _propTypes["default"].string.isRequired,
  name: _propTypes["default"].string,
  fileType: _propTypes["default"].string,
  fileSize: _propTypes["default"].number,
  onChange: _propTypes["default"].func,
  showUploadList: _propTypes["default"].bool
};