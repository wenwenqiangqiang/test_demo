"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _antd = require("antd");

var _index = _interopRequireDefault(require("./index.less"));

var _responsive = _interopRequireDefault(require("./responsive"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Description = function Description(_ref) {
  var term = _ref.term,
      column = _ref.column,
      tooltip = _ref.tooltip,
      className = _ref.className,
      children = _ref.children,
      restProps = _objectWithoutProperties(_ref, ["term", "column", "tooltip", "className", "children"]);

  var clsString = (0, _classnames["default"])(_index["default"].description, className);
  return _react["default"].createElement(_antd.Col, _extends({
    className: clsString
  }, _responsive["default"][column], restProps), term && _react["default"].createElement("div", {
    className: _index["default"].term
  }, term), _react["default"].createElement("div", {
    className: _index["default"].detail
  }, _react["default"].createElement(_antd.Tooltip, {
    placement: "topLeft",
    title: tooltip
  }, children ? children : "无")));
};

Description.defaultProps = {
  term: ''
};
Description.propTypes = {
  term: _propTypes["default"].node
};
var _default = Description;
exports["default"] = _default;