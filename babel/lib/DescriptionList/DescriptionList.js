"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _antd = require("antd");

var _index = _interopRequireDefault(require("./index.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var DescriptionList = function DescriptionList(_ref) {
  var _classNames;

  var className = _ref.className,
      title = _ref.title,
      _ref$col = _ref.col,
      col = _ref$col === void 0 ? 3 : _ref$col,
      _ref$layout = _ref.layout,
      layout = _ref$layout === void 0 ? 'horizontal' : _ref$layout,
      _ref$gutter = _ref.gutter,
      gutter = _ref$gutter === void 0 ? 32 : _ref$gutter,
      children = _ref.children,
      size = _ref.size,
      restProps = _objectWithoutProperties(_ref, ["className", "title", "col", "layout", "gutter", "children", "size"]);

  var clsString = (0, _classnames["default"])(_index["default"].descriptionList, _index["default"][layout], className, (_classNames = {}, _defineProperty(_classNames, _index["default"].small, size === 'small'), _defineProperty(_classNames, _index["default"].large, size === 'large'), _classNames));
  var column = col > 4 ? 4 : col;
  return _react["default"].createElement("div", _extends({
    className: clsString
  }, restProps), title ? _react["default"].createElement("div", {
    className: _index["default"].title
  }, title) : null, _react["default"].createElement(_antd.Row, {
    gutter: gutter
  }, _react["default"].Children.map(children, function (child) {
    return child ? _react["default"].cloneElement(child, {
      column: column
    }) : child;
  })));
};

var _default = DescriptionList;
exports["default"] = _default;