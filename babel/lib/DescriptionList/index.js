"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _DescriptionList = _interopRequireDefault(require("./DescriptionList"));

var _Description = _interopRequireDefault(require("./Description"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_DescriptionList["default"].Description = _Description["default"];
var _default = _DescriptionList["default"];
exports["default"] = _default;