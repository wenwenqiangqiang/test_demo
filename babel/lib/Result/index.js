"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = Result;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _antd = require("antd");

var _index = _interopRequireDefault(require("./index.less"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Result(_ref) {
  var className = _ref.className,
      type = _ref.type,
      title = _ref.title,
      description = _ref.description,
      extra = _ref.extra,
      actions = _ref.actions,
      restProps = _objectWithoutProperties(_ref, ["className", "type", "title", "description", "extra", "actions"]);

  var iconMap = {
    error: _react["default"].createElement(_antd.Icon, {
      className: _index["default"].error,
      type: "close-circle"
    }),
    success: _react["default"].createElement(_antd.Icon, {
      className: _index["default"].success,
      type: "check-circle"
    })
  };
  var clsString = (0, _classnames["default"])(_index["default"].result, className);
  return _react["default"].createElement("div", _extends({
    className: clsString
  }, restProps), _react["default"].createElement("div", {
    className: _index["default"].icon
  }, iconMap[type]), _react["default"].createElement("div", {
    className: _index["default"].title
  }, title), description && _react["default"].createElement("div", {
    className: _index["default"].description
  }, description), extra && _react["default"].createElement("div", {
    className: _index["default"].extra
  }, extra), actions && _react["default"].createElement("div", {
    className: _index["default"].actions
  }, actions));
}