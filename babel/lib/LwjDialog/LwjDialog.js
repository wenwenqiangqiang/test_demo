"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function notice(type, options) {
  if (!options.onOk || typeof options.onOk != 'function') {
    options.onOk = function () {
      console.log('丽维家弹框默认回调！');
    };
  }

  if (!options.onCancel || typeof options.onCancel != 'function') {
    options.onCancel = function () {
      console.log('丽维家弹框默认回调！');
    };
  }

  options.okText = options.okText || '确认';
  options.cancelText = options.cancelText || '取消';
  return _antd.Modal[type](options);
}

exports['default'] = {
  info: function info(options) {
    return notice('info', options);
  },
  success: function success(options) {
    return notice('success', options);
  },
  error: function error(options) {
    return notice('error', options);
  },
  confirm: function error(options) {
    return notice('confirm', options);
  }
};
module.exports = exports['default'];