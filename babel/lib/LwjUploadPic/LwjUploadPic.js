"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _imageService = _interopRequireDefault(require("./imageService"));

var _qs = _interopRequireDefault(require("qs"));

var _urlencode = _interopRequireDefault(require("urlencode"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LwjUploadPic =
/*#__PURE__*/
function (_Component) {
  _inherits(LwjUploadPic, _Component);

  function LwjUploadPic() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LwjUploadPic);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LwjUploadPic)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      previewVisible: false,
      previewImage: '',
      fileList: []
    });

    _defineProperty(_assertThisInitialized(_this), "handleCancel", function () {
      return _this.setState({
        previewVisible: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handlePreview", function (file) {
      _this.setState({
        previewImage: file.url || file.thumbUrl,
        previewVisible: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (_ref) {
      var fileList = _ref.fileList;

      _this.setState({
        fileList: fileList
      });
    });

    return _this;
  }

  _createClass(LwjUploadPic, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      // 元处理方式
      if (this.props.fileLists) {
        var picUrlsAAA = this.props.fileLists;

        var getUrlAAA = function getUrlAAA(picUrls) {
          var promiseArr = [];
          var IMAGE_THUMBNAIL_PARAM_ALIOSS = '?x-oss-process=image/resize,m_lfit,h_400,w_400';

          if (picUrls && picUrls.split(';').length > 0) {
            var newpicUrls = picUrls.split(';');

            if (newpicUrls[newpicUrls.length - 1] == "") {
              newpicUrls.pop();
            }

            newpicUrls.map(function (img) {
              promiseArr.push(new Promise(function (resolve, reject) {
                _imageService["default"].show(img, function (imgUrl) {
                  var thumbnail = '';

                  if (imgUrl) {
                    thumbnail = imgUrl.split('?')[0] + IMAGE_THUMBNAIL_PARAM_ALIOSS;
                  }

                  resolve({
                    url: imgUrl,
                    thumbnail: thumbnail,
                    orginUrl: img
                  });
                });
              }));
            });
            return promiseArr;
          }
        };

        var dealedTargetUrlAAA = getUrlAAA(picUrlsAAA);
        var sureDealedUrlAAA = [];
        dealedTargetUrlAAA.forEach(function (value, index) {
          value.then(function (val) {
            var name = _urlencode["default"].decode(_qs["default"].parse(val.url)['response-content-disposition'], 'utf-8');

            var tname = name.slice(name.indexOf("filename*=UTF-8") + 17);
            sureDealedUrlAAA.push({
              name: tname,
              uid: index,
              url: val.url,
              response: {
                result: val.orginUrl
              }
            });

            _this2.setState({
              fileList: sureDealedUrlAAA
            });
          });
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          maxfile = _this$props.maxfile,
          target = _this$props.target,
          onChange = _this$props.onChange;
      var _this$state = this.state,
          previewVisible = _this$state.previewVisible,
          previewImage = _this$state.previewImage,
          fileList = _this$state.fileList;

      var uploadButton = _react["default"].createElement("div", null, _react["default"].createElement(_antd.Icon, {
        type: "plus"
      }), _react["default"].createElement("div", {
        className: "ant-upload-text"
      }, "Upload"));

      var preChange = function preChange(_ref2) {
        var fileList = _ref2.fileList;

        _this3.setState({
          fileList: fileList
        }); // 与处理数据


        var targetValue = '';

        for (var i = 0; i < fileList.length; i++) {
          if (fileList[i].response) {
            targetValue += fileList[i].response.result + ';';
          }
        }

        targetValue = targetValue.substring(0, targetValue.length - 1);
        onChange(targetValue);
      };

      return _react["default"].createElement("div", {
        className: "clearfix"
      }, _react["default"].createElement(_antd.Upload, {
        action: target,
        listType: "picture-card",
        fileList: fileList,
        onPreview: this.handlePreview,
        onChange: preChange
      }, fileList.length >= maxfile ? null : uploadButton), _react["default"].createElement(_antd.Modal, {
        visible: previewVisible,
        footer: null,
        onCancel: this.handleCancel
      }, _react["default"].createElement("img", {
        alt: "example",
        style: {
          width: '100%'
        },
        src: previewImage
      })));
    }
  }]);

  return LwjUploadPic;
}(_react.Component);

exports["default"] = LwjUploadPic;
LwjUploadPic.props = {
  maxfile: _propTypes["default"].number,
  target: _propTypes["default"].string.isRequired,
  change: _propTypes["default"].func,
  fileLists: _propTypes["default"].string
};