"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _imageService = _interopRequireDefault(require("./imageService"));

var _superagent = _interopRequireDefault(require("superagent"));

var _qs = _interopRequireDefault(require("qs"));

var _urlencode = _interopRequireDefault(require("urlencode"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LwjDownload =
/*#__PURE__*/
function (_Component) {
  _inherits(LwjDownload, _Component);

  function LwjDownload() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LwjDownload);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LwjDownload)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      fileList: []
    });

    return _this;
  }

  _createClass(LwjDownload, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      if (this.props.targetUrl) {
        var picUrlsAAA = this.props.targetUrl;

        var getUrlAAA = function getUrlAAA(picUrls) {
          var promiseArr = [];
          var IMAGE_THUMBNAIL_PARAM_ALIOSS = '?x-oss-process=image/resize,m_lfit,h_400,w_400';

          if (picUrls && picUrls.split(';').length > 0) {
            var newpicUrls = picUrls.split(';');

            if (newpicUrls[newpicUrls.length - 1] == "") {
              newpicUrls.pop();
            }

            newpicUrls.map(function (img) {
              promiseArr.push(new Promise(function (resolve, reject) {
                _imageService["default"].show(img, function (imgUrl) {
                  var thumbnail = '';

                  if (imgUrl) {
                    thumbnail = imgUrl.split('?')[0] + IMAGE_THUMBNAIL_PARAM_ALIOSS;
                  }

                  resolve({
                    url: imgUrl,
                    thumbnail: thumbnail,
                    orginUrl: img
                  });
                });
              }));
            });
            return promiseArr;
          }
        };

        var dealedTargetUrlAAA = getUrlAAA(picUrlsAAA);
        var sureDealedUrlAAA = [];

        if (dealedTargetUrlAAA) {
          dealedTargetUrlAAA.forEach(function (value, index) {
            value.then(function (val) {
              var name = _urlencode["default"].decode(_qs["default"].parse(val.url)['response-content-disposition'], 'utf-8');

              var tname = name.slice(name.indexOf("filename*=UTF-8") + 17);

              var reName = _qs["default"].parse(val.url).fileName;

              sureDealedUrlAAA.push({
                name: tname,
                reName: reName,
                uid: index,
                url: val.url,
                response: {
                  result: val.orginUrl
                }
              });

              _this2.setState({
                fileList: sureDealedUrlAAA
              });
            });
          });
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var downloadIcon = this.props.downloadIcon;
      var fileList = this.state.fileList;
      /**
       * 地址预处理
       */
      // let picUrls = targetUrl; if (_.isEmpty(targetUrl)) {   title = "无"; } const
      // getUrl = (picUrls) => {   let promiseArr = [];   const
      // IMAGE_THUMBNAIL_PARAM_ALIOSS =
      // '?x-oss-process=image/resize,m_lfit,h_400,w_400';   if (picUrls &&
      // picUrls.split(';').length > 0) {     picUrls       .split(';')       .map(img
      // => {         promiseArr.push(new Promise(function (resolve, reject) {
      // ImageService.show(img, (imgUrl) => {             let thumbnail = '';      if
      // (imgUrl) {               thumbnail = imgUrl.split('?')[0] +
      // IMAGE_THUMBNAIL_PARAM_ALIOSS;             }             resolve({url: imgUrl,
      // thumbnail: thumbnail});           });         }));       });     return
      // promiseArr;   } } let dealedTargetUrl = []; if (!_.isEmpty(targetUrl)) {
      // dealedTargetUrl = getUrl(picUrls); } let sureDealedUrl = []; if
      // (dealedTargetUrl) {   dealedTargetUrl.forEach((value) => { value.then((val)
      // => {       sureDealedUrl.push(val.url)     })   }) }

      var Downer = function (files) {
        var h5Down = !/Trident|MSIE/.test(navigator.userAgent);
        /**
               * 在支持 download 属性的情况下使用该方法进行单个文件下载
               * @param  {String} fileName
               * @param  {String|FileObject} contentOrPath
               * @return {Null}
               */

        function downloadFile(fileName, contentOrPath) {
          var aLink = document.createElement("a"),
              evt = document.createEvent("HTMLEvents"),
              isData = contentOrPath.slice(0, 5) === "data:",
              isPath = contentOrPath.lastIndexOf(".") > -1; // 初始化点击事件 注：initEvent 不加后两个参数在FF下会报错

          evt.initEvent("click", false, false); // 添加文件下载名

          aLink.download = fileName; // 如果是 path 或者 dataURL 直接赋值 如果是 file 或者其他内容，使用 Blob 转换

          aLink.href = isPath || isData ? contentOrPath : URL.createObjectURL(new Blob([contentOrPath])); //执行下载

          aLink.click();
        }
        /**
               * 兼容IE
               * @param  {String} fileName
               * @param  {String|FileObject} contentOrPath
               */


        function IEdownloadFile(fileName, contentOrPath, bool) {
          var isImg = contentOrPath.slice(0, 10) === "data:image",
              ifr = document.createElement('iframe');
          ifr.style.display = 'none';
          ifr.src = contentOrPath;
          document.body.appendChild(ifr); // dataURL 的情况

          isImg && ifr.contentWindow.document.write("<img src='" + contentOrPath + "' />"); // 保存页面 -> 保存文件 alert(ifr.contentWindow.document.body.innerHTML)

          if (bool) {
            ifr.contentWindow.document.execCommand('SaveAs', false, fileName);
            document.body.removeChild(ifr);
          } else {
            setTimeout(function () {
              ifr.contentWindow.document.execCommand('SaveAs', false, fileName);
              document.body.removeChild(ifr);
            }, 0);
          }
        }
        /**
               * 截取文件名
               * @param  {String} str [description]
               * @return {String}     [description]
               */


        function parseURL(str) {
          return str.lastIndexOf("/") > -1 ? str.slice(str.lastIndexOf("/") + 1) : str;
        }

        return function (files) {
          // 选择下载函数
          var downer = h5Down ? downloadFile : IEdownloadFile; // 判断类型，处理下载文件名

          if (files instanceof Array) {
            for (var i = 0, l = files.length; i < l; i++) {
              downer(parseURL(files[i]), files[i], true);
            }
          } else if (typeof files === "string") {
            downer(parseURL(files), files);
          } else {
            // 对象
            for (var file in files) {
              downer(file, files[file]);
            }
          }
        };
      }();

      var downloadfile = function downloadfile(e) {
        var tarindex = e.target.getAttribute('at');

        if (_.isEmpty(fileList[tarindex])) {
          _antd.Modal.info({
            title: '文件不存在'
          });

          return;
        }

        Downer(fileList[tarindex].url);
      };

      var getdownload = function getdownload(dealedtargetUrl) {
        var res = dealedtargetUrl.map(function (val, index) {
          if (downloadIcon) {
            return _react["default"].createElement(_react.Fragment, {
              key: index
            }, _react["default"].createElement(_antd.Button, {
              type: "primary",
              at: index,
              onClick: downloadfile,
              shape: "circle",
              icon: downloadIcon
            }), _react["default"].createElement("br", null));
          } else {
            return _react["default"].createElement(_react.Fragment, {
              key: index
            }, _react["default"].createElement("a", {
              style: {
                marginRight: 10
              },
              href: 'javascript:;',
              at: index,
              onClick: downloadfile
            }, val.name || val.reName || val.url && val.url.split('/') && val.url.split('/').length && val.url.split('/')[val.url.split('/').length - 1] || '点击下载'), _react["default"].createElement("br", null));
          }
        });
        return res;
      };

      return _react["default"].createElement(_react.Fragment, null, fileList && getdownload(fileList));
    }
  }]);

  return LwjDownload;
}(_react.Component);

exports["default"] = LwjDownload;
LwjDownload.propTypes = {
  title: _propTypes["default"].string,
  targetUrl: _propTypes["default"].string,
  downloadIcon: _propTypes["default"].string
};