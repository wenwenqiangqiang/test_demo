# LWJ组件使用

# LwjButton
相比原生antd的Button,增加了Tooltip的使用

### Demo

```
<LwjButton tooltip="查看"
        type="primary" 
        shape="circle"
        size="small" icon="eye"
        onClick={() => onShowDetail(record)} />
```

参数解释
1. tooltip为文字提示
2. 其他的参数和antd的button组件一样,请直接看官方文档

---

## LwjArea
省市区级联组件
### 入门Demo-数据源为默认配置

```
<LwjArea onChange={onChange} width={100} />
```

参数解释
1.非必填,onChange为选中后的回掉函数,参数为选择的省市区数据,格式为数组格式.如:
 
``` 
const onChange = (value) => {
       console.log(value); // ["130000", "130300", "130303"]
}
```

2.非必填,width为当前组件长度

----

### 后端读取数据源的使用方式

```
<LwjArea
        dataSource="backEnd"
        getProvinces={getProvinces}
        getCitiesByProvince={getCitiesByProvince}
        getAreaByCity={getAreaByCity}
        onChange={onChange}
        width={100} />
```

参数解释
1.必填项,指定数据源为backEnd *
2.必填项,getProvinces,获取所有省份的方法.
数据格式为:

```
let province = [{
    "name": "北京市",
    "id": "110000"
}, {
    "name": "天津市",
    "id": "120000"
}, {
    "name": "河北省",
    "id": "130000"
}]
```
3.必填项,getCitiesByProvince,根据省份ID获取城市信息.
返回数据格式:

```
[
    {
        "province": "北京市", 
        "name": "市辖区", 
        "id": "110100"
    }
]

```

4.必填项,getAreaByCity,根据城市ID获取区域信息
返回数据格式:

```
    [
        {
            "city": "市辖区", 
            "name": "东城区", 
            "id": "110101"
        }, 
        {
            "city": "市辖区", 
            "name": "西城区", 
            "id": "110102"
        }, 
        {
            "city": "市辖区", 
            "name": "朝阳区", 
            "id": "110105"
        }, 
        {
            "city": "市辖区", 
            "name": "丰台区", 
            "id": "110106"
        }
    ]
```

## LwjTable

### Demo

```
    <LwjTable
            dataSource={dataSource}
            pagination={pagination}
            onChange={onChange}
            loading={loading}            
            columns={columns}
            selectType={selectType}
            onSelectRows={onSelectRows}
            align={align}/>  
```

参数解释:
1. 必填项,dataSource,pagination,onChange,loading,columns请参考官方文档
2. 非必填,如果列表需要选择,使用selectType属性.(multiple|single)
3. 非必填,onSelectRows,选中列的回掉函数
4. 非必填,align,指定列的对齐格式.(left|right|center)

## LwjTree

### Demo

```
<LwjTree dataSource={dataSource}/>
```

dataSource为数据来源,
数据格式为:

```
[{
		"id": "0",
		"updateTime": 1499225724466,
		"createTime": 1499225724466,
		"version": 0,
		"enabled": false,
		"name": "资源组",
		"priority": -1,
		"description": "资源组根节点"
	}, {
		"id": "8e0ee2d8b84e45459f837d77a873144b",
		"updateTime": 1510884680135,
		"createTime": 1510884680135,
		"version": 0,
		"enabled": false,
		"name": "财务管理",
		"code": "000001",
		"parentId": "0",
		"priority": 0,
		"description": "财务管理"
	}]
```

可选参数:

```
multiple : 是否多选,
checkable : 是否可以选择,
defaultExpandAll : 是否默认展开所有,
onCheck : 选中方法,返回选中的节点数据,
onSelect : 点击方法,返回点击的节点数据,
canEdit : 是否可编辑,
onAdd : 添加节点的方法, 
onDelete : 删除节点的方法
```







