"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.urlToList = urlToList;

// /userinfo/2144/id => ['/userinfo','/useinfo/2144,'/userindo/2144/id']
function urlToList(url) {
  var urllist = url.split('/').filter(function (i) {
    return i;
  });
  return urllist.map(function (urlItem, index) {
    return "/".concat(urllist.slice(0, index + 1).join('/'));
  });
}