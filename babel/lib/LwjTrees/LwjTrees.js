"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _LwjButton = _interopRequireDefault(require("components/LwjButton/LwjButton"));

var _util = require("util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TreeNode = _antd.Tree.TreeNode;

var LwjTree = function LwjTree(_ref) {
  var dataSource = _ref.dataSource,
      _ref$multiple = _ref.multiple,
      multiple = _ref$multiple === void 0 ? false : _ref$multiple,
      _ref$checkable = _ref.checkable,
      checkable = _ref$checkable === void 0 ? false : _ref$checkable,
      _ref$defaultExpandAll = _ref.defaultExpandAll,
      defaultExpandAll = _ref$defaultExpandAll === void 0 ? true : _ref$defaultExpandAll,
      _ref$checkedExpand = _ref.checkedExpand,
      checkedExpand = _ref$checkedExpand === void 0 ? false : _ref$checkedExpand,
      _ref$canEdit = _ref.canEdit,
      canEdit = _ref$canEdit === void 0 ? false : _ref$canEdit,
      _ref$showLine = _ref.showLine,
      showLine = _ref$showLine === void 0 ? false : _ref$showLine,
      onCheck = _ref.onCheck,
      onSelect = _ref.onSelect,
      onAdd = _ref.onAdd,
      onDelete = _ref.onDelete;
  // 当选中的节点
  var currentSelectedNode = {}; // 处理源数据,生成树形结构

  var dealedSourceData = function loop(data) {
    var temp = [];
    var lev = 0;
    data.forEach(function (val, index) {
      if (!val.parentId) {
        val.key = "0-".concat(index);
        data[index].key = "0-".concat(index);
        temp.push(val);
      }
    }); // 查找子元素

    function findChild(temp) {
      temp.forEach(function (col, i) {
        if (find(col.id).length > 0) {
          col.children = find(col.id);
          col.children.forEach(function (con, num) {
            con.key = col.key + '-' + num;
            data.forEach(function (val, index) {
              if (val.id === con.id) {
                val.key = col.key + '-' + num;
              }
            });
          });
          findChild(col.children);
        }
      });
      return temp;
    } // 看是否有子元素


    function find(id) {
      var temp = [];
      data.forEach(function (v) {
        if (v.parentId === id) {
          temp.push(v);
        }
      });
      return temp;
    }

    temp = findChild(temp);
    return {
      data: data,
      temp: temp
    };
  }(dataSource); // 获取默认选择值


  var getKeys = function getKeys(data) {
    var temp = [];
    data.forEach(function (val) {
      if (!val.children && val.id !== '0' && val.checked === 1) {
        temp.push(val.key);
      }
    });
    return temp;
  }; //处理原数据


  var loop = function loop(data) {
    return data.map(function (item) {
      var customLabel = {};

      if (item.children) {
        return _react["default"].createElement(TreeNode, {
          title: item.name,
          key: item.key,
          disabled: item.disabled,
          disableCheckbox: item.disableCheckbox
        }, loop(item.children));
      }

      return _react["default"].createElement(TreeNode, {
        title: item.name,
        key: item.key,
        disabled: item.disabled,
        disableCheckbox: item.disableCheckbox
      });
    });
  };

  var treeNodes = loop(dealedSourceData.temp); // 选择节点预处理

  var pretreatCheck = function pretreatCheck(checkedKeys, info) {
    /** @namespace info.halfCheckedKeys */
    var halfCheckedKeys = info.halfCheckedKeys || [];
    checkedKeys = checkedKeys.concat(halfCheckedKeys);
    var temp = [];
    dealedSourceData.data.forEach(function (val) {
      if (!_.isEmpty(checkedKeys) && checkedKeys.indexOf(val.key) !== -1) {
        temp.push(val);
      }
    });

    if (onCheck) {
      onCheck(temp);
    }
  }; // 点击节点预处理


  var pretreatSelect = function pretreatSelect(selectedKeys) {
    var selectedData = _.find(dealedSourceData.data, function (d) {
      return d.key === selectedKeys[0];
    });

    currentSelectedNode = selectedData;

    if (onSelect) {
      onSelect(selectedData);
    }
  }; // 点击添加预处理


  var pretreatAdd = function pretreatAdd() {
    if (!_.isEmpty(currentSelectedNode)) {
      if (onAdd) {
        onAdd(currentSelectedNode);
      }
    } else {
      // TODO 之后再给错误提示框
      console.log('您还没选中任何的节点呀');
    }
  }; // 点击删除预处理


  var pretreatDelete = function pretreatDelete() {
    if (!_.isEmpty(currentSelectedNode)) {
      if (onDelete) {
        onDelete(currentSelectedNode);
      }
    } else {
      // TODO 之后再给错误提示框
      console.log('您还没选中任何的节点呀');
    }
  };

  var ColProps = {
    xs: 24,
    sm: 12,
    style: {
      marginBottom: 16
    }
  };

  var TwoColProps = _objectSpread({}, ColProps, {
    xl: 96
  });

  return _react["default"].createElement("div", null, canEdit && _react["default"].createElement(_antd.Row, {
    gutter: 24
  }, _react["default"].createElement(_antd.Col, ColProps, _react["default"].createElement(_antd.Button, {
    type: "primary",
    size: "small",
    className: "margin-right",
    onClick: pretreatAdd
  }, "\u6DFB\u52A0"), _react["default"].createElement(_antd.Button, {
    type: "primary",
    size: "small",
    className: "margin-right",
    onClick: pretreatDelete
  }, "\u5220\u9664"))), dealedSourceData !== undefined && _react["default"].createElement(_antd.Tree, {
    multiple: multiple,
    checkable: checkable,
    defaultExpandAll: true,
    defaultCheckedKeys: getKeys(dealedSourceData.data),
    onCheck: pretreatCheck,
    onSelect: pretreatSelect,
    showLine: showLine
  }, treeNodes));
};

LwjTree.propTypes = {
  dataSource: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    name: _propTypes["default"].string.isRequired,
    id: _propTypes["default"].string.isRequired,
    disabled: _propTypes["default"].bool,
    disableCheckbox: _propTypes["default"].bool
  })).isRequired,
  multiple: _propTypes["default"].bool,
  checkable: _propTypes["default"].bool,
  defaultExpandAll: _propTypes["default"].bool,
  checkedExpand: _propTypes["default"].bool,
  onCheck: _propTypes["default"].func,
  onSelect: _propTypes["default"].func,
  canEdit: _propTypes["default"].bool,
  onAdd: _propTypes["default"].func,
  onDelete: _propTypes["default"].func
};
var _default = LwjTree;
exports["default"] = _default;