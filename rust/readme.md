###安装
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

cargo new hello-rust //创建新项目

hello-rust
|- Cargo.toml
|- src
  |- main.rs
Cargo.toml 为 Rust 的清单文件。其中包含了项目的元数据和依赖库。
src/main.rs 为编写应用代码的地方。

```
###依赖安装
```
我们在 Cargo.toml 文件中添加以下信息（从 crate 页面上获取）：
[dependencies]
ferris-says = "0.1"
cargo build
```

###换阿里镜像源
```
tee $HOME/.cargo/config <<-'EOF'
[source.crates-io]
replace-with = "rustcc"

[source.rustcc]
registry = "https://code.aliyun.com/rustcc/crates.io-index"
EOF
```
###中科大镜像源
```
tee $HOME/.cargo/config <<-'EOF'
[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"
replace-with = 'ustc'
[source.ustc]
registry = "git://mirrors.ustc.edu.cn/crates.io-index"
EOF
```

###常用命令
- cargo build 可以构建项目
- cargo run 可以运行项目
- cargo test 可以测试项目
- cargo doc 可以为项目构建文档
- cargo publish 可以将库发布到 crates.io。


