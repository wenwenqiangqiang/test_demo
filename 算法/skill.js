/**
 * 整数反转
 * @param num
 * @returns {string|number}
 */
function reverse(num) {
    const now = Math.abs(num).toString().split('').reverse().join('');
    if (num < 0) {
        return -now
    } else {
        return now
    }
}

console.log(reverse(-989876));

/**
 * 查找数组中的重复元素
 */
var arr = [1, 2, 3, 2, 1, 33, 4];

function duplicates(arr) {
    var resArr = [];
    arr.forEach(val => {
        if (arr.indexOf(val) != arr.lastIndexOf(val) && resArr.indexOf(val) == -1) {
            resArr.push(val);
        }
    })
    return resArr;
}

console.log(duplicates(arr));

/**
 * 数组返回所有元素的平方
 */

function squ(num) {
    return num.map(val => {
        return val * val;
    })
}

console.log(squ(arr));


/**
 *判断两个值是否完全相等
 */
console.log(Object.is(1, 2));
console.log(Object.is(1, 1));

/**
 * call\bind\apply
 */

var obj = {
    a: 1,
    fire: function () {
        console.log(this.a);
    }
}
var a = 2;
var newFire = obj.fire;
newFire();
newFire.call(obj);
newFire.bind(obj)();

/**
 *
 */
var fullName = 'language'
var obj = {
    fullName: 'javascript',
    prop: {
        getFullName: function () {
            return this.fullName
        }
    }
}
console.log(obj.prop.getFullName())
var test = obj.prop.getFullName
console.log(test())

/**
 *数组扁平化
 */

function flatp(data) {
    return data.reduce(function (prve, now) {
        if (Array.isArray(now)) {
            return prve.concat(flatp(now))
        } else {
            prve.push(now);
            return prve;
        }
    }, [])
}

function flatp2(data) {
    var res = [];
    data.map(value => {
        if (Array.isArray(value)) {
            res = res.concat(flatp2(value));
        } else {
            res.push(value);
        }
    })
    return res;
}

console.log(flatp([1, [1, 2, 3], 3, [, 23]]));
console.log(flatp2([1, [1, 2, 3], 3, [, 23]]));



