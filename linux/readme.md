#装ngnix
```
1. wget -c https://nginx.org/download/nginx-1.10.1.tar.gz

2. sudo apt-get install gcc zlib1g zlib1g-dev libpcre3 libpcre3-dev openssl libssl-dev

3. tar -xvf nginx-1.10.1.tar.gz

4. cd ./nginx-1.10.1

5. ./configure

6. make && make install
  可能问题，编译不通过
  找到当前目录下找到objs文件夹，并进入，打开文件Makefile,找到有一下内容的这行：
  CFLAGS =  -pipe  -O -W -Wall -Wpointer-arith -Wno-unused-parameter -Werror -g　　
  把这行内容中的 “-Werror”去掉

7. usr/local/nginx/sbin# ./nginx  启动
usr/local/nginx/sbin# ./nginx -s reload  重启
usr/local/nginx/sbin# ps aux|grep nginx 查看nginx进程


8. nginx 配置
   upstream qsq{
        server 127.0.0.1:7001;
    }

   location / {
            proxy_pass http://qsq;
   } 


9. 多站点配置

mkdir /etc/nginx/vhost

cd vhost

vim confA.conf

server {
    listen       80;              # 站点监听端口
    server_name www.webA.com ;    # 站点域名
    root  /www/webA;              # 站点存放目录
    index index.html index.htm index.php;   #站点默认导航
}
vim  /etc/nginx/nginx.conf
http {
    ...
    include /etc/nginx/vhost/*.conf; 加入的语句，
                          #表示包含我们刚才建立的配置文件
}



```


### apt-get常用命令
```
apt-cache search package 搜索包
apt-cache show package 获取包的相关信息，如说明、大小、版本等
sudo apt-get install package 安装包
sudo apt-get install package - - reinstall 重新安装包
sudo apt-get -f install 修复安装"-f = ——fix-missing"

sudo apt-get remove package 删除包
sudo apt-get remove package - - purge 删除包，包括删除配置文件等
sudo apt-get update 更新源
sudo apt-get upgrade 更新已安装的包
sudo apt-get dist-upgrade 升级系统
sudo apt-get dselect-upgrade 使用 dselect 升级
apt-cache depends package 了解使用依赖
apt-cache rdepends package 是查看该包被哪些包依赖
sudo apt-get build-dep package 安装相关的编译环境
apt-get source package 下载该包的源代码
sudo apt-get clean && sudo apt-get autoclean 清理无用的包
sudo apt-get check 检查是否有损坏的依赖
```

###
```
tar  -zcvf ./node_modules.tar ./node_modules
 tar  -zxvf  ./node_modules.tar
```


### 磁盘满了
```
1、当使用df -h命令查看磁盘使用情况时发现满了
2、这时候我们需要使用命令du -sh * 看哪个目录占用空间大

比如 cd /usr

```
