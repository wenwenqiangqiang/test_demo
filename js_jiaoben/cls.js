const yParser = require('yargs-parser');
const {existsSync, readdirSync} = require('fs');
const {join} = require('path');
const semver = require('semver');//来对特定的版本号做判断
const chalk = require('chalk');//控制台中输出不同的颜色的字
const inquirer = require('inquirer');//命令行交互式工具
const mkdirp = require('mkdirp');//递归创建目录及其子目录
/**
 * node版本低于8.0.0不继续执行
 */
if (!semver.satisfies(process.version, '>= 8.0.0')) {
    console.log(chalk.blue(process.version));
    console.error(chalk.red('✘ The generator will only work with Node v8.0.0 and up!'));
    process.exit(1);
}
/**
 * 打印版本号
 */
const args = yParser(process.argv.slice(2));
if (args.v || args.version) {
    console.log(require('../package').version);
    //如果有.local文件夹，打印
    if (existsSync(join(__dirname, '.local'))) {
        console.log(chalk.cyan('@local'));
    }
    //正常退出进程
    process.exit(0);
}

/**
 * 读取可选库文件
 */
const generators = readdirSync(`${__dirname}/lib/generators`)
    .filter(f => !f.startsWith('.'))
    .map(f => {
        return {
            name: `${f.padEnd(15)} - ${chalk.gray(require(`./lib/generators/${f}/meta.json`).description)}`,//扩充字符串到15个字符拼接描述信息
            value: f,
            short: f,
        };
    });


inquirer.prompt([
    {
        name: 'type',
        message: 'Select the boilerplate type',
        type: 'list',
        choices: generators,
    },
]).then(answers => {
    runGenerator(`./lib/generators/${answers.type}`);
}).catch(e => {
    console.error(chalk.red(`> Generate failed`));
    console.log(e);
    process.exit(1);
});

/**
 * 执行目标文件的generator
 * @param generatorPath
 */
function runGenerator(generatorPath) {
    let cwd = process.cwd();
    //是否有项目名，有递归生成子文件夹
    if (args._.length) {
        mkdirp.sync(args._[0]);
        cwd = join(cwd, args._[0]);
    }


    const Generator = require(generatorPath);
    const generator = new Generator(process.argv.slice(2), {
        name: '_',
        env: {
            cwd,
        },
        resolved: require.resolve(generatorPath),
    })
    generator.run(() => {
        if (args._[0]) {
            clipboardy.writeSync(`cd ${args._[0]}`);
            console.log('📋  Copied to clipboard, just use Ctrl+V');
        }
        console.log('✨  File Generate Done');
    });
}
