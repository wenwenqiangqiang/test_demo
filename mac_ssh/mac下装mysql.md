```
brew install mysql

brew services start mysql

mysql_secure_installation  重置密码
```

## linux 下
```
wget https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.24-linux-glibc2.12-x86_64.tar.gz

tar xzvf mysql-5.7.24-linux-glibc2.12-x86_64.tar.gz
mv mysql-5.7.24-linux-glibc2.12-x86_64 /usr/local/mysql

在/usr/local/mysql目录下创建data目录


groupadd mysql     //创建mysql组
useradd -g mysql mysql //创建mysql用户添加到mysql组

 mkdir /usr/local/mysql/data
chown -R mysql:mysql /usr/local/mysql
chmod -R 755 /usr/local/mysql
cd /usr/local/mysql/bin
./mysqld --initialize --user=mysql --datadir=/usr/local/mysql/data --basedir=/usr/local/mysql


编辑配置文件my.cnf，添加配置如下

vi /etc/my.cnf
[mysqld]
datadir=/usr/local/mysql/data
port = 3306
sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES
symbolic-links=0
max_connections=400
innodb_file_per_table=1
#表名大小写不明感，敏感为
lower_case_table_names=1

/usr/local/mysql/support-files/mysql.server start


https://www.jianshu.com/p/276d59cbc529

ln -s /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql 
ln -s /usr/local/mysql/bin/mysql /usr/bin/mysql
service mysql restart

```
