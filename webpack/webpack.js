const config = {
    entry: {
        // 主入口
        app: './src/app.js',
        // 插件入口
        vendors: './src/vendors.js',
        // 多入口方式
        pageOne: './src/pageOne/index.js',
        pageTwo: './src/pageTwo/index.js',
        pageThree: './src/pageThree/index.js'
    },
    // 输出文件及目录
    output: {
        filename: 'bundle.js',
        path: '/home/proj/public/assets'
    }
}

// 多入口 写入到硬盘：./dist/app.js, ./dist/search.js
const config1 = {
    entry: {
        app: './src/app.js',
        search: './src/search.js'
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist'
    }
}
// 以下是使用 CDN 和资源 hash 的复杂示例：
const config3 = {
    output: {
        path: "/home/proj/cdn/assets/[hash]",
        publicPath: "http://cdn.example.com/assets/[hash]/"
    }
}

/**
 * loader 支持链式传递。能够对资源使用流水线(pipeline)。一组链式的 loader 将按照相反的顺序执行。
 * loader 可以是同步的，也可以是异步的。
 * loader 运行在 Node.js 中，并且能够执行任何可能的操作。
 * loader 接收查询参数。用于对 loader 传递配置。
 * loader 也能够使用 options 对象进行配置。
 * 除了使用 package.json 常见的 main 属性，还可以将普通的 npm 模块导出为 loader，做法是在 package.json 里定义一个 loader 字段。
 * 插件(plugin)可以为 loader 带来更多特性。
 * loader 能够产生额外的任意文件。
 * @type {{module: {rules: {test: RegExp, use: *[]}[]}}}
 */
const config4 = {
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader'},
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true
                        }
                    }
                ]
            }
        ]
    }
}

/**
 * 插件目的在于解决 loader 无法实现的其他事。
 * 本质上是一个可以被complier随时调用的对象
 *
 */
const HtmlWebpackPlugin = require('html-webpack-plugin'); //通过 npm 安装
const webpack = require('webpack'); //访问内置的插件
const path = require('path');

const config = {
    entry: './path/to/my/entry/file.js',
    output: {
        filename: 'my-first-webpack.bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: 'babel-loader'
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin(),
        new HtmlWebpackPlugin({template: './src/index.html'})
    ]
};

module.exports = config;


/**
 * 常用配置清单
 */
const path = require('path');

module.exports = {
    // 模式
    mode: "production", // "production" | "development" | "none"  // Chosen mode tells webpack to use its built-in optimizations accordingly.

    // 入口
    entry: "./app/entry", // string | object | array  // 这里应用程序开始执行
    // webpack 开始打包
    // 出口
    output: {
        // webpack 如何输出结果的相关选项
        path: path.resolve(__dirname, "dist"), // string
        // 所有输出文件的目标路径
        // 必须是绝对路径（使用 Node.js 的 path 模块）
        filename: "bundle.js", // string    // 「入口分块(entry chunk)」的文件名模板（出口分块？）
        publicPath: "/assets/", // string    // 输出解析文件的目录，url 相对于 HTML 页面
        library: "MyLibrary", // string,
        // 导出库(exported library)的名称
        libraryTarget: "umd", // 通用模块定义    // 导出库(exported library)的类型
        /* 高级输出配置（点击显示） */
    },
    module: {
        // 关于模块配置
        rules: [
            // 模块规则（配置 loader、解析器等选项）
            {
                test: /\.jsx?$/,
                include: [
                    path.resolve(__dirname, "app")
                ],
                exclude: [
                    path.resolve(__dirname, "app/demo-files")
                ],
                // 这里是匹配条件，每个选项都接收一个正则表达式或字符串
                // test 和 include 具有相同的作用，都是必须匹配选项
                // exclude 是必不匹配选项（优先于 test 和 include）
                // 最佳实践：
                // - 只在 test 和 文件名匹配 中使用正则表达式
                // - 在 include 和 exclude 中使用绝对路径数组
                // - 尽量避免 exclude，更倾向于使用 include

                issuer: {test, include, exclude},
                // issuer 条件（导入源）

                enforce: "pre",
                enforce: "post",
                // 标识应用这些规则，即使规则覆盖（高级选项）

                loader: "babel-loader",
                // 应该应用的 loader，它相对上下文解析
                // 为了更清晰，`-loader` 后缀在 webpack 2 中不再是可选的
                // 查看 webpack 1 升级指南。

                options: {
                    presets: ["es2015"]
                },
                // loader 的可选项
            },

            {
                test: /\.html$/,
                test: "\.html$",
                use: [
                    // 应用多个 loader 和选项
                    "htmllint-loader",
                    {
                        loader: "html-loader",
                        options: {
                            /* ... */
                        }
                    }
                ]
            },

            {oneOf: [/* rules */]},
            // 只使用这些嵌套规则之一

            {rules: [/* rules */]},
            // 使用所有这些嵌套规则（合并可用条件）

            {resource: {and: [/* 条件 */]}},
            // 仅当所有条件都匹配时才匹配

            {resource: {or: [/* 条件 */]}},
            {resource: [/* 条件 */]},
            // 任意条件匹配时匹配（默认为数组）

            {resource: {not: /* 条件 */}}
            // 条件不匹配时匹配
        ],

        /* 高级模块配置（点击展示） */
    },

    resolve: {
        // 解析模块请求的选项
        // （不适用于对 loader 解析）

        modules: [
            "node_modules",
            path.resolve(__dirname, "app")
        ],
        // 用于查找模块的目录

        extensions: [".js", ".json", ".jsx", ".css"],
        // 使用的扩展名

        alias: {
            // 模块别名列表

            "module": "new-module",
            // 起别名："module" -> "new-module" 和 "module/path/file" -> "new-module/path/file"

            "only-module$": "new-module",
            // 起别名 "only-module" -> "new-module"，但不匹配 "only-module/path/file" -> "new-module/path/file"

            "module": path.resolve(__dirname, "app/third/module.js"),
            // 起别名 "module" -> "./app/third/module.js" 和 "module/file" 会导致错误
            // 模块别名相对于当前上下文导入
        },
        /* 可供选择的别名语法（点击展示） */
        /* 高级解析选项（点击展示） */
    },

    performance: {
        hints: "warning", // 枚举    maxAssetSize: 200000, // 整数类型（以字节为单位）
        maxEntrypointSize: 400000, // 整数类型（以字节为单位）
        assetFilter: function (assetFilename) {
            // 提供资源文件名的断言函数
            return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
        }
    },

    devtool: "source-map", // enum  // 通过在浏览器调试工具(browser devtools)中添加元信息(meta info)增强调试
    // 牺牲了构建速度的 `source-map' 是最详细的。

    context: __dirname, // string（绝对路径！）
    // webpack 的主目录
    // entry 和 module.rules.loader 选项
    // 相对于此目录解析

    target: "web", // 枚举  // 包(bundle)应该运行的环境
    // 更改 块加载行为(chunk loading behavior) 和 可用模块(available module)

    externals: ["react", /^@angular\//],  // 不要遵循/打包这些模块，而是在运行时从环境中请求他们

    stats: "errors-only",  // 精确控制要显示的 bundle 信息

    devServer: {
        proxy: { // proxy URLs to backend development server
            '/api': 'http://localhost:3000'
        },
        contentBase: path.join(__dirname, 'public'), // boolean | string | array, static file location
        compress: true, // enable gzip compression
        historyApiFallback: true, // true for index.html upon 404, object for multiple paths
        hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin
        https: false, // true for self-signed, object for cert authority
        noInfo: true, // only errors & warns on hot reload
        // ...
    },

    plugins: [
        // ...
    ],
    // 附加插件列表


    /* 高级配置（点击展示） */

    resolveLoader: {/* 等同于 resolve */}
    // 独立解析选项的 loader

    parallelism: 1, // number
    // 限制并行处理模块的数量

    profile: true, // boolean
    // 捕获时机信息

    bail: true, //boolean
    // 在第一个错误出错时抛出，而不是无视错误。

    cache: false, // boolean
    // 禁用/启用缓存

    watch: true, // boolean
    // 启用观察

    watchOptions: {
        aggregateTimeout: 1000, // in ms
        // 将多个更改聚合到单个重构建(rebuild)

        poll: true,
        poll: 500, // 间隔单位 ms
        // 启用轮询观察模式
        // 必须用在不通知更改的文件系统中
        // 即 nfs shares（译者注：Network FileSystem，最大的功能就是可以透過網路，讓不同的機器、不同的作業系統、可以彼此分享個別的檔案 ( share file )）
    },

    node: {
        // Polyfills and mocks to run Node.js-
        // environment code in non-Node environments.

        console: false, // boolean | "mock"
        global: true, // boolean | "mock"
        process: true, // boolean
        __filename: "mock", // boolean | "mock"
        __dirname: "mock", // boolean | "mock"
        Buffer: true, // boolean | "mock"
        setImmediate: true // boolean | "mock" | "empty"
    },

    recordsPath: path.resolve(__dirname, "build/records.json"),
    recordsInputPath: path.resolve(__dirname, "build/records.json"),
    recordsOutputPath: path.resolve(__dirname, "build/records.json"),
    // TODO

}
