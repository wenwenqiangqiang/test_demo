const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyWebpackPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const utils = require('./utils');
const SriPlugin = require('webpack-subresource-integrity');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');


module.exports = {
   cache: true,
   entry: {
      index: utils.resolve('index.js'),
      vender: ['react']
   },
   output: {
      path: utils.resolvedist('dist'),
      // filename: '[name].js',
      filename: '[name].[chunkhash].js',
      chunkFilename: '[name].[chunkhash].async.js',
      publicPath: ""
   },
   module: {
      rules: [
         {
            test: /\.css$/,
            use: [{
               loader: MiniCssExtractPlugin.loader,
               options: {
                  // you can specify a publicPath here
                  // by default it use publicPath in webpackOptions.output
                  publicPath: '/'
               }
            }, 'css-loader']
         },
         {
            test: /\.less$/,
            use: [{loader: MiniCssExtractPlugin.loader}, 'css-loader', 'less-loader'],
            exclude: /node_modules/,
            include: path.resolve(__dirname, '../src')
         },
         {
            test: /\.scss$/,
            use: [{loader: MiniCssExtractPlugin.loader}, 'css-loader', 'sass-loader'],
            exclude: /node_modules/,
            include: path.resolve(__dirname, '../src')
         }
         ,
         {
            test: /\.(gif|jpg|png|bmp|eot|woff|woff2|ttf|svg)/,
            use:
              [
                 {
                    loader: 'url-loader',
                    options: {
                       limit: 8192
                    }
                 }
              ]
         }
         ,
         {
            test: /\.js$/,
            use:
              [
                 {
                    loader: 'babel-loader'
                 }
              ],
            include:
              path.resolve(__dirname, 'src'),
            exclude: /node_modules/
         }
      ]
   },
   plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
         template: utils.resolve('index.html')
      }),
      new MiniCssExtractPlugin({
         filename: 'css/[name].css'
      })
   ],
   optimization:
     {
        minimizer: [
           new UglifyWebpackPlugin({
              parallel: 4
           }),
           new OptimizeCssAssetsWebpackPlugin(),
           new SriPlugin({
              hashFuncNames: ['sha256', 'sha384']
           }),
           new ScriptExtHtmlWebpackPlugin({
              coustom: {
                 test: /\*_[A-Za-z0-9]{8}.js/,
                 attribute: 'onerror',
                 value: 'loadScriptError.call(this,event)'
              }
           }),
           new ScriptExtHtmlWebpackPlugin({
              coustom: {
                 test: /\*_[A-Za-z0-9]{8}.js/,
                 attribute: 'onsuccess',
                 value: 'loadScriptSuccess.call(this,event)'
              }
           })
        ]
     }
   ,
   resolve: {
      extensions: ['.json', '.js', '.jsx', '.css'],
      alias:
        {
           'assets':
             utils.resolve('assets'),
           'pages':
             utils.resolve('pages'),
           'static':
             utils.resolve('static'),
           'components':
             utils.resolve('components')
        }
   }
}
