'use strict'

const webpack = require('webpack')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.config.base')

const HOST = 'localhost'
const PORT = 8080

module.exports = merge(baseConfig, {
   mode: 'development',
   devServer: {
      clientLogLevel: 'warning',
      hot: true,
      contentBase: 'dist',
      compress: true,
      host: HOST,
      port: PORT,
      open: true,
      overlay: {warnings: false, errors: true},
      publicPath: '/',
      quiet: true
   },
   watch: true,
   watchOptions: { //不监听目录
      ignored: [/node_modules/, '/assets/']
   },
   devtool: '#source-map',

   module: {},

   plugins: [
      new webpack.HotModuleReplacementPlugin()
   ]
})
