const {SyncHook, AsyncParallelHook} = require("tapable")

class Car {
   constructor(options) {
      this.hooks = {
         accelerate: new SyncHook(["newSpeed"]),
         break: new SyncHook(),
         calculateRoutes: new AsyncParallelHook(["source", "target", "routesList"])
      };
      let plugins = options.plugins;
      if (plugins && Array.isArray(plugins)) {
         plugins.forEach(plugin => plugin.apply(this));
      }
   }

   run() {
      console.time('cost');
      this.hooks.break.call();
      this.hooks.accelerate.call(999);
      this.hooks.calculateRoutes.callAsync('i', 'love', 'you', err => {
         console.timeEnd('cost');
         if (err) {
            console.log(err);
         }
      })
   }
}

module.exports = Car

