const {SyncHook, AsyncParallelHook} = require("tapable")

class Car {
   constructor() {
      this.hooks = {
         accelerate: new SyncHook(["newSpeed"]),
         break: new SyncHook(),
         calculateRoutes: new AsyncParallelHook(["source", "target", "routesList"])
      };
   }
}

const myCar = new Car();
myCar.hooks.break.tap("WarningLampPlugin", () => console.log("WarningLampPlugin"));
myCar.hooks.accelerate.tap("LoggerPlugin", newSpeed => console.log(`newSpeed is ${newSpeed }`));
myCar.hooks.calculateRoutes.tapPromise("calculateRoutes tapPromise", (source, target, routesList, callback) => {
   return new Promise((resolve, reject) => {
      setTimeout(() => {
         console.log(`tabpromise to ${source}${target}${routesList}`);
         resolve();
      }, 2000)
   })
})

// 执行异步钩子
myCar.hooks.break.promise('i', 'love', 'you').then(() => {
   console.timeEnd('cost');
}, err => {
   console.log(err);
   console.timeEnd('cost');
})

//执行同步钩子
myCar.hooks.break.call();
myCar.hooks.accelerate.call(98);
console.time('cost');

