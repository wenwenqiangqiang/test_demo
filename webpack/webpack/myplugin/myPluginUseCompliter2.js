const Compliter = require("./complier2")

/**
 * webpack插件的定义
 * 1、具有apply方法的对象；
 * 2、complier对象会在实例化时注册apply中的事件
 * 3、每次编译都会生成一个Compilation对象；
 */
class MyPlugin {
   constructor() {

   }

   apply(complier) {
      complier.hooks.break.tap("WarningLampPlugin", () => console.log("WarningLampPlugin"));
      complier.hooks.accelerate.tap("LoggerPlugin", speed => console.log(`WarningLampPlugin speed is ${speed}`));
      complier.hooks.calculateRoutes.tapAsync("calculateRoutes tapAsync", (source, target, routesList, callback) => {
         setTimeout(() => {
            console.log(`calculateRoutes tapAsync ${source} ${target} ${routesList}`)
            callback();
         }, 2000)
      });
   }
}

const myPlugin = new MyPlugin();
const options = {
   plugins: [myPlugin]
}

let complier = new Compliter(options);
complier.run();
