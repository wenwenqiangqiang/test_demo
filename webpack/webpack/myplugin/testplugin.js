const {SyncHook} = require('tapable');

class Car {
   constructor() {
      this.hooks = {
         carNubmber: new SyncHook(['arg1', 'arg2'])
      }
   }
}

var myCar = new Car();
//注册事件
myCar.hooks.carNubmber.tap('getCarNumber', (arg1, arg2) => {
   console.log(arg1);
   console.log(arg2);
});
myCar.hooks.carNubmber.tap('getMoreNumber', (arg1, arg2) => {
   console.log(arg1 + 1);
   console.log(arg2 + 1);
})
// 粗发事件
myCar.hooks.carNubmber.call(100, 200);
