#插件的书写
1. new Hook 新建钩子
- tapable暴露出很多hook，new 一个类方法获得我们需要的钩子，在任务执行到相应的阶段会触发我们定义的new Hook。
- class 接受数组参数options，非必传。类方法会根据传参，接受同样数量的参数。
2. 使用 tap/tapAsync/tapPromise 绑定钩子(注册事件)
3. call/callAsync 执行绑定事件（触发事件）
```javascript
const hook1 = new SyncHook(["arg1", "arg2", "arg3"]);
hook1.tap('hook1', (arg1, arg2, arg3) => console.log(arg1, arg2, arg3));
hook1.call(1,2,3);
```
