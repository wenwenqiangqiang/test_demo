const HOST = 'localhost';
const PORT = 8080;
const path = require('path');
const HelloPlugin = require('./hello-plugin.js');
const FileListPlugin = require('./plugins/FileListPlugin.js');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, './dist'),
        filename: '[name]-build.js'
    },
    mode: 'development',
    devServer: {
        clientLogLevel: 'warning',
        hot: true,
        contentBase: 'dist',
        compress: true,
        host: HOST,
        port: PORT,
        open: true,
        overlay: {warnings: false, errors: true},
        publicPath: '/',
        quiet: true
    },
    watch: true,
    watchOptions: { //不监听目录
        ignored: [/node_modules/, '/assets/']
    },
    plugins: [
        new HelloPlugin({options: true}),
        new FileListPlugin()
    ]
}
