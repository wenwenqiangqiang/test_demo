class HelloPlugin {
    // 在构造函数中获取用户给该插件传入的配置
    constructor(options) {
        console.log(options)
    }

    // Webpack 会调用 HelloPlugin 实例的 apply 方法给插件实例传入 compiler 对象
    apply(compiler) {
        compiler.hooks.entryOption.tap('HelloPlugin', (compilation) => {
            // 在 entry 配置项处理过之后，执行插件。
            console.log('entry 配置项处理过之后。')
        });
        // 在emit阶段插入钩子函数，用于特定时机处理额外的逻辑；
        compiler.hooks.emit.tap('HelloPlugin', (compilation) => {
            // 在功能流程完成后可以调用 webpack 提供的回调函数；
            console.log('以同步方式触及 compile 钩子。')
        });

        // 如果事件是异步的，会带两个参数，第二个参数为回调函数，在插件处理完任务时需要调用回调函数通知webpack，才会进入下一个处理流程。
        compiler.hooks.emit.tapAsync('HelloPlugin', (compilation, callback) => {
            console.log(compilation.assets['main-build.js'].source());
            console.log('以异步方式触及 emit 钩子。')
            // 支持处理逻辑
            // 处理完毕后执行 callback 以通知 Webpack
            // 如果不执行 callback，运行流程将会一直卡在这不往下执行
            let source = compilation.assets['main-build.js'].source();
            source += `
            <script>
            console.log('加点料')
            </script>
            `;
            compilation.assets['main-build.js'] = {
                // 返回文件内容
                source: () => {
                    // fileContent 既可以是代表文本文件的字符串，也可以是代表二进制文件的 Buffer
                    return source;
                },
                // 返回文件大小
                size: () => {
                    return source.length;
                }
            };


            compilation.chunks.forEach(function (chunk) {
                // chunk 代表一个代码块
                // 代码块由多个模块组成，通过 chunk.forEachModule 能读取组成代码块的每个模块

                // chunk.forEachModule(function (module) {
                //     // module 代表一个模块
                //     // module.fileDependencies 存放当前模块的所有依赖的文件路径，是一个数组
                //     module.fileDependencies.forEach(function (filepath) {
                //     });
                // });

                // Webpack 会根据 Chunk 去生成输出的文件资源，每个 Chunk 都对应一个及其以上的输出文件
                // 例如在 Chunk 中包含了 CSS 模块并且使用了 ExtractTextPlugin 时，
                // 该 Chunk 就会生成 .js 和 .css 两个文件
                chunk.files.forEach(function (filename) {
                    // compilation.assets 存放当前所有即将输出的资源
                    // 调用一个输出资源的 source() 方法能获取到输出资源的内容
                    let source = compilation.assets[filename].source();
                    console.log(source);

                });
            });

            callback()
        })

        compiler.hooks.run.tapPromise('HelloPlugin', compiler => {
            return new Promise(resolve => setTimeout(resolve, 1000)).then(() => {
                console.log('以具有延迟的异步方式触及 run 钩子')
            })
        })
    }
}

module.exports = HelloPlugin;
