####一个简单的插件的构成
plugins是可以用自身原型方法apply来实例化的对象。apply只在安装插件被Webpack compiler执行一次。apply方法传入一个webpck compiler的引用，来访问编译器回调。
```javascript
class HelloPlugin{
  // 在构造函数中获取用户给该插件传入的配置
  constructor(options){
  }
  // Webpack 会调用 HelloPlugin 实例的 apply 方法给插件实例传入 compiler 对象
  apply(compiler) {
    // 在emit阶段插入钩子函数，用于特定时机处理额外的逻辑；
    compiler.hooks.emit.tap('HelloPlugin', (compilation) => {
      // 在功能流程完成后可以调用 webpack 提供的回调函数；
    });
    // 如果事件是异步的，会带两个参数，第二个参数为回调函数，在插件处理完任务时需要调用回调函数通知webpack，才会进入下一个处理流程。
    compiler.plugin('emit',function(compilation, callback) {
      // 支持处理逻辑
      // 处理完毕后执行 callback 以通知 Webpack 
      // 如果不执行 callback，运行流程将会一直卡在这不往下执行 
      callback();
    });
  }
}

module.exports = HelloPlugin;
```

安装插件时, 只需要将它的一个实例放到Webpack config plugins 数组里面:
```javascript
const HelloPlugin = require('./hello-plugin.js');
var webpackConfig = {
  plugins: [
    new HelloPlugin({options: true})
  ]
};
```
工作原理分析
1. 实例化插件，并形成插件队列；
2. 实例化compiler对象，并将compiler对象依次传入插件实例的apply方法；
3. 插件实例在获取到 compiler 对象后，就可以通过compiler.plugin(事件名称, 回调函数) 监听到 Webpack 广播出来的事件。并且可以通过 compiler 对象去操作 Webpack


### webapck 构建流程
1. 校验配置文件 ：读取命令行传入或者webpack.config.js文件，合并生成初始化本次构建的配置参数
2. 初始化compiler对象，实例化插件，并将其挂在webpack事件流上的自定义hooks上
3. 读取entries，递归遍历所有入口文件
4. run/watch：如果运行在watch模式则执行watch方法，否则执行run方法
5. compilation：创建Compilation对象回调compilation相关钩子，依次进入每一个入口文件(entry)，使用loader对文件进行编译。通过compilation我可以可以读取到module的resource（资源路径）、loaders（使用的loader）等信息。再将编译好的文件内容使用acorn解析生成AST静态语法树。然后递归、重复的执行这个过程， 所有模块和和依赖分析完成后，执行 compilation 的 seal 方法对每个 chunk 进行整理、优化、封装__webpack_require__来模拟模块化操作.
6. emit：所有文件的编译及转化都已经完成，包含了最终输出的资源，我们可以在传入事件回调的compilation.assets上拿到所需数据，其中包括即将输出的资源、代码块Chunk等等信息。
```javascript
// 修改或添加资源
compilation.assets['new-file.js'] = {
  source() {
    return 'var a=1';
  },
  size() {
    return this.source().length;
  }
};
```
7. afterEmit：文件已经写入磁盘完成
8. done：完成编译

### 理解事件流机制 Tapable
1. Tapable也是一个小型的 library，是Webpack的一个核心工具。类似于node中的events库，核心原理就是一个订阅发布模式。作用是提供类似的插件接口。
2. webpack中最核心的负责编译的Compiler和负责创建bundles的Compilation都是Tapable的实例，可以直接在 Compiler 和 Compilation 对象上广播和监听事件，方法如下：
```javascript
/**
* 广播事件
* event-name 为事件名称，注意不要和现有的事件重名
*/
compiler.apply('event-name',params);
compilation.apply('event-name',params);
/**
* 监听事件
*/
compiler.plugin('event-name',function(params){});
compilation.plugin('event-name', function(params){});
```
3. Tapable类暴露了tap、tapAsync和tapPromise方法，可以根据钩子的同步/异步方式来选择一个函数注入逻辑。
```javascript
compiler.hooks.compile.tap('MyPlugin', params => {
  console.log('以同步方式触及 compile 钩子。')
})
// tapAsync 异步钩子，通过callback回调告诉Webpack异步执行完毕tapPromise 异步钩子，返回一个Promise告诉Webpack异步执行完毕
compiler.hooks.run.tapAsync('MyPlugin', (compiler, callback) => {
  console.log('以异步方式触及 run 钩子。')
  callback()
})

compiler.hooks.run.tapPromise('MyPlugin', compiler => {
  return new Promise(resolve => setTimeout(resolve, 1000)).then(() => {
    console.log('以具有延迟的异步方式触及 run 钩子')
  })
})
```
### webpack打包过程或者插件代码里该如何调试？
1. 在当前webpack项目工程文件夹下面，执行命令行：
```javascript
node --inspect-brk ./node_modules/webpack/bin/webpack.js --inline --progress
```
其中参数--inspect-brk就是以调试模式启动node：
终端会输出：
```javascript
Debugger listening on ws://127.0.0.1:9229/1018c03f-7473-4d60-b62c-949a6404c81d
For help, see: https://nodejs.org/en/docs/inspector
```
2. 谷歌浏览器输入 chrome://inspect/#devices
3. 然后点一下Chrome调试器里的“继续执行”，断点就提留在我们设置在插件里的debugger断点了。






