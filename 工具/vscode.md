1. 开发规范文档为尺度
2. vscode编码格式为利刃(文章结尾分享本人使用的vscode配置)
3. 插件（重要） Beautify css/sass/scss/less，Chinese (Simplified) Language Pack for Visual Studio Code，ESLint，Git History，Git Project Manager，GitLens — Git supercharged，Path Intellisense，Vetur，npm，Live Server，koroFileHeader，Terminal
4. 插件（不那么重要）Bracket Pair Colorizer，Code Spell Checker，Comment Translate
5. 其他语言
```
{

  // vscode默认启用了根据文件类型自动设置tabsize的选项

  "editor.detectIndentation": false,

  // 重新设定tabsize

  "editor.tabSize": 2,

  // #每次保存的时候自动格式化

  "editor.formatOnSave": true,

  // #每次保存的时候将代码按eslint格式进行修复

  "eslint.autoFixOnSave": true,

  // 添加 vue 支持

  "eslint.validate": [

    "javascript",

    "javascriptreact",

    {

      "language": "vue",

      "autoFix": true

    }

  ],

  // "javascript.suggest.autoImports": true,

  //  #让prettier使用eslint的代码格式进行校验

  "prettier.eslintIntegration": true,

  //  #去掉代码结尾的分号

  "prettier.semi": false,

  //  #使用带引号替代双引号

  "prettier.singleQuote": true,

  //  #让函数(名)和后面的括号之间加个空格

  "javascript.format.insertSpaceBeforeFunctionParenthesis": true,

  // #这个按用户自身习惯选择

  "vetur.format.defaultFormatter.html": "js-beautify-html",

  // #让vue中的js按编辑器自带的ts格式进行格式化

  "vetur.format.defaultFormatter.js": "vscode-typescript",

  "vetur.format.defaultFormatterOptions": {

    "js-beautify-html": {

      "wrap_attributes": "force-aligned"

      // #vue组件中html代码格式化样式

    }

  },

  "explorer.confirmDelete": false,

  "fileheader.customMade": {

    "Description": "描述",

    "Author": "吴文周",

    "Github": "https://github.com/fodelf",

    "Date": "Do not edit", // 文件创建时间(不变)

    "LastEditors": "吴文周", // 文件最后编辑者

    "LastEditTime": "Do not edit" // 文件最后编辑时间

  },

  // 函数注释

  "fileheader.cursorMode": {

    "name": "默认名称",

    "description": "默认描述",

    "param {type}": "默认参数",

    "return {type}": "默认类型",

  },

  // 将该选项设置为true即可开启

  "fileheader.configObj": {

    "autoAdd": true,

  },

  "files.associations": {

    "*.cjson": "jsonc",

    "*.wxss": "css",

    "*.wxs": "javascript",

    "*.vue": "vue"

  },

  "editor.suggestSelection": "first",

  "vsintellicode.modify.editor.suggestSelection": "automaticallyOverrodeDefaultValue",

  "[javascript]": {

    "editor.defaultFormatter": "esbenp.prettier-vscode"

  },

  // "npm-intellisense.scanDevDependencies": true,

  // "npm-intellisense.importES6": true,

  // "npm-intellisense.importQuotes": "'",

  // "npm-intellisense.importLinebreak": ";\r\n",

  // "npm-intellisense.importDeclarationType": "const",

}
```
