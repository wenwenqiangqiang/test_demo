module.exports = function (babel) {
    const {types: babelTypes} = babel; //babelTypes类似lodash那样的工具集，主要用来操作AST节点，比如创建、校验、转变等
    //state：代表了插件的状态，你可以通过state来访问插件的配置项。
    //Babel采取递归的方式访问AST的每个节点，之所以叫做visitor，只是因为有个类似的设计模式叫做访问者模式
    //Identifier、ASTNodeTypeHere：AST的每个节点，都有对应的节点类型，比如标识符（Identifier）、函数声明（FunctionDeclaration）等，
    // 可以在visitor上声明同名的属性，当Babel遍历到相应类型的节点，属性对应的方法就会被调用，传入的参数就是path、state。


    return {
        name: "test-plugin",
        visitor: {
            Identifier(path, state) {
                // if (path.node.name === 'bad') {
                //     console.log(path, state)
                //     path.node.name = 'good';
                // }

                /**
                 * path.node.name 入参
                 * state.opts完整传入参数
                 */
                let name = path.node.name;
                if (state.opts[name]) {
                    path.node.name = state.opts[name];
                }
            }
        }
    };
};
