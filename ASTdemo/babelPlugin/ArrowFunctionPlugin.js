const t = require('@babel/types')

module.exports = function (api, options) {

    console.log(api, options);
    const {spec} = options;
    return {
        name: "test-plugin-arrow-function",
        visitor: {
            ArrowFunctionExpression(path) {
                if (!path.isArrowFunctionExpression()) return;
                path.arrowFunctionToExpression({
                    allowInsertArrow: false,
                    specCompliant: !!spec,
                });
            },
        }
    };
};
