import React from 'react'
import PropTypes from 'prop-types'
import {Layout} from 'antd'
import {config} from 'utils'
import styles from './Layout.less'
import Menus from './Menu'


const Sider = ({
  siderFold,
  darkTheme,
  location,
  changeTheme,
  navOpenKeys,
  changeOpenKeys,
  menu,
  logo
}) => {
  const menusProps = {
    menu,
    siderFold,
    darkTheme,
    location,
    navOpenKeys,
    changeOpenKeys
  }

  return (
    <Layout.Sider
    theme="light"
    trigger={null}
    collapsible
    collapsed={siderFold}
  >
      <div className={styles.logo}>
        <img alt={'logo'} src={logo}/>
      </div>
      <Menus {...menusProps}/>
      <div className={styles.poweredBy}>{config.footerText}</div>
    </Layout.Sider>
  )
}

Sider.propTypes = {
  menu: PropTypes.array,
  siderFold: PropTypes.bool,
  darkTheme: PropTypes.bool,
  location: PropTypes.object,
  changeTheme: PropTypes.func,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func
}

export default Sider
