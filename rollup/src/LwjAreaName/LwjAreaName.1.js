import React, { useEffect, useState } from 'react';
import { Input, Select, Form } from 'antd';
import PropTypes from 'prop-types';
const { Option } = Select;
const FormItem = Form.Item;

const LwjAreaName = ({
  value,
  form: { getFieldDecorator, validateFieldsAndScroll, setFieldsValue, getFieldsValue },
}) => {
  const selectAfterPrvince = (
    <Select defaultValue="Prvince" style={{ width: 50 }}>
      <Option value="Prvince">省</Option>
    </Select>
  );
  const selectAfterCity = (
    <Select defaultValue="City" style={{ width: 50 }}>
      <Option value="City">市</Option>
    </Select>
  );
  const selectAfterArea = (
    <Select defaultValue="Area" style={{ width: 50 }}>
      <Option value="Area">区</Option>
    </Select>
  );

  const preChange = () => {
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        onChange(values);
      }
    });
  };
  const [defaultValue, CHANGDEFAULTVALUE] = useState({
    provinceName: '',
    cityName: '',
    districtName: '',
  });

  // useEffect(() => {
  //   CHANGDEFAULTVALUE(value);
  // }, value);
  // console.log(value, '%%%%%%%%%%%%%%%%%%%%');

  return (
    <Form>
      <div style={{ display: 'flex' }}>
        <FormItem style={{ marginBottom: 0 }}>
          {getFieldDecorator('provinceName', {
            rules: [
              {
                required: true,
                message: '请输入省',
                whitespace: true,
              },
            ],
            initialValue:
              defaultValue && defaultValue.provinceName ? defaultValue.provinceName : '',
          })(<Input onChange={preChange} addonAfter={selectAfterPrvince} />)}
        </FormItem>
        <FormItem style={{ marginBottom: 0 }}>
          {getFieldDecorator('cityName', {
            rules: [
              {
                required: true,
                message: '请输入市',
                whitespace: true,
              },
            ],
            initialValue: defaultValue && defaultValue.cityName ? defaultValue.cityName : '',
          })(<Input onChange={preChange} addonAfter={selectAfterCity} />)}
        </FormItem>
        <FormItem style={{ marginBottom: 0 }}>
          {getFieldDecorator('districtName', {
            rules: [
              {
                required: true,
                message: '请输入区',
                whitespace: true,
              },
            ],
            initialValue:
              defaultValue && defaultValue.districtName ? defaultValue.districtName : '',
          })(<Input onChange={preChange} addonAfter={selectAfterArea} />)}
        </FormItem>
      </div>
    </Form>
  );
};

LwjAreaName.propTypes = {
  onChange: PropTypes.func,
};
export default Form.create()(LwjAreaName);
