import { Input, Select, Form } from 'antd';
const { Option } = Select;
const FormItem = Form.Item;

const selectAfterPrvince = (
  <Select defaultValue="Prvince" style={{ width: 30 }} size="small" disabled showArrow={false}>
    <Option value="Prvince" style={{ margin: 0 }}>
      省
    </Option>
  </Select>
);
const selectAfterCity = (
  <Select defaultValue="City" style={{ width: 30 }} size="small" disabled showArrow={false}>
    <Option value="City" style={{ margin: 0 }}>
      市
    </Option>
  </Select>
);
const selectAfterArea = (
  <Select defaultValue="Area" style={{ width: 30 }} size="small" disabled showArrow={false}>
    <Option value="Area" style={{ margin: 0 }}>
      区
    </Option>
  </Select>
);

export const getAddressData = (
  getFieldDecorator,
  provinceName,
  cityName,
  districtName,
  showprovinceName,
  showcityName,
  showdistrictName,
  handleChange,
  isrequired = true
) => {
  return (
    <div style={{ display: 'flex' }}>
      <FormItem style={{ marginBottom: 0 }}>
        {getFieldDecorator(showprovinceName, {
          rules: [
            {
              required: isrequired,
              message: '请输入省',
              whitespace: true,
            },
          ],
          initialValue: provinceName,
        })(<Input onChange={handleChange} placeholder="请输入省" />)}
      </FormItem>
      <FormItem style={{ marginBottom: 0 }}>
        {getFieldDecorator(showcityName, {
          rules: [
            {
              required: isrequired,
              message: '请输入市',
              whitespace: true,
            },
          ],
          initialValue: cityName,
        })(<Input onChange={handleChange} placeholder="请输入市" />)}
      </FormItem>
      <FormItem style={{ marginBottom: 0 }}>
        {getFieldDecorator(showdistrictName, {
          rules: [
            {
              required: isrequired,
              message: '请输入区',
              whitespace: true,
            },
          ],
          initialValue: districtName,
        })(<Input onChange={handleChange} placeholder="请输入区" />)}
      </FormItem>
    </div>
  );
};
