import React from 'react'
import { Button, Tooltip } from 'antd'

const LwjButton = ({
    tooltip,
    ...ButtonProps
}) => {
    return (
        <Tooltip title={tooltip}>
            <Button {...ButtonProps} />
        </Tooltip>
    )
}
export default LwjButton
