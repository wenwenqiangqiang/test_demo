import {
  Tag,
  Modal,
  Table,
  Input,
  InputNumber,
  Divider,
  Icon,
  Button
} from 'antd';
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

const LwjMetaialAdd = ({
  data = [],
  removeOne,
  addOne
}) => {

  const columns = [
    {
      title: '物料编号',
      dataIndex: 'name',
      key: 'name',
      render: text => (<Input placeholder="请输入物料编号"/>)
    }, {
      title: '物料数量',
      dataIndex: 'age',
      key: 'age',
      render: text => (<InputNumber min={1}/>)
    }, {
      title: '物料名称',
      dataIndex: 'address',
      key: 'address'
    }, {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <a href="javascript:;" onClick={removeOne(record)}>删除</a>
        </span>
      )
    }
  ]

  return (
    <div
      className="clearfix"
      style={{
      padding: 10,
      marginBottom: 10
    }}>
      <Table columns={columns} dataSource={data} pagination={false}/>
      <Button
        type="primary"
        onClick={addOne}
        style={{
        marginTop: 20,
        marginLeft: 20
      }}>绑定物品</Button>
    </div>
  );
}
export default LwjMetaialAdd

LwjMetaialAdd.props = {}
