import request from "superagent";
const SERVICE_ENDPOINT =  '/services/';

export const URL_GET_PICTURE_URL = SERVICE_ENDPOINT + 'fileDownload?returnType=url';
export const ALIYUN_OSS_URL = '.oss-cn-beijing.aliyuncs.com';
export const ALIYUN_OSS_HW_PARAMS = '?x-oss-process';


class ImageService {}

ImageService.show = function (img, onComplete = (imgUrl) => {}) {
    if (img.indexOf(ALIYUN_OSS_URL) > -1) {
        if (img.indexOf(ALIYUN_OSS_HW_PARAMS) > -1) {
            let imgUrl = img.substring(0, img.indexOf(ALIYUN_OSS_HW_PARAMS));
            onComplete(imgUrl);
        } else {
            onComplete(img);
        }
    } else {
        request
            .get(URL_GET_PICTURE_URL + '&filePath=' + img)
            .responseType('text')
            .end((err, res) => {
                try {
                    let imgUrl = err.rawResponse;
                    onComplete(imgUrl);
                } catch (e) {
                    onComplete(null);
                }

            });
    }

}

export default ImageService;
