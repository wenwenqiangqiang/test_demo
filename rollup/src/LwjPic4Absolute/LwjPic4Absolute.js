
import { Upload, Button, Icon, Modal, Card } from 'antd';
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './LwjSowPicList.less'


export default class LwjPic4Absolute extends Component {

  state = {
    fileList: [],
    previewVisible: false,
    previewVisibleUrl: ''
  };


  handleCancel = () => this.setState({ previewVisible: false })
  componentDidMount() {

  }
  componentWillUnmount() {
    this.setState({ previewVisible: false });
  }

  render() {
    const { title, fileSrc } = this.props;
    const { previewVisible } = this.state;
    const gridStyle = {
      width: '100%',
      textAlign: 'center',
      position: 'relative'
    };
    const toDetail = () => {
      this.setState({ previewVisible: true })
    }
    const getIMGlist = (fileList) => {
      console.info(this.props.fileSrc)
      return (
        <Card.Grid className='LwjSowPicList' style={gridStyle} key="key">
          <img alt={title} src={fileList} style={{ height: "100%", width: "100%", position: "absolute", top: 0, left: 0 }} className='LwjSowPicList_showPic' />
          <div className='LwjSowPicList_showPic_preview'>
            <Icon
              type="eye-o"
              style={{
                fontSize: 30,
                color: '#fff'
              }}
              at={fileList}
              onClick={toDetail}
            />
          </div>
        </Card.Grid>
      )
    }
    return (
      <div >
        {getIMGlist(fileSrc)}
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          <img
            alt="example"
            style={{
              width: '100%'
            }}
            src={fileSrc} />
        </Modal>
      </div>
    );
  }
}

LwjPic4Absolute.props = {
  title: PropTypes.string.isRequired,
  fileSrc: PropTypes.string.isRequired
}
