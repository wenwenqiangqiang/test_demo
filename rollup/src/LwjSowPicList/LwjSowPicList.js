import { Upload, Button, Icon, Modal, Card } from 'antd';
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import ImageService from './imageService'
import './LwjSowPicList.less'
import { isAbsolute } from 'path';
import { debug } from 'util';

export default class LwjSowPicList extends Component {

  state = {
    fileList: [],
    previewVisible: false,
    previewVisibleUrl: '',
    absoluteUrl: ''
  };

  handleCancel = () => this.setState({ previewVisible: false })

  setAbsoluteUrl = (url) => this.setState({ absoluteUrl: url })

  componentDidMount() {
    if (this.props.fileLists) {
      let picUrlsAAA = this.props.fileLists;
      const getUrlAAA = (picUrls) => {
        let promiseArr = [];
        const IMAGE_THUMBNAIL_PARAM_ALIOSS = '?x-oss-process=image/resize,m_lfit,h_400,w_400';
        if (picUrls && picUrls.split(';').length > 0) {
          let newpicUrls = picUrls.split(';');
          if (newpicUrls[newpicUrls.length - 1] == "") {
            newpicUrls.pop();
          }
          newpicUrls.map(img => {
            promiseArr.push(new Promise(function (resolve, reject) {
              ImageService.show(img, (imgUrl) => {
                let thumbnail = ''
                if (imgUrl) {
                  thumbnail = imgUrl.split('?')[0] + IMAGE_THUMBNAIL_PARAM_ALIOSS;
                }
                resolve({ url: imgUrl, thumbnail: thumbnail, orginUrl: img });
              });
            }));
          });
          return promiseArr;
        }
      }

      const dealedTargetUrlAAA = getUrlAAA(picUrlsAAA);
      let sureDealedUrlAAA = [];
      dealedTargetUrlAAA.forEach((value, index) => {
        value.then((val) => {
          let newName = val
            .orginUrl
            .split('/')[
            val
              .orginUrl
              .split('/')
              .length - 1
          ];
          sureDealedUrlAAA.push({
            name: newName.slice(-10),
            uid: index,
            url: val.url,
            thumbnail: val.thumbnail,
            orginUrl: val.orginUrl,
            response: {
              result: val.orginUrl
            }
          })
          this.setState({ fileList: sureDealedUrlAAA });
        })
      })
    }
  }

  componentWillUnmount() {
    this.setState({ fileList: [], previewVisible: false, previewVisibleUrl: '' });
  }

  render() {
    const { title, width } = this.props;

    const { fileList, previewVisible, previewVisibleUrl } = this.state;
    // 生成列表
    const getIMGlist = (fileList) => {
      console.info(fileList)
      if (fileList.length > 0) {
        const res = fileList.map(val => <Card.Grid className='LwjSowPicList' style={gridStyle} key={val.uid}>
          <img alt={val.name} src={val.url} style={{ height: 40 }} className={_.isEmpty(width) ? 'LwjSowPicList_showPic' : ''} />
          <div className='LwjSowPicList_showPic_preview'>
            <Button
              shape="circle"
              icon="eye"
              style={{ marginTop: '31%' }}
              at={val.url}
              size='small'
              onClick={preView}
              ghost
            />

          </div>
        </Card.Grid>);
        return res;
      }
    }

    // 布局样式
    const gridStyle = {
      width: _.isEmpty(width) ? '12.5%' : width,
      textAlign: 'center'
    };
    const preView = (e) => {
      const targetUrl = e
        .target
        .getAttribute('at');
      console.log(e.target);
      this.setState({ previewVisible: true, previewVisibleUrl: targetUrl });
    }

    return (
      <div className="clearfix">
        <Card title={title}>
          {fileList.length > 0 && getIMGlist(fileList)}
        </Card>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          <img
            alt="example"
            style={{
              width: '100%'
            }}
            src={previewVisibleUrl} />
        </Modal>
      </div>
    );
  }
}

LwjSowPicList.props = {
  title: PropTypes.string.isRequired,
  fileLists: PropTypes.string.isRequired,
  width: PropTypes.string
}
