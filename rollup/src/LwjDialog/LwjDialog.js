import {Modal} from 'antd';
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

function notice(type, options) {
    if (!options.onOk || typeof options.onOk != 'function') {
        options.onOk = () => {
            console.log('丽维家弹框默认回调！')
        }
    }
    if (!options.onCancel || typeof options.onCancel != 'function') {
        options.onCancel = () => {
            console.log('丽维家弹框默认回调！')
        }
    }
    options.okText = options.okText || '确认';
    options.cancelText = options.cancelText || '取消';
    return (Modal[type](options));
}

exports['default'] = {
    info: function info(options) {
        return notice('info', options);
    },
    success: function success(options) {
        return notice('success', options);
    },
    error: function error(options) {
        return notice('error', options);
    },
    confirm: function error(options) {
        return notice('confirm', options);
    }
};
module.exports = exports['default'];