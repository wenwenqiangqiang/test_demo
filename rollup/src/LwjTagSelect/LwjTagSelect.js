import {Tag, Modal} from 'antd';
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

export default class LwjTagSelect extends Component {
  state = {
    selectedTabs: [],
    canselectTabs: []
  };

  componentDidMount() {
    this.setState({canselectTabs: this.props.tabLists});
  }

  render() {

    const {tabLists, onChange, title} = this.props;
    const {selectedTabs, canselectTabs} = this.state;

    const preventDefault = (tag) => {
      selectedTabs.splice(canselectTabs.indexOf(tag), 1);
      canselectTabs.push(tag);
      this.setState({selectedTabs, canselectTabs});
      onChange(selectedTabs);
    }

    const handleChange = (tag) => {
      // let flag = true; for (let i = 0; i < selectedTabs.length; i++) {   if
      // (selectedTabs[i].id == tag.id) {     flag = false;   } } if(!flag){
      // Modal.error({     title:'请勿重复添加'   }) }else{   console.log(8888) }

      canselectTabs.splice(canselectTabs.indexOf(tag), 1);
      selectedTabs.push(tag);
      this.setState({selectedTabs, canselectTabs});
      onChange(selectedTabs);
    }

    return (
      <div
        className="clearfix"
        style={{
        border: '1px solid #ccc',
        padding: 10,
        marginBottom: 10
      }}>
        <div style={{
          marginBottom: 10
        }}>
          {selectedTabs.map(tag => (
            <Tag key={tag.id} closable onClose={() => preventDefault(tag)} color="#f50">
              {tag.name}
            </Tag>
          ))}
        </div>

        <div>
          <h6
            style={{
            marginRight: 8,
            display: 'inline'
          }}>{title}:</h6>
          {canselectTabs.map(tag => (
            <Tag key={tag.id} onClick={() => handleChange(tag)}>
              {tag.name}
            </Tag>
          ))}
        </div>
      </div>
    );
  }
}

LwjTagSelect.props = {}
