### 插件
跟webpack可以通过loader、minnimizer和plugins来扩展本身的功能以外。插件是扩展Rollup的唯一途径。
- 使用 ES 的新特性
- 加载其它类型的模块资源
- 导入 CommonJS 模块


### 加载npm包

Rollup默认不能在代码里面通过名称去就加载第三方npm模块，通过使用rollup-plugin-node-resolve可以解决这个问题。

```
// rollup.config.js
...
import resolve from 'rollup-plugin-node-resolve';

export default {
  ...
  plugins:[
    ...
    resolve()
  ]
}
```

### 加载 CommonJS 模块
Rollup本身只处理 ESM，导入 CommonJS 模块会报错。官方给出了rollup-plugin-commonjs插件来解决这个问题。
```
// rollup.config.js
import cjs from 'rollup-plugin-commonjs';
...
export default {
  ...
  plugins: [
    ...
    cjs()
  ]
}
```

### Code Splitting
ESM支持动态导入，因此也可以做代码分割。
```
// src/index.js
import('./logger').then(({ log } => {
  log('code splitting~');
})
```
注意：代码分割不支持 iife 和 umd 这种 format 的


### 多入口打包
只需要将Rollup的配置入口修改为对象或者数组，即可实现多入口打包


### Rollup 于 Webpack 的差别
通过使用，Rollup有以下优势：
- 输出结果更加扁平
- 自动移除未引用的代码
- 打包结果依然完全可读

同时存在以下缺点：
- 加载非 ESM 的第三方模块比较复杂
- 模块最终被打包到一个函数中，无法实现 HMR
- 浏览器环境中，代码拆分功能依赖 AMD 库
Webpack大而全，Rollup小而美。
Webpack适合用作应用程序的开发；Rollup适合一些框架或者类库的开发。 







