// rollup.config.js
// 虽然这是一个Node.js文件，依然采用ESM的方式，Rollup内部做了相关处理


import json from 'rollup-plugin-json';
import resolve from 'rollup-plugin-node-resolve';
import cjs from 'rollup-plugin-commonjs';

export default {
    // input: './src/index.js',
    input: {
        foo: './src/foo.js',
        bar: './src/bar.js',
    },
    // output: {
    //     file: 'dist/bundle.js',
    //     format: 'iife'
    // },
    output: {
        dir: 'dist',
        format: 'amd',
    },
    plugins: [
        json(),
        resolve(),
        cjs()
    ]
};
