// src/logger.js
export const log = (msg) => {
    console.log(msg);
};

export const error = (msg) => {
    console.error(msg);
};
