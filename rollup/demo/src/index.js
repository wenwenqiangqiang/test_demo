// src/index.js
import {log} from './logger';
import messages from './messages';
import {name, version} from '../package.json';
import _ from 'lodash-es';
import {foo} from './cjs-module';

const msg = messages.hi;

log(msg);
console.log(name, version);
// log(_.camelCase('hello world'));
console.log(foo);

//npx rollup src/index.js --format iife --file dist/bundle.js
