define(['require'], function (require) { 'use strict';

    // src/bar.js
    new Promise(function (resolve, reject) { require(['./logger-d72802e7'], resolve, reject) }).then(({ log }) => {
        log('bar module');
    });
    console.log('bar module');

});
