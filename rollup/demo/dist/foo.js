define(['require'], function (require) { 'use strict';

    // src/foo.js
    new Promise(function (resolve, reject) { require(['./logger-d72802e7'], resolve, reject) }).then(({ log }) => {
        log('foo module');
    });
    console.log('foo module');

});
