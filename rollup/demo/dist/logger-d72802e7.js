define(['exports'], function (exports) { 'use strict';

    // src/logger.js
    const log = (msg) => {
        console.log(msg);
    };

    const error = (msg) => {
        console.error(msg);
    };

    exports.error = error;
    exports.log = log;

});
