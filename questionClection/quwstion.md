### seo
1. title、description、keyword
2. html语义化
3. 重要内容靠前输出
4. 重要内容不用js输出
5. 不用iframe
6. 非装饰图片使用alt属性

### 状态码
100系列，请求成功等待服务器响应
200系列，请求成功，正常返回
300系列，重定向
400系列，客户端错误
500系列，服务端错误


### 虚拟列表
react-infinite-scroller

### 脚手架工具
panz + qsq + lwj-ui-modal-react + lwj-easy-report

### z-index
- background
- z-index<0
- block
- 浮动
- inline/inline-block
- z-index=0 || auto
- z-index>0

定位：相对定位不脱离文档流，绝对定位脱离文档流，相对于最近的具有定位的父级元素padding的位置定位，固定定位相对于视窗


### 事件委托的好处
1. 减少事件注册，节省内存
2. 动态新增元素不需要额外增加点击事件


### 如何判断两个变量绝对相等
```javascript
Object.is('a','a') //true
```

#cjs
```javascript
// a.js
module.exports = {
    a: 1
}
// or
exports.a = 1

// b.js
var module = require('./a.js')
module.a // -> log 1
```

```javascript

var module = require('./a.js')
module.a
// 这里其实就是包装了一层立即执行函数，这样就不会污染全局变量了，
// 重要的是 module 这里，module 是 Node 独有的一个变量
module.exports = {
    a: 1
}
// module 基本实现
var module = {
  id: 'xxxx', // 我总得知道怎么去找到他吧
  exports: {} // exports 就是个空对象
}
// 这个是为什么 exports 和 module.exports 用法相似的原因
var exports = module.exports
var load = function (module) {
    // 导出的东西
    var a = 1
    module.exports = a
    return module.exports
};
// 然后当我 require 的时候去找到独特的
// id，然后将要使用的东西用立即执行函数包装下，over

/**
*另外虽然 exports 和 module.exports 用法相似，但是不能对 exports 直接赋值。
*因为 var exports = module.exports 这句代码表明了 exports 和 module.exports享有相同地址，
*通过改变对象的属性值会对两者都起效，但是如果直接对 exports 赋值就会导致两者不再指向同一个内存地址，
*修改并不会对 module.exports 起效
*/

```

### 状态码区别
- 401/403 未登录/权限不足
- 204/304 请求资源为空/请求资源已有，请用缓存
- 301/302 永久重定向/临时重定向

### fiber原理
任务调度器==》Fiber 树在首次渲染的时候会一次过生成。
在后续需要 Diff 的时候，会根据已有树和最新 Virtual DOM 的信息，生成一棵新的树。
这颗新树每生成一个新的节点，都会将控制权交回给主线程，去检查有没有优先级更高的任务需要执行。
===》如果没有，则继续构建树的过程
===》如果过程中有优先级更高的任务需要进行，则 Fiber Reconciler 会丢弃正在生成的树，在空闲的时候再重新执行一遍。

### react的setstate原理
1. setState在react生命周期和合成事件会批量覆盖执行
```
合成事件：react为了解决跨平台，兼容性问题，自己封装了一套事件机制，代理了原生的事件，
像在jsx中常见的onClick、onChange这些都是合成事件
```
2. setstate在原生事件，setTimeout,setInterval,promise等异步操作中，state会同步更新（异步操作，内部机制无法检测）
3. setState批量更新的过程
```
在react生命周期和合成事件执行前后都有相应的钩子
```

### react hooks/高阶组件/class组件
1. 高阶组件=》书写复杂，层级结构深，组合性差
2. class组件状态复杂，不符合react的推荐

### new lifecycle
1. 挂载
- constructor
- getDerivedStateFromProps
- render
- componentDidMount
2. 更新
- getDerivedStateFromProps
- shouldComponentUpdate
- render
- getSnapshotBeforeUpdate
- componentDidUpdate
3. 卸载
- componentWillUnmount




