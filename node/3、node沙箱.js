/**
 *  eval 是全局对象的一个函数属性，执行的代码拥有着和应用中其它正常代码一样的的权限，
 *  它能访问「执行上下文」中的局部变量，也能访问所有「全局变量」，在这个场景下，它是一个非常危险的函数。
 */
console.log(eval('1+2'));
/**
 * 再来看看 Functon，通过 Function 构造器，我们可以动态的创建一个函数，然后执行它
 * 运行函数的时候，只能访问自己的本地变量和全局变量，不能访问 Function 构造器被调用生成的上下文的作用域。
 */

const sum = new Function('m', 'n', 'return m + n');
console.log(sum(1, 2));

/**
 * 结合 ES6 的新特性 Proxy便能更安全一些
 */

function evalute(code, sandbox) {
    sandbox = sandbox || Object.create(null);
    const fn = new Function('sandbox', `with(sandbox){return (${code})}`);
    const proxy = new Proxy(sandbox, {
        has(target, p) {
            return true
        }
    })
    return fn(proxy)
}

console.log(evalute('1+2'));
//没有传入global会报错
evalute('console.log(1)', global);

/**
 * vm模块
 */
const vm = require('vm');
const script = new vm.Script('m+n');
const sandbox = {m: 1, n: 2};
const context = vm.createContext(sandbox);
console.log(script.runInContext(context));




