
- docker image build ./ -t qsq-front:1.0.0 # 打包镜像
- docker container create -p 2333:80 qsq-front:1.0.0
- docker container start xxx # xxx 为上一条命令运行得到的结果
- docker container exec -it xxx /bin/bash

### 镜像相关
- docker inspect 61d271fc1567 镜像详情
- docker search mysql
- docker rmi image/imageId

- docker tag qsq-front:1.0.1 qsq1989/qsq-front:1.0.1
- docker push qsq1989/qsq-front:1.0.1

### 容器相关
- docker create -it qsq-front:1.0.1
- docker run ===  docker create + docker start
- docker run -it qsq-front:1.0.1 /bin/bash
- pwd 当前文件夹
- ps 查看进程
-  -i容器标准输入打开 -t分配一个伪终端
- docker logs 436f066a013d
- docker stop container/containerId
- docker ps -a -q  退出状态的container
- docker ps 运行状态的container
- docker exec -it 436f066a013d /bin/bash 进入容器
- docker rm 删除容器
- 导入导出容器

### 仓库
- docker run -d -p 5000:5000 -v /opt/data/registry:/tmp/registry   下载启动仓库容器并将镜像放在/opt/data/registry下，默认tmp/registry
- 

### docker-compose 
```
速搭建环境。具体步骤如下所示：
$ git -v
$ docker -v
$ docker-compose -v
$ git clone git@bitbucket.com:<project>/development.git <project> && cd <project>
$ git submodule init && git submodule update
$ git submodule foreach npm install
$ docker-compose up -d
```
- docker-compose up -d
- docker-compose ps




