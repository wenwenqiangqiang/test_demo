##创建存放数据的位置
```
# 进入个目录，这个你们随便
cd /srv
# 创建个文件夹
mkdir nexus-data
# 赋予权限，不然启动会报错，无操作权限
    chmod 777 nexus-data
```
##启动
```
docker run -d -p 8081:8081 --name nexus -v /srv/nexus-data:/nexus-data --restart=always sonatype/nexus3
通过docker logs -f nexus查看启动日志，当出现Started Sonatype Nexus OSS说明启动成功，这时通过http://ip:8081即可访问

点击右上角Sign in进行登录，账号为admin，密码需要去镜像中查看
# 进入镜像
docker exec -it nexus bash
# 查看密码，路径在登录框会提示，然后复制即可，登陆成功后会让你修改密码
cat /nexus-data/admin-password

至此，就启动完成了，进入主页后点击左边菜单栏的Browse即可查看你拥有的仓库啦
```
##详细
```
https://yq.aliyun.com/articles/720033?spm=a2c4e.11155472.0.0.779b6a01U90vEx
```

#配置加速源
```
修改文件 /etc/docker/daemon.json

{
  "registry-mirrors": [
    "http://f42ebfb9.m.daocloud.io"
  ]
}
```

```
下载
https://blog.csdn.net/qq_42428528/article/details/100747560
http://119.29.241.56:8080/view/1320
```

#  直接安装

```
  scp ./nexus-3.13.0-01-unix.tar.gz root@119.23.109.180:~/workspace/


文件夹传输
 scp -r test      codinglog@192.168.0.101:/var/www/   

tar -zvxf nexus-3.13.0-01-unix.tar.gz


解压到/usr/local/nexus下
使用命令：tar -zxvf nexus-3.13.0-01-unix.tar.gz  -C /usr/local/nexus




a、修改配置文件，nexus目录下，cd etc，可以修改端口号和ip地址
如，修改端口号：vim etc/nexus-default.properties  =>  application-port=8081
b、如果Linux硬件配置比较低的话，建议修改为合适的大小，否则会出现运行崩溃的现象
　　# vim nexus/bin/nexus.vmoptions //虚拟机选项配置文件，可以修改数据、日志存储位置

启动 Nexus（默认端口是8081），Nexus 常用的一些命令包括：/root/nexus/nexus/bin/nexus  {start|stop|run|run-redirect|status|restart|force-reload}，如：//启动 nexus start //停止 nexus stop //重启 nexus restart //查看状态 nexus status下面我们启动Nexus：
启动命令：进入解压后的文件nexus中，输入命令：./bin/nexus start,也可以使用./bin/nexus run,run可以打印详细启动日志信息会出现如下警告：

11、查看nexus服务是否启动成功
　　使用命令：ps -ef|grep nexus,如果出现以下界面，说明nexus服务启动成功。


12. 云服务器开启安全组8081端口访问
  端口：8081
授权对象：0.0.0.0/0


默认用户名密码 admin  admin123

cd /sonatype-work/nexus3  admin.password

13. Nexus max file descriptors  提示

65535换成65536

/etc/security/limits.conf
nexus          soft    nofile  65536
nexus          hard    nofile  65536

https://blog.csdn.net/nklinsirui/article/details/80417293

```

#npm 伺服
```
npm(group)表示分组，npm(hosted)表示本机私有，npm(proxy)表示远程代理。
若registry配置为group(包括hosted和proxy)，首先会从hosted取，若无则从proxy取并缓存，下次则会从缓存取。


点击Create repository按钮创建仓库
选择 npm(proxy), 输入 Name: npm-proxy, remote storage 填写 https://registry.npm.taobao.org 或 https://registry.npmjs.org. 用于将包情求代理到地址地址

https://www.jianshu.com/p/1674a6bc1c12
```




